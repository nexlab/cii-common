import * as Sequelize from "sequelize";

export class PlanUsers extends Sequelize.Model {
  public id: number;
  public userId: number;
  public planIdsUnRead: number[];
}

export function initPlanUsers(sequelize: Sequelize.Sequelize) {
  PlanUsers.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: Sequelize.BIGINT,
      allowNull: false,

      references: {
        model: "users",
        key: "id"
      }
    },
    planIdsUnRead: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: Sequelize.NOW,
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: Sequelize.NOW,
    },
    deletedAt: {
      type: Sequelize.DATE,
      allowNull: true,
    }
  }, {
    tableName: "plan_users",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
