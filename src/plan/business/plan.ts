import { PlanUsers } from "../model/plan-users";

export async function countNewPlanUser(userId: string | number): Promise<number> {
  const current = await PlanUsers.find({
    where: { userId }
  });

  if (!current) {
    return 0;
  }

  return current.planIdsUnRead.length;
}
