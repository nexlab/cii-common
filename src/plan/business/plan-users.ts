import * as lodash from "lodash";
import { PlanUsers } from "../model/plan-users";

export interface ICreatePlanUsers {
  memberIds: Array<string | number>;
  planId: string;
}

export async function createPlanUsers(data: ICreatePlanUsers): Promise<any> {
  try {
    const planUsers = await PlanUsers.findAll({
      where: {
        userId: data.memberIds
      }
    });

    function parseIds(reverse: boolean = false) {
      return planUsers.filter((e) => {
        const current = data.memberIds.find((a) => a.toString() === e.userId.toString());
        if (current) {
          return reverse;
        }

        return !reverse;
      });
    }

    const planUsersNeedCreate = data.memberIds.filter((e) => {
      const current = planUsers.find((a) => e.toString() === a.userId.toString());
      if (current) {
        return false;
      }

      return true;
    });
    const planUsersNeedUpdate = parseIds(true);

    if (planUsersNeedCreate && planUsersNeedCreate.length > 0) {
      await Promise.all(
        planUsersNeedCreate.map((e) => {
          return PlanUsers.create({
            userId: e,
            planIdsUnRead: [data.planId]
          });
        })
      );
    }

    if (planUsersNeedUpdate && planUsersNeedUpdate.length > 0) {
      await Promise.all(
        planUsersNeedUpdate.map((e) => {
          e.planIdsUnRead.push(<any> data.planId);

          return PlanUsers.update({
            planIdsUnRead: lodash.uniq(e.planIdsUnRead)
          }, {
            where: { userId: e.userId, }
          });
        })
      );
    }

    return;
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function markPlanUserAsRead(userId: number | string): Promise<number[]> {

  const current = await PlanUsers.find({
    where: { userId }
  });
  let result = [];
  if (current) {
    result = current.planIdsUnRead || [];
  }
  if (result.length > 0) {
    await PlanUsers.update({ planIdsUnRead: [] }, {
      where: { userId }
    });
  }

  return result;
}
