import * as Sequelize from "sequelize";
import { defaultColumns, ReferenceType } from "../../models";
import { NotificationUser } from "./notification-user";

export class Notification extends Sequelize.Model {

  public id: number;
  public title: string;
  public content: string;
  public senderId: string;
  public groupId: string;
  public metadata: object;

  public type: ReferenceType;
  public referenceId: string;
  public referenceUrl: string;

  public createdAt: Date;
  public createdBy: string;
  public updatedAt: Date;
  public updatedBy: string;
  public deletedAt: Date;

  public receivers?: NotificationUser[];
}

export function initNotification(sequelize: Sequelize.Sequelize) {
  Notification.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: ""
    },

    content: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: ""
    },

    senderId: {
      type: Sequelize.BIGINT,
      allowNull: true,
    },

    groupId: {
      type: Sequelize.BIGINT,
      allowNull: true,
    },
    type: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    referenceId: {
      type: Sequelize.BIGINT,
      allowNull: true,
    },
    metadata: {
      type: Sequelize.JSON,
      allowNull: false,
      defaultValue: {}
    },

    referenceUrl: {
      type: Sequelize.TEXT,
      allowNull: true,
    },
    ...defaultColumns
  }, {
    tableName: "notifications",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
