import * as Sequelize from "sequelize";
import { defaultColumns } from "../../models";

export class NotificationGroup extends Sequelize.Model {

  public id: number;
  public title: string;
  public subscriptions: string[];

  public createdAt: Date;
  public createdBy: string;
  public updatedAt: Date;
  public updatedBy: string;
  public deletedAt: Date;

}

export function initNotificationGroup(sequelize: Sequelize.Sequelize) {
  NotificationGroup.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: ""
    },
    subscriptions: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },
    ...defaultColumns
  }, {
    tableName: "notification_groups",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
