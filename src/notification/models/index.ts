import { Sequelize } from "sequelize";
import { initNotification, Notification } from "./notification";
import { initNotificationGroup, NotificationGroup } from "./notification-group";
import { initNotificationUser, NotificationUser } from "./notification-user";

export {
  Notification,
  NotificationGroup,
  NotificationUser,
};

export function initDBNotification(sequelize: Sequelize) {

  initNotification(sequelize);
  initNotificationGroup(sequelize);
  initNotificationUser(sequelize);

  Notification.hasMany(NotificationUser, {
    as: "receivers",
    foreignKey: "notificationId",
  });
}
