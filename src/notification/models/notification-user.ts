import * as Sequelize from "sequelize";
import { Notification } from "./notification";

export class NotificationUser extends Sequelize.Model {

  public id: number;
  public notificationId: string;
  public userId: string;
  public isRead: string;

  public notification?: Notification;
  public metadata?: any;

  public createdAt: Date;
  public updatedAt: Date;
  public deletedAt: Date;

}

export function initNotificationUser(sequelize: Sequelize.Sequelize) {
  NotificationUser.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    notificationId: {
      type: Sequelize.BIGINT,
      allowNull: false,
    },
    userId: {
      type: Sequelize.BIGINT,
      allowNull: false,
    },
    isRead: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: Sequelize.NOW,
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: Sequelize.NOW,
    },
    deletedAt: {
      type: Sequelize.DATE,
      allowNull: true,
    },
  }, {
    tableName: "notification_users",
    sequelize,
    paranoid: true,
    timestamps: true,
  });

  NotificationUser.belongsTo(Notification, {
    as: "notification",
    foreignKey: "notificationId"
  });
}
