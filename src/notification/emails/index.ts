import * as lodash from "lodash";
import { ReferenceType } from "../../models";
import {
  messageEmailTemplate, planItemEmailTemplate, planTemplateEmail, projectEmailTemplate,
  reminderEmailTemplate, taskEmailTemplate, topicEmailTemplate,
} from "./templates";
import { IParamTemplateEmail, TypeContentTemplateEmail, TypeEmailTemplate } from "./types";
import { contentAndTitleForEmail } from "./vi";

export * from "./types";

export const emailTemplates = {
  [TypeEmailTemplate.MessageNotification]: messageEmailTemplate,
  [TypeEmailTemplate.TopicNotification]: topicEmailTemplate,
  [TypeEmailTemplate.PlanItemNotification]: planItemEmailTemplate,
  [TypeEmailTemplate.PlanNotification]: planTemplateEmail,
  [TypeEmailTemplate.ProjectNotification]: projectEmailTemplate,
  [TypeEmailTemplate.ReminderNotification]: reminderEmailTemplate,
  [TypeEmailTemplate.TaskNotification]: taskEmailTemplate,
};

export interface IParamEmail {
  template: {
    type: TypeEmailTemplate,
    params: IParamTemplateEmail
  };
  titleAndContent: {
    type: TypeContentTemplateEmail,
    params: {
      title?: {
        [key: string]: string
      },
      content?: {
        [key: string]: string
      }
    },
    content?: string;
    title?: string;
  };
}

export function parseDataEmail(params: IParamEmail) {
  let emailBody = lodash.cloneDeep(emailTemplates[params.template.type]);

  const titleAndContent = lodash.cloneDeep(contentAndTitleForEmail[params.titleAndContent.type]);
  const paramTitle = params.titleAndContent.params ? params.titleAndContent.params.title : null;
  let emailTitle = titleAndContent.title;
  if (params.titleAndContent.title) {
    emailTitle = params.titleAndContent.title;
  }
  if (emailTitle && paramTitle && Object.keys(paramTitle).length > 0) {
    Object.keys(paramTitle).forEach((e) => {
      emailTitle = emailTitle.replace(`{{${e}}}`, paramTitle[e]);
    });
  }

  const paramContent = params.titleAndContent.params ? params.titleAndContent.params.content : null;
  let emailContent = titleAndContent.content;
  if (params.titleAndContent.content) {
    emailContent = params.titleAndContent.content;
  }
  if (emailContent && paramContent && Object.keys(paramContent).length > 0) {
    Object.keys(paramContent).forEach((e) => {
      emailContent = emailContent.replace(`{{${e}}}`, paramContent[e]);
    });
  }

  params.template.params.content = emailContent;
  if (titleAndContent.params) {
    params.template.params = {
      ...params.template.params,
      ...titleAndContent.params
    };
  }

  const paramTemplate = params.template.params;
  if (paramTemplate && Object.keys(paramTemplate).length > 0) {
    Object.keys(paramTemplate).forEach((e) => {
      emailBody = emailBody.replace(`{{${e}}}`, paramTemplate[e]);
    });
  }

  return {
    emailBody,
    emailTitle
  };

}

const baseUrl = "https://system.ciiec-cc.vn";
const [topic, message, plan, planItem, project, task, taskComment] = [
  "/my-topic?type=openModal&id={{id}}",
  "/my-inbox?type=openModal&id={{id}}",
  "/plans/{{id}}/detail",
  "/plans/{{planId}}/detail/{{id}}",
  "/projects/{{id}}",
  "/projects/{{projectId}}/task/{{id}}",
  "/projects/{{projectId}}/task/{{id}}/task-comment/{{commentId}}"
];

export interface IParamParseLink {
  id: string;
  planId?: string;
  projectId?: string;
}

export function parseLink(params: IParamParseLink, type: ReferenceType) {
  let path;
  switch (type) {
  case ReferenceType.Message:
    path = message;
    break;
  case ReferenceType.Topic:
    path = topic;
    break;
  case ReferenceType.Plan:
    path = plan;
    break;
  case ReferenceType.PlanItem:
    path = planItem;
    break;
  case ReferenceType.Project:
    path = project;
    break;
  case ReferenceType.Task:
    path = task;
    break;
  case ReferenceType.TaskComment:
    path = taskComment;
    break;
  }
  let url = baseUrl + path;
  for (const key in params) {
    url = url.replace(`{{${key}}}`, params[key]);
  }

  return url;
}
