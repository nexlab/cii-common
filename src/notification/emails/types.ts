export enum TypeEmailTemplate {
  MessageNotification = "message_notification",
  TopicNotification = "topic_notification",
  PlanItemNotification = "plan_item_notification",
  PlanNotification = "plan_notification",
  ProjectNotification = "project_notification",
  ReminderNotification = "reminder_notification",
  TaskNotification = "task_notification"
}

export interface IParamBasicTemplateEmail {
  trigger?: string;
  action?: string;
  title: string;
  modified?: string;
  content?: string;
  link: string;
}

export type IParamMessageTemplateEmail = IParamBasicTemplateEmail;

export type IParamTopicTemplateEmail = IParamBasicTemplateEmail;

export type IParamProjectTemplateEmail = IParamBasicTemplateEmail;

export interface IParamProjectTaskTemplateEmail extends IParamBasicTemplateEmail {
  header: string;
  status: string;
}

export type IParamPlanTemplateEmail = IParamProjectTemplateEmail;

export interface IParamPlanItemTemplateEmail extends IParamBasicTemplateEmail {
  header: string;
  deadline: string;
}

export interface IParamReminderTemplateEmail {
  type: string;
  parent: string;
  header: string;
  title: string;
  deadline: string;
  content: string;
}

export type IParamTemplateEmail = IParamMessageTemplateEmail | IParamTopicTemplateEmail | IParamProjectTemplateEmail |
  IParamProjectTaskTemplateEmail | IParamPlanTemplateEmail | IParamPlanItemTemplateEmail |
  IParamReminderTemplateEmail;

export enum TypeContentTemplateEmail {
  MessageCreate = "message_create",
  MessageUpdate = "message_update",
  MessageUpdateMembers = "message_update_members",
  MessageUpdateMembersTypeNew = "message_update_members_type_new",
  MessageUpdateMembersTypeRemove = "message_update_members_type_remove",
  MessageAddComment = "message_add_comment",
  MessageCommentAddMention = "message_comment_add_mention",
  MessageAddAttachment = "message_add_attachment",
  MessageClose = "message_close",

  TopicCreate = "topic_create",
  TopicUpdate = "topic_update",
  TopicUpdateMembers = "topic_update_members",
  TopicUpdateMembersTypeNew = "topic_update_members_type_new",
  TopicUpdateMembersTypeRemove = "topic_update_members_type_remove",
  TopicAddComment = "topic_add_comment",
  TopicCommentAddMention = "topic_comment_add_mention",
  TopicAddAttachment = "topic_add_attachment",
  TopicClose = "topic_close",

  ProjectCreate = "project_create",
  ProjectUpdate = "project_update",
  ProjectClose = "project_close",

  ProjectTaskCreate = "project_task_create",
  ProjectTaskUpdateContent = "project_task_update_content",
  ProjectTaskUpdateDeadline = "project_task_update_deadline",
  ProjectTaskUpdateTaskStatus = "project_task_update_task_status",
  ProjectTaskUpdateEstimation = "project_task_update_estimation",
  ProjectTaskChangeAssignee = "project_task_change_assignee",
  ProjectTaskChangeReporter = "project_task_change_reporter",
  ProjectTaskAddComment = "project_task_add_comment",
  ProjectTaskAddAttachment = "project_task_add_attachment",
  ProjectTaskChangePrivate = "project_task_change_private",
  ProjectTaskUpdateRating = "project_task_update_rating",

  PlanCreate = "plan_create",
  PlanUpdate = "plan_update",

  PlanItemCreate = "plan_item_create",
  PlanItemUpdateContent = "plan_item_update_content",
  PlanItemUpdateDeadline = "plan_item_update_deadline",
  PlanItemClose = "plan_item_close",

  ReminderTask = "reminder_task",
  ReminderPlanItem = "reminder_plan_item"
}

export interface IContentAndTitleForEmail {
  title?: string;
  content?: string;
  params?: {
    [key: string]: string
  };
}

export interface IParamContentMessageUpdateMembersTypeNew {
  messageUpdater: string;
  newMember: string | string[];
}

export interface IParamContentMessageUpdateMembersTypeRemove {
  messageUpdater: string;
  removeMember: string | string[];
}

export interface IParamContentTopicUpdateMembersTypeNew {
  topicUpdater: string;
  newMember: string | string[];
}

export interface IParamContentTopicUpdateMembersTypeRemove {
  topicUpdater: string;
  removeMember: string | string[];
}

export interface IParamContentAddAttachment {
  attachmentList: string[];
}

export interface IParamContentActionClose {
  time: string;
}

export interface IParamContentUpdateDeadline {
  oldDeadline: string;
  newDeadline: string;
}

export interface IParamContentProjectTaskUpdateStatus {
  oldStatus: string;
  newStatus: string;
}

export interface IParamContentUpdateEstimation {
  oldEstimation: string;
  newEstimation: string;
}

export interface IParamContentProjectTaskChangeAssignee {
  oldAssignee: string;
  newAssignee: string;
}

export interface IParamContentProjectTaskChangeReporter {
  oldReporter: string;
  newReporter: string;
}

export interface IParamContentProjectTaskChangePrivate {
  oldValue: string;
  newValue: string;
}

export interface IParamContentProjectTaskUpdateRating {
  ratingName: string;
}

export type IParamContentEmailTemplate =
  string | IParamContentMessageUpdateMembersTypeNew | IParamContentMessageUpdateMembersTypeRemove |
  IParamContentTopicUpdateMembersTypeNew | IParamContentTopicUpdateMembersTypeRemove |
  IParamContentAddAttachment | IParamContentActionClose | IParamContentUpdateDeadline |
  IParamContentProjectTaskUpdateStatus | IParamContentUpdateEstimation | IParamContentProjectTaskChangeAssignee |
  IParamContentProjectTaskChangeReporter | IParamContentProjectTaskChangePrivate | IParamContentProjectTaskUpdateRating;
