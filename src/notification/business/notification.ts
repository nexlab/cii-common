import { omit } from "lodash";
import { Op } from "sequelize";
import { IUpdateOptions, ReferenceType, StatusCode, User } from "../../models";
import { defaultSettingForEntire, UserNotificationSetting } from "../../user-notification-setting";
import { Notification } from "../models";
import { validateEmailForm, validateNotificationForm, INotificationInput, INotificationPusher } from "../vendors";
import { OneSignalClient, OneSignalPusher } from "../vendors/one-signal";
import { createManyNotificationUsers } from "./notification-user";

import { parseDataEmail, IParamEmail } from "../emails";

let _pusher: INotificationPusher;

export function notificationPusher(appId?: string, apiKey?: string): INotificationPusher {
  if (_pusher) {
    return _pusher;
  }

  appId = appId || process.env.ONE_SIGNAL_APP_ID;
  apiKey = apiKey || process.env.ONE_SIGNAL_API_KEY;

  _pusher = OneSignalPusher(
    OneSignalClient({
      app_id: appId,
      api_key: apiKey,
    })
  );

  return _pusher;
}

export async function createNotification(
  input: INotificationInput, updateOptions?: IUpdateOptions): Promise<Notification> {
  const opts = { ...updateOptions };

  const model = await Notification.create({
    senderId: opts.actorId,
    createdBy: opts.actorId || input.senderId,
    updatedBy: opts.actorId || input.senderId,
    ...omit(input, ["receiverIds"]),
  });

  const receivers = await createManyNotificationUsers(model.id, input.receiverIds);

  return <any> {
    ...model,
    receivers,
  };
}

export interface IOptionCreateAndPushNotification extends IUpdateOptions {
  filterUserSettingNotification?: boolean;
}

export async function createAndPushNotification(
  pusher: INotificationPusher, input: INotificationInput,
  updateOptions?: IOptionCreateAndPushNotification): Promise<Notification> {
  pusher = pusher || notificationPusher();
  // only push app
  if (updateOptions.filterUserSettingNotification === false) {
    const modelOnlyPushApp = await createNotification(input, updateOptions);
    const payloadMore = <any> validateNotificationForm(input, null, input.parentId);
    await pusher.sendToUsers(<any> input.receiverIds, payloadMore);

    return modelOnlyPushApp;
  }

  if (![ReferenceType.Message, ReferenceType.Topic].includes(input.type)) {
    return pushFollowUserSettingNotification(pusher, input, updateOptions);
  }

  const model = await createNotification(input, updateOptions);
  const payload = <any> validateNotificationForm(input, null, input.parentId);
  await pusher.sendToUsers(<any> input.receiverIds, payload);

  return model;
}

export interface IOptionFilterForPush {
  userNotificationSetting?: boolean;
}

export async function filterUserForPush(input: INotificationInput | INotificationEmailInput): Promise<any> {
  const receiverIdsForEmail = [];
  const receiverIdsForAppPush = [];
  if (input.receiverIds.length === 0) {
    return { receiverIdsForAppPush, receiverIdsForEmail };
  }
  const userNotificationSetting = await UserNotificationSetting.findAll({
    where: {
      type: input.type,
      userId: {
        [Op.in]: input.receiverIds
      },
      referenceId: input.referenceId
    }
  });

  return parseListUser(input, userNotificationSetting);
}

export async function parseListUser(
  input: INotificationInput | INotificationEmailInput, userNotificationSetting?: UserNotificationSetting[]) {
  const arr: number[] = <any> input.receiverIds;
  const users = await User.findAll({
    where: { id: { [Op.in]: arr }, status: StatusCode.Active }
  });
  const receiverIdsForEmail = [];
  const receiverIdsForAppPush = [];

  const receiverIds: UserNotificationSetting[] = arr.map((e) => {
    let current = null;
    if (userNotificationSetting) {
      const currentUserNotificationSetting = userNotificationSetting.find((a) => a.userId.toString() === e.toString());
      current = currentUserNotificationSetting ? currentUserNotificationSetting.toJSON() : null;
    }
    const currentUser = users.find((b) => b.id.toString() === e.toString());

    return <any> {
      setting: current ? current.setting : defaultSettingForEntire(),
      user: currentUser && currentUser.toJSON ? currentUser.toJSON() : currentUser
    };
  }).filter((e) => e.user);

  receiverIds.forEach((e) => {
    if (e.setting.email) {
      if (e.user.notificationEmail) {
        receiverIdsForEmail.push(e.user.notificationEmail);
      } else {
        receiverIdsForEmail.push(e.user.id);
      }
    }
    if (e.setting.appPush) {
      receiverIdsForAppPush.push(e.user.id);
    }
  });

  return {
    receiverIdsForEmail,
    receiverIdsForAppPush
  };
}

export async function pushFollowUserSettingNotification(
  pusher: INotificationPusher, input: INotificationInput, updateOptions?: IUpdateOptions): Promise<any> {
  const userIds = await filterUserForPush(input);

  if (userIds.receiverIdsForAppPush.length > 0) {
    input.receiverIds = userIds.receiverIdsForAppPush;
    await createNotification(input, updateOptions);
    const payloadForNotification = validateNotificationForm(input);
    await pusher.sendToUsers(<any> input.receiverIds, payloadForNotification);
  }

  return;
}

export interface INotificationEmailInput {
  receiverIds: Array<number | string>;
  params: IParamEmail;
  type: ReferenceType;
  referenceId: string;
  userNotificationSetting?: boolean;
}

export interface IValidateEmailFormInput {
  receiverIds: Array<number | string>;
  title: string;
  content: string;
}

export async function pushEmailNotification(
  pusher: INotificationPusher, input: INotificationEmailInput, _updateOptions?: IUpdateOptions): Promise<any> {
  pusher = pusher || notificationPusher();
  const userIds = input.userNotificationSetting === false
    ? await parseListUser(input)
    : await filterUserForPush(input);

  if (userIds.receiverIdsForEmail.length > 0) {
    const dataEmail = parseDataEmail(input.params);
    const dataEmailForm = {
      receiverIds: userIds.receiverIdsForEmail,
      title: dataEmail.emailTitle,
      content: dataEmail.emailBody
    };
    const payload = validateEmailForm(dataEmailForm);

    await pusher.sendToEmailUsers(<any> dataEmailForm.receiverIds, payload);
  }

  return;
}
