import { NotificationUser } from "../models";

export function createManyNotificationUsers(
  notificationId: number | number, receiverIds: Array<string | number>): Promise<NotificationUser[]> {

  return <any> receiverIds.map((userId) => NotificationUser.create({
    notificationId,
    userId,
    isRead: false,
  }));
}

export function markNotificationUserAsRead(userId: number | string): Promise<any> {

  return <any> NotificationUser.update({ isRead: true }, {
    where: { userId }
  });
}

export function countNewNotificationUser(userId: number | string): Promise<number> {

  return <any> NotificationUser.count({
    where: {
      userId,
      isRead: false
    }
  });
}
