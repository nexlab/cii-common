import * as lodash from "lodash";
import { Op } from "sequelize";
import { User } from "../models";

export function userFindManyByIds(ids: Array<string | number>): Promise<User[]> {
  return <any> User.findAll({
    where: {
      id: {
        [Op.in]: ids,
      }
    }
  });
}

export async function mapMetadata(data: any[]) {
  const userIds = lodash.uniq(data.map((e) => [e.createdBy || null, e.updatedBy || null]).reduce((pre, current) => {
    return pre.concat(current);
  }, [])).filter((e) => e);
  const users = await userFindManyByIds(userIds);

  return data.map((e) => ({
    ...e.toJSON ? e.toJSON() : e,
    createdBy: users.find((u) => u.id.toString() === (e.createdBy ? e.createdBy.toString() : null)),
    updatedBy: users.find((u) => u.id.toString() === (e.updatedBy ? e.updatedBy.toString() : null))
  }));
}
