import * as firebaseAdmin from "firebase-admin";
import { privateMiddleware } from "./middleware";

export interface ICreateUserInput {
  email: string;
  password: string;
}

export function createFirebaseUser(form: ICreateUserInput): Promise<firebaseAdmin.auth.UserRecord> {

  // create firebase user
  return firebaseAdmin.auth()
    .createUser({
      email: form.email,
      password: form.password
    });
}

export function deleteFirebaseUser(uid: string): Promise<void> {

  // delete user firebase
  return firebaseAdmin.auth().deleteUser(uid);
}

export function getUserFirebaseByEmail(email: string): Promise<firebaseAdmin.auth.UserRecord> {

  // get user firebase by email
  return firebaseAdmin.auth().getUserByEmail(email);
}

export interface IUserFirebaseForm {
  password?: string;
  email?: string;
}
// update information of user, but create this function to update password for user
export function updateUserFirebaseByAuthId(
  authId: string, form: IUserFirebaseForm
): Promise<firebaseAdmin.auth.UserRecord> {
  return firebaseAdmin.auth().updateUser(authId, form);
}

export function verifyFirebaseToken(token: string): Promise<string> {
  return firebaseAdmin.auth()
    .verifyIdToken(token)
    .then((user) => user.uid);
}

export function firebaseAuthMiddleware(authId?: string): any {

  return privateMiddleware(authId || verifyFirebaseToken);
}
