import { Request, Response } from "express";
import { validate, SchemaLike, ValidationError } from "joi";
import { responseError } from "../helpers";
import { Role, StatusCode, User } from "../models";
import { Action } from "../models/permissions";
import { getMessageError } from "../resources";

export type VerifyIdToken = (token: string) => Promise<string>;

export interface IRequest extends Request {
  currentUser: User;
}

export interface IPrivateMiddlewareOptions {
  authIdKey?: string;
}

// authentication

export function privateMiddleware(verify: string | VerifyIdToken, options?: IPrivateMiddlewareOptions) {

  const { authIdKey = "authId" } = { ...options };

  return async (req: IRequest, res: Response, next) => {

    // if (req.method.toLowerCase() === "options") {
    //   return next();
    // }

    const token = req.header("Authorization");

    if (!token) {
      console.log("Authorization header is required");

      return res.status(401).send({
        message: "Authorization header is required"
      });
    }

    try {
      const uid: string = typeof verify === "function" ? await verify(token) : verify;

      const user = await User.findOne({
        where: { [authIdKey]: uid },
        include: [{ model: Role }]
      });
      if (!user) {
        const message = `Can not find user with firebase ID: ${uid}`;

        console.log(message);

        return res.status(401).send({
          message
        });
      }

      if (user.status !== StatusCode.Active) {
        const message = `User is ${user.status}`;

        console.log(message);

        return res.status(401).send({
          message: `User is ${user.status}`
        });
      }
      req.currentUser = user;

      console.log("Log request:");
      console.log("Url: " + req.originalUrl);
      console.log("Method: " + req.method);
      console.log("Headers: " + JSON.stringify(req.headers, null, 2));
      console.log("Payload: " + JSON.stringify({
        body: req.body,
        params: req.params,
        query: req.query
      }, null, 2));

      return next();

    } catch (e) {
      if (!e.statusCode || e.statusCode !== 401) {
        console.error("Authorize error:", e);
      }

      console.log(e.message);

      return res.status(e.statusCode || 401).send(e || {
        message: "Unauthorized"
      });
    }

  };
}

// authorization

export function authorization(actionName: Action | Action[]) {

  return (req: IRequest, res: Response, next) => {

    const { currentUser } = req;
    if (currentUser && currentUser.superuser) {
      return next();
    }
    if (currentUser && currentUser.Role.isSuperRole) {
      return next();
    }

    if (!currentUser || !currentUser.Role) {
      return responseError(res, {
        message: "Unauthorized",
        code: "unauthorized"
      }, { statusCode: 401 });
    }

    const listActions = currentUser.Role.actions;
    if (typeof actionName === "string") {
      if (listActions.includes(actionName)) {
        return next();
      }
    } else if (Array.isArray(actionName)) {
      if (listActions.some((r: Action) => actionName.includes(r))) {
        return next();
      }
    }

    return responseError(res, {
      message: getMessageError("permission"),
      code: "permission"
    }, {
      statusCode: 403
    });
  };

}

// validate

function commonValidator(schema: SchemaLike, key: string) {

  return (req: Request, res: Response, next) => {
    const value = req[key];

    return validate(value, schema).then(() => {
      return next();
    }).catch((errors: ValidationError) => {
      const firstError = errors.details[0];
      const error = {
        code: firstError.type,
        message: firstError.message
      };

      return responseError(res, error);
    });
  };
}

export function validateParams(schema: SchemaLike) {
  return commonValidator(schema, "params");
}

export function validateBody(schema: SchemaLike) {
  return commonValidator(schema, "body");
}

export function validateQuery(schema: SchemaLike) {
  return commonValidator(schema, "query");
}

export function validateHeader(schema: SchemaLike) {
  return commonValidator(schema, "headers");
}
