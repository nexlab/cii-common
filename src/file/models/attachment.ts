import * as Sequelize from "sequelize";
import { defaultColumns, ReferenceType } from "../../models";

export enum FileType {
  Photo = "photo",
  Video = "video",
  File = "file",
}

export class Attachment extends Sequelize.Model {
  public id: number;
  public fileName: string;
  public fileType: FileType;
  public fileSize: string;
  public fileUrl: string;
  public extension: string;
  public departmentId: number;
  public referenceId: string;
  public referenceType: ReferenceType;
  public thumbnailUrl: string;
  public directoryId: string;
  public description: string;
  public type: string;
  public password: string;

  public status: string;
  public createdAt: Date;
  public createdBy: string;
  public updatedAt: Date;
  public updatedBy: string;
  public deletedAt: Date;
  public hardDeletedAt: Date;
}

export function initAttachment(sequelize: Sequelize.Sequelize) {
  Attachment.init(
    {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true,
      },
      fileName: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      fileType: {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: "",
      },
      fileSize: {
        type: Sequelize.BIGINT,
        allowNull: false,
        defaultValue: 0,
      },
      fileUrl: {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: "",
      },
      extension: {
        type: Sequelize.TEXT,
        allowNull: true,
        defaultValue: "",
      },
      thumbnailUrl: {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: "",
      },

      referenceId: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      referenceType: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      directoryId: {
        type: Sequelize.BIGINT,
        allowNull: false,
        defaultValue: 0,
      },
      departmentId: {
        type: Sequelize.BIGINT,
        allowNull: true,
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: false,
        defaultValue: "",
      },
      type: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      status: {
        type: Sequelize.TEXT,
        allowNull: true,
        defaultValue: "active",
      },
      password: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      hardDeletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      ...defaultColumns,
    },
    {
      tableName: "files",
      sequelize,
      paranoid: true,
      timestamps: true,
    }
  );
}
