import { Sequelize } from "sequelize";
import { initAttachment, Attachment, FileType } from "./attachment";
import { initFileDirectory, FileDirectory } from "./file-directory";

export {
  Attachment,
  FileDirectory,
  FileType
};

export function initDBAttachment(sequelize: Sequelize) {
  initAttachment(sequelize);
  initFileDirectory(sequelize);
}
