import * as Sequelize from "sequelize";
import { defaultColumns } from "../../models";

export class FileDirectory extends Sequelize.Model {

  public id: number;
  public name: string;
  public url: string;
  public refId: number;
  public refType: string;
  public directoryId: number;
  public departmentId: number;
  public type: string;
  public status: string;
  public createdAt: Date;
  public createdBy: string;
  public updatedAt: Date;
  public updatedBy: string;
  public deletedAt: Date;
  public hardDeletedAt: Date;
}

export function initFileDirectory(sequelize: Sequelize.Sequelize) {
  FileDirectory.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    url: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: ""
    },
    refType: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    refId: {
      type: Sequelize.BIGINT,
      allowNull: true
    },
    directoryId: {
      type: Sequelize.BIGINT,
      allowNull: true
    }, departmentId: {
      type: Sequelize.BIGINT,
      allowNull: true,
    }, type: {
      type: Sequelize.TEXT,
      allowNull: true,
    }, status: {
      type: Sequelize.TEXT,
      allowNull: true,
      defaultValue: "active"
    }, hardDeletedAt: {
      type: Sequelize.DATE,
      allowNull: true,
    },
    ...defaultColumns
  }, {
    tableName: "file_directories",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
