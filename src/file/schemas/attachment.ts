import * as Joi from "joi";
import { FileType } from "../models";

const common = {
  id: Joi.string().optional(),
  fileName: Joi.string().required(),
  fileType: Joi.string().valid([FileType.File, FileType.Photo, FileType.Video]).required(),
  fileSize: Joi.number().integer().default(0),
  fileUrl: Joi.string().required().trim(),
  thumbnailUrl: Joi.string().required().trim().allow(""),
  directoryId: Joi.number().allow(null),
  description: Joi.string().allow(""),
  extension: Joi.string(),
  createdAt: Joi.date().optional()
};

export const AttachmentInputSchema = Joi.object(common);
