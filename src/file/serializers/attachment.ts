import { Attachment } from "../models";

export function serializeAttachment(input: Attachment): any {
  return {
    fileType: input.fileType,
    fileName: input.fileName,
    fileUrl: input.fileUrl,
    fileSize: input.fileSize,
    extension: input.extension,
    thumbnailUrl: input.thumbnailUrl,
    description: input.description,
    directoryId: input.directoryId
  };
}
