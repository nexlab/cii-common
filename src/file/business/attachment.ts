import { Op } from "sequelize";
import { IUpdateOptions, ReferenceType, StatusCode } from "../../models";
import { Attachment } from "../models";

export interface IAttachmentInput {

  fileName: string;
  fileType: string;
  fileSize: string;
  fileUrl: string;
  extension: string;
  thumbnailUrl: string;
  directoryId: number;
  departmentId: number;
  description: string;
  type: string;
  referenceId: string;
  referenceType: ReferenceType;
}

export interface IAttachmentInputUpdate extends IAttachmentInput {
  id?: number;
}

export function createAttachment(
  input: IAttachmentInput, updateOptions: IUpdateOptions): Promise<Attachment> {

  input.directoryId = input.directoryId || 0;

  return <any> Attachment.create({
    ...input,
    createdBy: updateOptions.actorId,
    updatedBy: updateOptions.actorId,
    status: StatusCode.Active
  });
}

export function deleteAttachment(input: IAttachmentInputUpdate): Promise<any> {
  return <any> Attachment.update({
    status: StatusCode.Deleted
  }, {
    where: { id: input.id }
  });
}

export function findAllReferenceAttachments(
  referenceIds: Array<number | string>, referenceType: string): Promise<Attachment[]> {
  referenceIds = referenceIds.map((e) => e.toString());

  return <any> Attachment.findAll({
    where: {
      referenceType,
      referenceId: { [Op.in]: referenceIds },
      status: StatusCode.Active
    },
    order: [["id", "ASC"]]
  });
}
