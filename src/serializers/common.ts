import { safeParseInt } from "../helpers";
import { serializeSimpleUser } from "./user";

export function metadataSerializer(input: any) {
  return {
    createdAt: input.createdAt,
    createdBy: parseSimpleUser(input.createdBy),
    updatedAt: input.updatedAt,
    updatedBy: parseSimpleUser(input.updatedBy)
  };
}

function parseSimpleUser(data: any): any {
  if (data !== null && typeof data === "object" && Object.keys(data).length > 0) {
    return serializeSimpleUser(data);
  }

  return safeParseInt(data, null);
}
