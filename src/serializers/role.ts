import { Role } from "../models";

export function serializeSimpleRole(model: Role): any {
  return {
    id: model.id,
    roleName: model.roleName,
    menuItems: model.menuItems
  };
}
