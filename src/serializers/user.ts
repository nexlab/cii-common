import { safeParseInt } from "../helpers";
import { User } from "../models";

export interface ISimpleUser {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  avatarUrl: string;
  onboardDate: Date;
}

export function serializeSimpleUser(user: User): ISimpleUser {
  return user ? {
    id: safeParseInt(user.id, null),
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
    avatarUrl: user.avatarUrl,
    onboardDate: user.onboardDate,
  } : null;
}
