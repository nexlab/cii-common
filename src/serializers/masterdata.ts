import { safeParseInt } from "../helpers";
import { Company, Education, Major } from "../models";

export function serializeMajor(model: Major): any {
  return {
    id: safeParseInt(model.id, null),
    majorName: model.majorName,
    description: model.description
  };
}

export function serializeCompany(model: Company): any {
  return {
    id: safeParseInt(model.id, null),
    companyTitleName: model.companyTitleName,
    description: model.description
  };
}

export function serializeEducation(model: Education): any {
  return {
    id: safeParseInt(model.id, null),
    educationName: model.educationName,
    description: model.description
  };
}
