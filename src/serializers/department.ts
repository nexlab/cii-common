import { Department } from "../models";

export function serializeDepartment(model: Department): any {
  return {
    id: model.id,
    weight: model.weight,
    departmentName: model.departmentName,
    imageUrl: model.imageUrl,
    description: model.description,
    departmentLeader: model.departmentLeader
  };
}
