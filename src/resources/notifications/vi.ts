import { NotificationKeyType } from "./types";

export default {
  [NotificationKeyType.MessageCreate]: {
    title: "{{userName}} vừa tạo tin nhắn mới",
    content: "{{messageTitle}}"
  },
  [NotificationKeyType.MessageUpdate]: {
    title: "{{messageTitle}}",
    content: "{{userName}} vừa cập nhật nội dung tin nhắn"
  },
  [NotificationKeyType.MessageUpdateMember]: {
    title: "{{messageTitle}}",
    content: "{{userName}} vừa cập nhật danh sách thành viên trong tin nhắn"
  },
  [NotificationKeyType.MessageAddLike]: {
    title: "Thích tin nhắn",
    content: "{{userName}} đã thích tin nhắn {{messageTitle}}"
  },
  [NotificationKeyType.MessageAddComment]: {
    title: "{{messageTitle}}",
    content: "{{userName}} vừa thêm một bình luận"
  },
  [NotificationKeyType.MessageCommentAddMention]: {
    title: "{{messageTitle}}",
    content: "{{userName}} vừa đề cập đến bạn trong một bình luận"
  },
  [NotificationKeyType.MessageAddAttachment]: {
    title: "{{messageTitle}}",
    content: "{{userName}} vừa thay đổi file đính kèm"
  },
  [NotificationKeyType.MessageClose]: {
    title: "{{messageTitle}}",
    content: "{{userName}} đã đóng tin nhắn"
  },

  [NotificationKeyType.TopicCreate]: {
    title: "{{userName}} vừa tạo tin nhắn mới",
    content: "{{topicTitle}}"
  },
  [NotificationKeyType.TopicUpdate]: {
    title: "{{topicTitle}}",
    content: "{{userName}} vừa cập nhật nội dung tin nhắn"
  },
  [NotificationKeyType.TopicUpdateMember]: {
    title: "{{topicTitle}}",
    content: "{{userName}} vừa cập nhật danh sách thành viên trong tin nhắn"
  },
  [NotificationKeyType.TopicAddLike]: {
    title: "Thích tin nhắn",
    content: "{{userName}} đã thích tin nhắn {{topicTitle}}"
  },
  [NotificationKeyType.TopicAddComment]: {
    title: "{{topicTitle}}",
    content: "{{userName}} vừa thêm một bình luận"
  },
  [NotificationKeyType.TopicCommentAddMention]: {
    title: "{{topicTitle}}",
    content: "{{userName}} vừa đề cập đến bạn trong một bình luận"
  },
  [NotificationKeyType.TopicAddAttachment]: {
    title: "{{topicTitle}}",
    content: "{{userName}} vừa thay đổi file đính kèm"
  },
  [NotificationKeyType.TopicClose]: {
    title: "{{topicTitle}}",
    content: "{{userName}} đã đóng tin nhắn"
  },

  [NotificationKeyType.PlanUpdate]: {
    title: "Cập nhật kế hoạch",
    content: "{{userName}} đã cập nhật kế hoạch '{{planName}}'"
  },
  [NotificationKeyType.PlanDelete]: {
    title: "Xoá kế hoạch",
    content: "{{userName}} đã xoá kế hoạch '{{planName}}'"
  },
  [NotificationKeyType.PlanItemUpdate]: {
    title: "Cập nhật hạng mục kế hoạch",
    content: "{{userName}} đã cập nhật hạng mục kế hoạch '{{planItemName}}'"
  },
  [NotificationKeyType.PlanItemDelete]: {
    title: "Xoá hạng mục kế hoạch",
    content: "{{userName}} đã xoá hạng mục kế hoạch '{{planItemName}}'"
  },

  [NotificationKeyType.RemindPlan]: {
    title: "Nhắc nhở",
    content: "Hạng mục kế hoạch '{{planItemTitle}}' sắp hết hạn"
  },
  [NotificationKeyType.RemindTask]: {
    title: "Nhắc nhở",
    content: "Nhiệm vụ'{{taskTitle}}' sắp hết hạn"
  },

  [NotificationKeyType.ProjectCreate]: {
    title: "{{userName}} vừa tạo bảng công việc",
    content: "{{projectName}}"
  },
  [NotificationKeyType.ProjectUpdate]: {
    title: "{{userName}} vừa cập nhật bảng công việc",
    content: "{{projectName}}"
  },
  [NotificationKeyType.ProjectClose]: {
    title: "{{userName}} vừa đóng bảng công việc",
    content: "{{projectName}}"
  },

  [NotificationKeyType.ActivityTaskCreate]: {
    title: "{{userName}} vừa tạo một công việc mới'",
    content: "{{taskTitle}}"
  },
  [NotificationKeyType.ActivityTaskEditTitle]: {
    title: "{{userName}} vừa thay đổi tiêu đề công việc'",
    content: "{{taskTitle}}"
  },
  [NotificationKeyType.ActivityTaskEditContent]: {
    title: "{{userName}} vừa thay đổi nội dung công việc'",
    content: "{{taskTitle}}"
  },
  [NotificationKeyType.ActivityTaskEditStatus]: {
    title: "{{userName}} vừa thay đổi trạng thái công việc",
    content: "{{taskTitle}}"
  },
  [NotificationKeyType.ActivityTaskEditDeadline]: {
    title: "{{userName}} vừa thay đổi thời hạn của công việc",
    content: "{{taskTitle}}"
  },
  [NotificationKeyType.ActivityTaskEditTaskGroup]: {
    title: "{{userName}} vừa thay đổi nhóm công việc",
    content: "{{taskTitle}}"
  },
  [NotificationKeyType.ActivityTaskEditEstimation]: {
    title: "{{userName}} vừa thay đổi ước lượng công việc",
    content: "{{taskTitle}}"
  },
  [NotificationKeyType.ActivityTaskUpdateAttachment]: {
    title: "{{userName}} vừa cập nhật file đính kèm",
    content: "{{taskTitle}}"
  },
  [NotificationKeyType.ActivityTaskChangeAssignee]: {
    title: "{{userName}} vừa thay đổi người được phân công",
    content: "{{taskTitle}}"
  },
  [NotificationKeyType.ActivityTaskAddComment]: {
    title: "{{userName}} vừa thêm một bình luận",
    content: "{{taskTitle}}"
  },
  [NotificationKeyType.ActivityTaskChangeReporter]: {
    title: "{{userName}} vừa thay đổi người phụ trách công việc",
    content: "{{taskTitle}}"
  },
  [NotificationKeyType.ActivityTaskChangeRating]: {
    title: "{{userName}} vừa đánh giá cho công việc",
    content: "{{taskTitle}}"
  },
  [NotificationKeyType.ActivityTaskEditPrivate]: {
    title: "{{userName}} vừa bật/tắt tính năng bảo mật cho công việc",
    content: "{{taskTitle}}"
  },
};
