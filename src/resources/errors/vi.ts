import { ErrorKey } from "./types";

export default {
  [ErrorKey.RecordNotFound]: "Không tìm thấy dữ liệu",
  error_unknown: "Lỗi không xác định",
  permission: "Không có quyền truy cập",
  [ErrorKey.UpdateProjectTemplate]: "Không thể bỏ trạng thái hoặc xoá khi mẫu dự án đã có dự án",
  [ErrorKey.UpdateTaskRating]: "Không thể bỏ trạng thái hoặc xoá khi đánh giá đã được gắn nhiệm vụ",
  [ErrorKey.ProjectTaskPrivate]: "Công việc đã được bảo mật"
};
