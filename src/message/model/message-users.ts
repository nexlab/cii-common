import * as Sequelize from "sequelize";

export class MessageUsers extends Sequelize.Model {
  public id: number;
  public userId: number;
  public messageIdsUnRead: string[];
}

export function initMessageUsers(sequelize: Sequelize.Sequelize) {
  MessageUsers.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: Sequelize.BIGINT,
      allowNull: false,

      references: {
        model: "users",
        key: "id"
      }
    },
    messageIdsUnRead: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: Sequelize.NOW,
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: Sequelize.NOW,
    },
    deletedAt: {
      type: Sequelize.DATE,
      allowNull: true,
    }
  }, {
    tableName: "message_users",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
