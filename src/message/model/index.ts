import * as Sequelize from "sequelize";
import { initSimpleMessage } from "./message";
import { initMessageUsers } from "./message-users";

export * from "./message";
export * from "./message-users";

export function initDBMessageToServeNotification(sequelize: Sequelize.Sequelize) {
  initSimpleMessage(sequelize);
  initMessageUsers(sequelize);
}
