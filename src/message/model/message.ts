import * as Sequelize from "sequelize";
import { defaultColumns, StatusCode } from "../../models";

export class SimpleMessage extends Sequelize.Model {

  public id: number;
  public title: string;
  public content: string;
  public categoryId: number;
  public memberIds: Array<number | string>;
  public subscribers: Array<number | string>;
  public labels: string[];
  public attachmentIds: number[];
  public isClosed: boolean;

  public createdAt: Date;
  public createdBy: number | string;
  public updatedAt: Date;
  public updatedBy: number | string;
  public deletedAt: Date;

  public status: StatusCode;
}

export function initSimpleMessage(sequelize: Sequelize.Sequelize) {

  SimpleMessage.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    content: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    categoryId: {
      type: Sequelize.BIGINT,
      allowNull: false
    },
    memberIds: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },
    subscribers: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },
    labels: {
      type: Sequelize.ARRAY(Sequelize.TEXT),
      allowNull: false,
      defaultValue: []
    },
    attachmentIds: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },
    isClosed: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },

    activities: {
      type: Sequelize.JSON,
      allowNull: false,
      defaultValue: "{}",
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    ...defaultColumns
  }, {
    tableName: "messages",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
