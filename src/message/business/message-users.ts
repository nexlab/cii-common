import * as lodash from "lodash";
import { MessageUsers } from "../model/message-users";

export interface ICreateMessageUsers {
  memberIds: Array<string | number>;
  messageId: string;
}

export async function createMessageUsers(data: ICreateMessageUsers): Promise<any> {
  try {
    const messageUsers = await MessageUsers.findAll({
      where: {
        userId: data.memberIds
      }
    });

    function parseIds(reverse: boolean = false) {
      return messageUsers.filter((e) => {
        const current = data.memberIds.find((a) => a.toString() === e.userId.toString());
        if (current) {
          return reverse;
        }

        return !reverse;
      });
    }

    const messageUsersNeedCreate = data.memberIds.filter((e) => {
      const current = messageUsers.find((a) => e.toString() === a.userId.toString());
      if (current) {
        return false;
      }

      return true;
    });
    const messageUsersNeedUpdate = parseIds(true);

    if (messageUsersNeedCreate && messageUsersNeedCreate.length > 0) {
      await Promise.all(
        messageUsersNeedCreate.map((e) => {
          return MessageUsers.create({
            userId: e,
            messageIdsUnRead: [data.messageId]
          });
        })
      );
    }

    if (messageUsersNeedUpdate && messageUsersNeedUpdate.length > 0) {
      await Promise.all(
        messageUsersNeedUpdate.map((e) => {
          e.messageIdsUnRead.push(data.messageId);

          return MessageUsers.update({
            messageIdsUnRead: lodash.uniq(e.messageIdsUnRead)
          }, {
            where: { userId: e.userId, }
          });
        })
      );
    }

    return;
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function markMessageUserAsRead(userId: number | string): Promise<number[]> {

  const current = await MessageUsers.find({
    where: { userId }
  });
  let result = [];
  if (current) {
    result = current.messageIdsUnRead || [];
  }
  if (result.length > 0) {
    await MessageUsers.update({ messageIdsUnRead: [] }, {
      where: { userId }
    });
  }

  return result;
}
