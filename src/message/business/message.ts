import { MessageUsers } from "../model/message-users";

export async function countNewMessageUser(userId: string | number): Promise<number> {
  const current = await MessageUsers.find({
    where: { userId }
  });

  if (!current) {
    return 0;
  }

  return current.messageIdsUnRead.length;
}
