import * as Sequelize from "sequelize";

export class TopicUsers extends Sequelize.Model {
  public id: number;
  public userId: number;
  public topicIdsUnRead: string[];
}

export function initTopicUsers(sequelize: Sequelize.Sequelize) {
  TopicUsers.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: Sequelize.BIGINT,
      allowNull: false,

      references: {
        model: "users",
        key: "id"
      }
    },
    topicIdsUnRead: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false
    },
    createdAt: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: Sequelize.NOW,
    },
    updatedAt: {
      type: Sequelize.DATE,
      allowNull: true,
      defaultValue: Sequelize.NOW,
    },
    deletedAt: {
      type: Sequelize.DATE,
      allowNull: true,
    }
  }, {
    tableName: "topic_users",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
