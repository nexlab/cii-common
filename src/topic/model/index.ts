import * as Sequelize from "sequelize";
import { initTopicUsers } from "./topic-users";

export * from "./topic-users";

export function initDBTopicToServeNotification(sequelize: Sequelize.Sequelize) {
  initTopicUsers(sequelize);
}
