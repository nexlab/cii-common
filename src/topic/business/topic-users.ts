import * as lodash from "lodash";
import { TopicUsers } from "../model/topic-users";

export interface ICreateTopicUsers {
  memberIds: Array<string | number>;
  topicId: string;
}

export async function createTopicUsers(data: ICreateTopicUsers): Promise<any> {
  try {
    const topicUsers = await TopicUsers.findAll({
      where: {
        userId: data.memberIds
      }
    });

    function parseIds(reverse: boolean = false) {
      return topicUsers.filter((e) => {
        const current = data.memberIds.find((a) => a.toString() === e.userId.toString());
        if (current) {
          return reverse;
        }

        return !reverse;
      });
    }

    const topicUsersNeedCreate = data.memberIds.filter((e) => {
      const current = topicUsers.find((a) => e.toString() === a.userId.toString());
      if (current) {
        return false;
      }

      return true;
    });
    const topicUsersNeedUpdate = parseIds(true);

    if (topicUsersNeedCreate && topicUsersNeedCreate.length > 0) {
      await Promise.all(
        topicUsersNeedCreate.map((e) => {
          return TopicUsers.create({
            userId: e,
            topicIdsUnRead: [data.topicId]
          });
        })
      );
    }

    if (topicUsersNeedUpdate && topicUsersNeedUpdate.length > 0) {
      await Promise.all(
        topicUsersNeedUpdate.map((e) => {
          e.topicIdsUnRead.push(data.topicId);

          return TopicUsers.update({
            topicIdsUnRead: lodash.uniq(e.topicIdsUnRead)
          }, {
            where: { userId: e.userId, }
          });
        })
      );
    }

    return;
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function markTopicUserAsRead(userId: number | string): Promise<number[]> {

  const current = await TopicUsers.find({
    where: { userId }
  });
  let result = [];
  if (current) {
    result = current.topicIdsUnRead || [];
  }
  if (result.length > 0) {
    await TopicUsers.update({ topicIdsUnRead: [] }, {
      where: { userId }
    });
  }

  return result;
}
