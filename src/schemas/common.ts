import { number, string, StringSchema } from "joi";

export const statusSchema: StringSchema = string().valid(["active", "deleted"]);
export const genderSchema: StringSchema = string().valid(["male", "female"]);
export const paginationSchema: any = {
  page: number(),
  size: number()
};
