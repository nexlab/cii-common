import * as Sequelize from "sequelize";
import { ReferenceType, User } from "../../models";

export interface IInputUserNotificationSetting {
  referenceId: number;
  type: ReferenceType;
  setting: object;
}

export interface IUserNotificationSetting {
  email: boolean;
  appPush: boolean;
}

export class UserNotificationSetting extends Sequelize.Model {
  public id: number;
  public userId: string;
  public referenceId: number;
  public type: ReferenceType;
  public setting: IUserNotificationSetting;

  public user?: User;
}

export function initUserNotificationSetting(sequelize: Sequelize.Sequelize) {
  UserNotificationSetting.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: Sequelize.BIGINT,
      allowNull: false,

      references: {
        model: "users",
        key: "id"
      }
    },
    type: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    referenceId: {
      type: Sequelize.BIGINT,
      allowNull: false
    },
    setting: {
      type: Sequelize.JSON,
      allowNull: false
    }
  }, {
    tableName: "user_notification_setting",
    sequelize,
    timestamps: false
  });

  UserNotificationSetting.belongsTo(User, {
    as: "user",
    foreignKey: "userId"
  });
}
