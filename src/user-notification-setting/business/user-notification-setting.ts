import { Op } from "sequelize";
import { ReferenceType } from "../../models";
import { IInputUserNotificationSetting, UserNotificationSetting } from "../models/user-notification-setting";

export async function upsertUserNotificationSetting(userId: string, data: IInputUserNotificationSetting) {
  const current = await UserNotificationSetting.findOne({
    where: {
      userId,
      type: data.type,
      referenceId: data.referenceId
    }
  });

  if (!current) {
    await UserNotificationSetting.create({
      userId,
      type: data.type,
      referenceId: data.referenceId,
      setting: data.setting
    });
  } else {
    await UserNotificationSetting.update({
      setting: data.setting
    }, {
      where: { id: current.id }
    });
  }
}

export function defaultSettingForEntire() {
  return {
    email: true,
    appPush: true
  };
}

export async function mappingUserNotificationForReference(
  userId: string, type: ReferenceType, model: any[]) {
  const ids = model.map((e) => e.id);
  const userNotifications = await UserNotificationSetting.findAll({
    where: {
      referenceId: {
        [Op.in]: ids
      },
      userId,
      type
    }
  });

  return model.map((e) => {
    const current = userNotifications.find((a) => a.referenceId.toString() === e.id.toString());
    e.setting = current ? current.setting : defaultSettingForEntire();

    return e;
  });

}
