import * as Joi from "joi";
import { ReferenceType } from "../../models";

export const UserNotificationSettingSchema = Joi.object({
  setting: Joi.object({
    email: Joi.boolean().required(),
    appPush: Joi.boolean().required()
  }).required(),
  type: Joi.string().valid(
    [ReferenceType.Message, ReferenceType.Plan, ReferenceType.Task, ReferenceType.TaskComment , ReferenceType.Project, ReferenceType.MessageComment, ReferenceType.Topic, ReferenceType.TopicComment]
  ).required(),
  referenceId: Joi.number().required()
});
