import { Response } from "express";
import { FindAndCountOptions, Model } from "sequelize";
import { IRequest } from "../../authentication";
import { responseError } from "../../helpers";
import { filterAll, filterPagination, parsePaginationParams, StatusCode } from "../../models";

export interface ICRUDForModule {
  create(): Promise<any>;
  update(): Promise<any>;
  filter(): Promise<any>;
  destroy(): Promise<any>;
  get(): Promise<any>;
}

export enum FunctionNameCrud {
  create = "create",
  update = "update",
  filter = "filter",
  destroy = "destroy",
  get = "get"
}

export interface IOptionCRUDModule {
  status?: boolean;
  createdBy?: boolean;
  updatedBy?: boolean;
}

export function crudForModule<M extends Model>(
  req: IRequest, res: Response,
  model: { new(): M } & typeof Model, serializer: any,
  options?: IOptionCRUDModule): ICRUDForModule {

  async function create() {
    try {
      const form = req.body;
      if (options && options.createdBy) {
        const user = req.currentUser;
        form.createdBy = user.id;
      }
      const result = await model.create(form);

      return res.json(serializer(result));
    } catch (error) {
      return responseError(res, error);
    }
  }

  async function update() {
    try {
      const form = req.body;
      const id = req.params.id;
      if (options && options.updatedBy) {
        const user = req.currentUser;
        form.updatedBy = user.id;
      }
      const result = await model.update(form, {
        where: { id },
        returning: true
      });

      return res.json(serializer(result[1][0]));
    } catch (error) {
      return responseError(res, error);
    }
  }

  async function filter() {
    try {
      const query = req.query;
      const filterWhere: FindAndCountOptions = {};
      if (options && options.status) {
        filterWhere.where = {
          status: StatusCode.Active
        };
      }
      const result = query.size && query.page
        ? await filterPagination(model, filterWhere, parsePaginationParams(query))
        : await filterAll(model, filterWhere, parsePaginationParams(query));
      result.data = result.data.map(serializer);

      return res.json(result);
    } catch (error) {
      return responseError(res, error);
    }
  }

  async function destroy() {
    try {
      const id = req.params.id;
      if (options && options.status) {
        await model.update({
          status: StatusCode.Deleted
        }, {
          where: { id }
        });

      } else {
        await model.destroy({
          where: { id },
          force: true
        });
      }

      return res.json({
        message: "Delete entire successfully"
      });
    } catch (error) {
      return responseError(res, error);
    }
  }

  async function get() {
    try {
      const id = req.params.id;
      const result = await model.findById(id);

      return res.json(serializer(result));
    } catch (error) {
      return responseError(res, error);
    }
  }

  return {
    create,
    update,
    filter,
    destroy,
    get
  };

}
