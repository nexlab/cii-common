import * as Sequelize from "sequelize";
import { defaultColumns, StatusCode } from "./common";

export class ProjectCategory extends Sequelize.Model {

  public id: string;
  public name: string;
  public description: string;
  public status: StatusCode;

  public createdAt: Date;
  public createdBy: string;
  public updatedAt: Date;
  public updatedBy: string;
  public deletedAt: Date;
}

export function initProjectCategory(sequelize: Sequelize.Sequelize) {
  ProjectCategory.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    },

    status: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: StatusCode.Active
    },
    ...defaultColumns
  }, {
    tableName: "project_categories",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
