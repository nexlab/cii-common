import * as Sequelize from "sequelize";
import { defaultColumns, StatusCode } from "./common";
import { SimpleProjectTemplate } from "./project-template";

export enum MyProjectFilter {
  All = "all",
  Owner = "owner",
  OnlyMember = "onlyMember",
  Closed = "closed",
  ExpiryDate = "expiryDate"
}

export class SimpleProject extends Sequelize.Model {

  public id: string;
  public projectCode: string;
  public name: string;
  public description: string;
  public avatarUrl: string;
  public startDate: Date;

  public endDate: Date;
  public leaderId: string;
  public categoryId: string;
  public templateId: string;
  public memberIds: string[];
  public subscribers: string[];
  public reminder: boolean;
  public isClosed: boolean;
  public remindTime: number;
  public numberOfTask?: number;

  public status: StatusCode;
  public ratingId: string;
  public template: SimpleProjectTemplate;

  public createdAt: Date;
  public createdBy: string;
  public updatedAt: Date;
  public updatedBy: string;
  public deletedAt: Date;
}

export function initSimpleProject(sequelize: Sequelize.Sequelize) {
  SimpleProject.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    projectCode: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    name: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    avatarUrl: {
      type: Sequelize.TEXT,
      allowNull: true
    },

    startDate: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    endDate: {
      type: Sequelize.DATE,
      allowNull: true,
    },
    leaderId: {
      type: Sequelize.BIGINT,
      allowNull: false,
    },
    categoryId: {
      type: Sequelize.BIGINT,
      allowNull: false
    },
    templateId: {
      type: Sequelize.BIGINT,
      allowNull: true,

      references: {
        model: SimpleProjectTemplate,
        key: "id"
      }
    },
    memberIds: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },
    subscribers: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },

    isClosed: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    reminder: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    remindTime: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 24
    },

    status: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: StatusCode.Active
    },
    ratingId: {
      type: Sequelize.BIGINT,
      allowNull: true
    },
    ...defaultColumns
  }, {
    tableName: "projects",
    sequelize,
    paranoid: true,
    timestamps: true,
  });

  SimpleProject.belongsTo(SimpleProjectTemplate, {
    foreignKey: "templateId",
    as: "template"
  });

}
