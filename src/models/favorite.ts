import * as Sequelize from "sequelize";
import { ReferenceType, StatusCode } from "./common";

export class Favorite extends Sequelize.Model {

  public id: string;
  public avatarUrl: string;
  public type: ReferenceType;
  public relateId: string;
  public userId: string;
  public status: StatusCode;

}

export function initFavorite(sequelize: Sequelize.Sequelize) {
  Favorite.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    avatarUrl: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    type: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    relateId: {
      type: Sequelize.BIGINT,
      allowNull: false
    },
    userId: {
      type: Sequelize.BIGINT,
      allowNull: false
    },
    metadata: {
      type: Sequelize.JSON,
      allowNull: false,
      defaultValue: {}
    },
    title: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: StatusCode.Active
    }
  }, {
    tableName: "favorites",
    sequelize,
    timestamps: false,
  });
}
