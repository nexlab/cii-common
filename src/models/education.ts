import * as Sequelize from "sequelize";
import { defaultColumns } from "./common";

export class Education extends Sequelize.Model {

  public id: number;
  public educationName: string;
  public description: string;

  // public createdAt: Date;
  // public updatedAt: Date;
  // public deletedAt: Date;

}

export function initEducation(sequelize: Sequelize.Sequelize) {
  Education.init({
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    educationName: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    ...defaultColumns
  }, {
    tableName: "educations",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
