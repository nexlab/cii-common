export enum Action {
  UserCreate = "user.create",
  UserUpdate = "user.update",
  UserDelete = "user.delete",
  UserGet = "user.get",
  UserList = "user.list",
  UserPicker = "user.picker",
  UserChangePassword = "user.change-password",

  RoleCreate = "role.create",
  RoleUpdate = "role.update",
  RoleDelete = "role.delete",
  RoleGet = "role.get",
  RoleList = "role.list",
  RoleListAction = "role.listAction",

  DepartmentCreate = "department.create",
  DepartmentUpdate = "department.update",
  DepartmentDelete = "department.delete",
  DepartmentGet = "department.get",
  DepartmentList = "department.list",

  EducationCreate = "education.create",
  EducationUpdate = "education.update",
  EducationDelete = "education.delete",
  EducationGet = "education.get",
  EducationList = "education.list",

  MajorCreate = "major.create",
  MajorUpdate = "major.update",
  MajorDelete = "major.delete",
  MajorGet = "major.get",
  MajorList = "major.list",

  CompanyCreate = "company-title.create",
  CompanyUpdate = "company-title.update",
  CompanyDelete = "company-title.delete",
  CompanyGet = "company-title.get",
  CompanyList = "company-title.list",

  MessageCreate = "message.create",
  MessageUpdate = "message.update",
  MessageDelete = "message.delete",
  MessageGet = "message.get",
  MessageList = "message.list",

  MessageCategoryCreate = "message-category.create",
  MessageCategoryUpdate = "message-category.update",
  MessageCategoryDelete = "message-category.delete",
  MessageCategoryGet = "message-category.get",
  MessageCategoryList = "message-category.list",

  TopicCreate = "topic.create",
  TopicUpdate = "topic.update",
  TopicDelete = "topic.delete",
  TopicGet = "topic.get",
  TopicList = "topic.list",

  TopicCategoryCreate = "topic-category.create",
  TopicCategoryUpdate = "topic-category.update",
  TopicCategoryDelete = "topic-category.delete",
  TopicCategoryGet = "topic-category.get",
  TopicCategoryList = "topic-category.list",

  PlanCreate = "plan.create",
  PlanUpdate = "plan.update",
  PlanDelete = "plan.delete",
  PlanGet = "plan.get",
  PlanList = "plan.list",
  PlanOwn = "plan.own",

  PlanItemCreate = "plan-item.create",
  PlanItemUpdate = "plan-item.update",
  PlanItemDelete = "plan-item.delete",
  PlanItemGet = "plan-item.get",
  PlanItemList = "plan-item.list",

  PlanItemGroupCreate = "plan-item-group.create",
  PlanItemGroupUpdate = "plan-item-group.update",
  PlanItemGroupDelete = "plan-item-group.delete",
  PlanItemGroupGet = "plan-item-group.get",
  PlanItemGroupList = "plan-item-group.list",

  ProjectCreate = "project.create",
  ProjectUpdate = "project.update",
  ProjectDelete = "project.delete",
  ProjectGet = "project.get",
  ProjectList = "project.list",
  ProjectOwner = "project.owner",

  ProjectTemplateCreate = "project-template.create",
  ProjectTemplateUpdate = "project-template.update",
  ProjectTemplateDelete = "project-template.delete",
  ProjectTemplateGet = "project-template.get",
  ProjectTemplateList = "project-template.list",

  ProjectCategoryCreate = "project-category.create",
  ProjectCategoryUpdate = "project-category.update",
  ProjectCategoryDelete = "project-category.delete",
  ProjectCategoryGet = "project-category.get",
  ProjectCategoryList = "project-category.list",

  TaskCreate = "task.create",
  TaskUpdate = "task.update",
  TaskDelete = "task.delete",
  TaskGet = "task.get",
  TaskList = "task.list",
  TaskCommentCreate = "task-comment.create",
  TaskCommentUpdate = "task-comment.update",
  TaskCommentDelete = "task-comment.delete",
  TaskByProject = "task.by-project",
  TaskMove = "task.move",

  TaskGroupCreate = "task-group.create",
  TaskGroupUpdate = "task-group.update",
  TaskGroupDelete = "task-group.delete",
  TaskGroupGet = "task-group.get",
  TaskGroupList = "task-group.list",
  TaskGroupByProject = "task-group.by-project",

  TaskRatingCreate = "task-rating.create",
  TaskRatingUpdate = "task-rating.update",
  TaskRatingDelete = "task-rating.delete",
  TaskRatingGet = "task-rating.get",
  TaskRatingList = "task-rating.list",

  FeedbackUpdateResolved = "feedback.update-resolved",

  ReportDepartmentList = "report.department-list",
  ReportUserList = "report.user-list",
  ReportDepartmentDetail = "report.department-detail",
  ReportUserDetail = "report.user-detail",
  ReportDepartmentProjectList = "report.department-project-list",
  ReportDepartmentUserList = "report.department-user-list",
  ReportUserTaskList = "report.user-task-list",
  FileList = "file.list",
  FileDirectoryList = "file.directory-list",
  FileDirectoryDetail = "file.directory-detail",
  FileDirectoryCreate = "file.directory-create",
  FileDirectoryEdit = "file.directory-edit",
  FileDirectoryDelete = "file.directory-delete",
  FileFileList = "file.file-list",
  FileFileDetail = "file.file-detail",
  FileFileCreate = "file.file-create",
  FileFileEdit = "file.file-edit",
  FileFileDelete = "file.file-delete",
}

export const ActionGroups = {
  user: [
    Action.UserCreate, Action.UserGet,
    Action.UserUpdate, Action.UserDelete,
    Action.UserList, Action.UserChangePassword
  ],
  role: [
    Action.RoleCreate, Action.RoleDelete,
    Action.RoleGet, Action.RoleList,
    Action.RoleListAction, Action.RoleUpdate
  ],
  department: [
    Action.DepartmentCreate, Action.DepartmentDelete,
    Action.DepartmentList, Action.DepartmentUpdate,
    Action.DepartmentGet
  ],
  education: [
    Action.EducationCreate, Action.EducationDelete,
    Action.EducationGet, Action.EducationList,
    Action.EducationUpdate
  ],
  major: [
    Action.MajorCreate, Action.MajorDelete,
    Action.MajorGet, Action.MajorList,
    Action.MajorUpdate
  ],
  company: [
    Action.CompanyCreate, Action.CompanyDelete,
    Action.CompanyGet, Action.CompanyList,
    Action.CompanyUpdate
  ],

  message: [
    Action.MessageCreate, Action.MessageDelete,
    Action.MessageGet, Action.MessageList,
    Action.MessageUpdate
  ],
  "message-category": [
    Action.MessageCategoryCreate, Action.MessageCategoryDelete,
    Action.MessageCategoryGet,
    Action.MessageCategoryList,
    Action.MessageCategoryUpdate
  ],
  plan: [
    Action.PlanCreate,
    Action.PlanDelete,
    Action.PlanGet,
    Action.PlanList,
    Action.PlanUpdate,
    Action.PlanOwn
  ],
  "plan-item-group": [
    Action.PlanItemGroupCreate,
    Action.PlanItemGroupDelete,
    Action.PlanItemGroupGet,
    Action.PlanItemGroupList,
    Action.PlanItemGroupUpdate
  ],
  "plan-item": [
    Action.PlanItemCreate,
    Action.PlanItemDelete,
    Action.PlanItemGet,
    Action.PlanItemList,
    Action.PlanItemUpdate
  ],
  project: [
    Action.ProjectCreate,
    Action.ProjectDelete,
    Action.ProjectGet,
    Action.ProjectList,
    Action.ProjectOwner,
    Action.ProjectUpdate
  ],
  "project-category": [
    Action.ProjectCategoryCreate,
    Action.ProjectCategoryDelete,
    Action.ProjectCategoryGet,
    Action.ProjectCategoryList,
    Action.ProjectCategoryUpdate
  ],
  "project-template": [
    Action.ProjectTemplateCreate,
    Action.ProjectTemplateDelete,
    Action.ProjectTemplateGet,
    Action.ProjectTemplateList,
    Action.ProjectTemplateUpdate
  ],
  "task-rating": [
    Action.TaskRatingCreate,
    Action.TaskRatingDelete,
    Action.TaskRatingGet,
    Action.TaskRatingList,
    Action.TaskRatingUpdate
  ],
  "task-group": [
    Action.TaskGroupCreate,
    Action.TaskGroupDelete,
    Action.TaskGroupGet,
    Action.TaskGroupList,
    Action.TaskGroupUpdate,
    Action.TaskGroupByProject
  ],
  task: [
    Action.TaskCreate,
    Action.TaskDelete,
    Action.TaskCommentCreate,
    Action.TaskCommentUpdate,
    Action.TaskCommentDelete,
    Action.TaskGet,
    Action.TaskList,
    Action.TaskUpdate,
    Action.TaskByProject,
    Action.TaskMove
  ],
  feedback: [
    Action.FeedbackUpdateResolved
  ],
  report: [
    Action.ReportDepartmentList,
    Action.ReportUserList,
    Action.ReportDepartmentDetail,
    Action.ReportUserDetail,
    Action.ReportDepartmentProjectList,
    Action.ReportDepartmentUserList,
    Action.ReportUserTaskList,
  ],
  file: [
    Action.FileDirectoryList,
    Action.FileDirectoryDetail,
    Action.FileDirectoryCreate,
    Action.FileDirectoryEdit,
    Action.FileDirectoryDelete,
    Action.FileFileList,
    Action.FileFileDetail,
    Action.FileFileCreate,
    Action.FileFileEdit,
    Action.FileFileDelete,
  ],
};

export enum MenuItem {
  StaffManagement = "staff_management",
  DataManagement = "data_management",
  PlanSchedule = "plan_schedule",
  ProjectManagement = "project_management",
  NotificationSystem = "notification_system",
  ReportExtract = "report_extract",
  InternalNews = "internal_news",
  InterDepartmental = "inter-departmental",
  MessageSystem = "message_system",
  Setting = "setting"
}

export const MenuItems = [
  MenuItem.StaffManagement, MenuItem.DataManagement,
  MenuItem.PlanSchedule, MenuItem.ProjectManagement,
  MenuItem.NotificationSystem, MenuItem.ReportExtract,
  MenuItem.InternalNews, MenuItem.InterDepartmental,
  MenuItem.MessageSystem, MenuItem.Setting
];

export const Permissions = {
  Admin: "admin",
  User: "user"
};
