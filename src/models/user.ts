import * as Sequelize from "sequelize";
import { defaultColumns, Gender, StatusCode } from "./common";
import { Company } from "./company";
import { Department } from "./department";
import { Education } from "./education";
import { Major } from "./major";
import { Role } from "./role";

export class User extends Sequelize.Model {

  public id: string;
  public email: string;
  public username: string;
  public phoneCode: string;
  public phoneNumber: string;

  public firstName: string;
  public lastName: string;
  public birthday: Date;
  public avatarUrl: string;
  public authId: string;
  public superuser: boolean;
  public homeTown: string;
  public address: string;
  public description: string;
  public gender: Gender;
  public onboardDate: Date;
  public identifierNumber: string;
  public notificationEmail: string;

  public departments?: Department[];
  public departmentIds: number[];

  public Role?: Role;
  public roleId: number;

  public company?: Company;
  public companyId: number;

  public Education?: Education;
  public educationId: number;

  public Major?: Major;
  public majorId: number;

  public metadata: any;
  public status: StatusCode;

  public createdAt: Date;
  public updatedAt: Date;
  public deletedAt: Date;

}

export function initUser(sequelize: Sequelize.Sequelize) {
  User.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    email: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    username: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    phoneCode: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    phoneNumber: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    firstName: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    lastName: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    gender: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    notificationEmail: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    homeTown: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    address: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    birthday: {
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    onboardDate: {
      type: Sequelize.DATEONLY,
      allowNull: true
    },
    avatarUrl: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    identifierNumber: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    authId: {
      type: Sequelize.TEXT,
      allowNull: false,
      unique: true
    },
    roleId: {
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: "roles",
        key: "id"
      }
    },
    companyId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    majorId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    educationId: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    departmentIds: {
      type: Sequelize.ARRAY(Sequelize.INTEGER),
      allowNull: true
    },
    superuser: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    metadata: {
      type: Sequelize.JSON,
      allowNull: true,
      defaultValue: {}
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: "active"
    },
    ...defaultColumns
  }, {
    tableName: "users",
    sequelize,
    paranoid: true,
    timestamps: true,
  });

  User.belongsTo(Role, { foreignKey: "roleId" });
  User.belongsTo(Company, { foreignKey: "companyId", as: "company" });
}

export function getFullName(user: User) {
  return `${user.firstName} ${user.lastName}`;
}
