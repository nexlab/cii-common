import * as Sequelize from "sequelize";
import { defaultColumns } from "./common";
import { SimpleProject } from "./project";
import { SimpleTaskGroup } from "./task-group";

export interface ITaskActivity {
  description: string;
  userId: string;
  createdAt: Date;
  previousData?: string;
  newData?: string;
}

export class SimpleTask extends Sequelize.Model {

  public id: string;
  public taskCode: string;
  public title: string;
  public description: string;
  public projectId: string;

  public groupId: string;
  public startDate: Date;
  public endDate: Date;
  public estimation: number;
  public reporterId: string;
  public assigneeId: string;
  public deadline: Date;

  public labels: string[];
  public memberIds: string[];
  public readers: string[];
  public activities: ITaskActivity[];
  public numberOfComment?: number;
  public numberOfAttachment?: number;

  public project?: SimpleProject;
  public group?: SimpleTaskGroup;

  public taskStatus: string;
  public rating: string;
  public isPrivate: boolean;

  public createdAt: Date;
  public createdBy: string;
  public updatedAt: Date;
  public updatedBy: string;
  public deletedAt: Date;
}

export function initSimpleTask(sequelize: Sequelize.Sequelize) {
  SimpleTask.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    taskCode: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    title: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    taskStatus: {
      type: Sequelize.TEXT,
      allowNull: false
    },

    projectId: {
      type: Sequelize.BIGINT,
      allowNull: true,

      references: {
        model: SimpleProject,
        key: "id"
      }
    },
    groupId: {
      type: Sequelize.BIGINT,
      allowNull: true,

      references: {
        model: SimpleTaskGroup,
        key: "id"
      }
    },
    startDate: {
      type: Sequelize.DATE,
      allowNull: true,
    },
    endDate: {
      type: Sequelize.DATE,
      allowNull: true,
    },
    deadline: {
      type: Sequelize.DATE,
      allowNull: true,
    },
    estimation: {
      type: Sequelize.FLOAT,
      allowNull: false,
      defaultValue: 0
    },
    reporterId: {
      type: Sequelize.BIGINT,
      allowNull: true,
    },
    assigneeId: {
      type: Sequelize.BIGINT,
      allowNull: true,
    },
    labels: {
      type: Sequelize.ARRAY(Sequelize.TEXT),
      allowNull: false,
      defaultValue: [],
    },
    memberIds: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },
    activities: {
      type: Sequelize.JSON,
      allowNull: false,
      defaultValue: {},
    },
    isPrivate: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    rating: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    readers: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },
    ...defaultColumns
  }, {
    tableName: "tasks",
    sequelize,
    paranoid: true,
    timestamps: true,
  });

  SimpleTask.belongsTo(SimpleProject, {
    as: "project",
    foreignKey: "projectId"
  });

  SimpleTask.belongsTo(SimpleTaskGroup, {
    as: "group",
    foreignKey: "groupId"
  });
}
