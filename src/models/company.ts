import * as Sequelize from "sequelize";
import { defaultColumns } from "./common";
// import { User } from "./user";

export class Company extends Sequelize.Model {

  public id: number;
  public companyTitleName: string;
  public description: string;

  // public createdAt: Date;
  // public updatedAt: Date;
  // public deletedAt: Date;

}

export function initCompany(sequelize: Sequelize.Sequelize) {
  Company.init({
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    companyTitleName: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    ...defaultColumns
  }, {
    tableName: "company_title",
    sequelize,
    paranoid: true,
    timestamps: true,
  });

  // Company.hasMany(User, {
  //   as: "users",
  //   foreignKey: "companyId"
  // });
}
