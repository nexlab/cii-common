import * as Sequelize from "sequelize";
import { defaultColumns, StatusCode } from "./common";

export class MessageCategory extends Sequelize.Model {

  public id: number;
  public name: string;
  public priority: number;
  public description: string;
  public status: StatusCode;

  public createdAt: Date;
  public createdBy: number;
  public updatedAt: Date;
  public updatedBy: number;
  public deletedAt: Date;
}

export function initMessageCategory(sequelize: Sequelize.Sequelize) {
  MessageCategory.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    priority: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: "active"
    },
    ...defaultColumns
  }, {
    tableName: "message_categories",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
