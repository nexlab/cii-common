import * as Sequelize from "sequelize";
import { Attachment } from "../file/models/attachment";
import { defaultColumns } from "./common";
import { User } from "./user";

export class MessageComment extends Sequelize.Model {

  public id: number;
  public content: string;
  public messageId: number | string;
  public parentId: number | string;

  public createdAt: Date;
  public createdBy: number | string;
  public updatedAt: Date;
  public updatedBy: number | string;
  public deletedAt: Date;
  public attachments: Attachment[];

  public owner?: User;
}

export function initMessageComment(sequelize: Sequelize.Sequelize) {
  MessageComment.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    content: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    messageId: {
      type: Sequelize.BIGINT,
      allowNull: false,
    },
    parentId: {
      type: Sequelize.BIGINT,
      allowNull: true,
    },
    ...defaultColumns
  }, {
    tableName: "message_comments",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
