import * as Sequelize from "sequelize";
import { Attachment } from "../file/models/attachment";
import { safeParseJSON } from "../helpers";
import { defaultColumns, StatusCode } from "./common";
import { MessageCategory } from "./message-category";
import { MessageComment } from "./message-comment";
import { MessageLike } from "./message-like";
import { User } from "./user";

export interface IMessageCount {
  messageId: string;
  count: number;
}

export enum MessageActivityType {
  CreateMessage = "create_message",
  UpdateMessage = "update_message",
  OwnerEditMessage = "owner_edit_message",
  CloseMessage = "close_message",
  DeleteMessage = "delete_message",
  AddComment = "add_comment",
  EditComment = "edit_comment",
  DeleteComment = "delete_comment",
  AddLike = "add_like",
  DeleteLike = "delete_like",
}

export enum FilterMyMessage {
  All = "all",
  Owner = "owner",
  OnlyMember = "onlyMember",
  Closed = "closed"
}

export interface IMessageActivity {
  description: string;
  type: MessageActivityType;
  userId: number;
  createdAt: string;
  relatedUser?: User;
}

export function addMessageActivity(
  activities: IMessageActivity[] | string, activity: IMessageActivity): IMessageActivity[] {

  return [activity, ...safeParseJSON(activities, [])];
}

export class Message extends Sequelize.Model {

  public id: string;
  public title: string;
  public content: string;
  public categoryId: number;
  public memberIds: Array<number | string>;
  public subscribers: Array<number | string>;
  public labels: string[];
  public attachmentIds: number[];
  public readers: string[];

  public isClosed: boolean;
  public activities: IMessageActivity[];

  public createdAt: Date;
  public createdBy: number | string;
  public updatedAt: Date;
  public updatedBy: number | string;
  public deletedAt: Date;

  public status: StatusCode;

  public comments?: MessageComment[];
  public likes?: MessageLike[];
  public category?: MessageCategory;
  public MessageCategory?: MessageCategory;
  public User?: User;
  public owner?: User;
  public attachments?: Attachment[];
  public members?: User[];
  public notificationSubscribers?: User[];

  public isRead?: boolean;
  public priorityType: string;
}

export interface IMessageItem extends Message {
  numberOfComment: number;
  numberOfLike: number;
  meLike: boolean;
}

export function initMessage(sequelize: Sequelize.Sequelize) {

  Message.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    content: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    categoryId: {
      type: Sequelize.BIGINT,
      allowNull: false
    },
    memberIds: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },
    subscribers: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },
    labels: {
      type: Sequelize.ARRAY(Sequelize.TEXT),
      allowNull: false,
      defaultValue: []
    },
    attachmentIds: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },
    isClosed: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },

    activities: {
      type: Sequelize.JSON,
      allowNull: false,
      defaultValue: "{}",
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    readers: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },
    priorityType: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    ...defaultColumns
  }, {
    tableName: "messages",
    sequelize,
    paranoid: true,
    timestamps: true,
  });

  Message.belongsTo(User, {
    foreignKey: "createdBy"
  });

  Message.belongsTo(MessageCategory, {
    foreignKey: "categoryId"
  });

  Message.hasMany(MessageLike, {
    as: "likes",
    foreignKey: "messageId"
  });

  Message.hasMany(MessageComment, {
    as: "comments",
    foreignKey: "messageId"
  });
}
