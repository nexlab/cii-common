import { Sequelize } from "sequelize";

function initDBConnection(connectionConfig: string | object, extraConfig: any = {}): Sequelize {

  const defaultOptions = {
    dialect: "postgres",
    ...extraConfig,
    pool: {
      max: 1,
      min: 0,
      ...extraConfig.pool,
    }
  };

  if (typeof connectionConfig === "string") {
    return new Sequelize(connectionConfig, <any> defaultOptions);
  }

  const options = {
    connectionConfig,
    ...defaultOptions,
  };

  return new Sequelize(<any> options);
}

export function db(connectionConfig?: string | object, extraConfig: any = {}): Sequelize {

  const connectionCfg = process.env.DATABASE_URL || connectionConfig;

  return initDBConnection(connectionCfg, extraConfig);
}
