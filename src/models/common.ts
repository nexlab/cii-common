import * as Sequelize from "sequelize";

export enum StatusCode {
  Active = "active",
  Deactivate = "deactivate",
  Deleted = "deleted",
  HardDeleted = "hardDeleted",
}

export enum Gender {
  Male = "male",
  Female = "female",
}

export interface ISimpleOption {
  code: string;
  name: string;
}

export interface IUpdateOptions {
  actorId: string;
  note?: string;
}

export enum ReferenceType {
  Topic = "topic",
  Message = "message",
  Plan = "plan",
  Task = "project-task",
  Project = "project",
  PlanItem = "plan-item",
  Feedback = "feedback",
  Help = "help",
  TaskComment = "project-task-comment",
  MessageComment = "message-comment",
  TopicComment = "topic-comment",
  Department = "department",
  CustomRootFolder = "custom-root-folder",
}

// default column for init model

export const defaultColumns = {
  createdAt: {
    type: Sequelize.DATE,
    allowNull: true,
    defaultValue: Sequelize.NOW,
  },
  createdBy: {
    type: Sequelize.BIGINT,
    allowNull: true,
  },
  updatedAt: {
    type: Sequelize.DATE,
    allowNull: true,
    defaultValue: Sequelize.NOW,
  },
  updatedBy: {
    type: Sequelize.BIGINT,
    allowNull: true,
  },
  deletedAt: {
    type: Sequelize.DATE,
    allowNull: true,
  },
};
