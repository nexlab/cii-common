import * as Sequelize from "sequelize";
import { defaultColumns, StatusCode } from "./common";
import { User } from "./user";

export class Department extends Sequelize.Model {

  public id: number;
  public departmentName: string;
  public weight: number;
  public description: string;
  public imageUrl: string;

  public departmentLeader: string;
  public status: StatusCode;

  public User: User;
  public leader: User;
  // public createdAt: Date;
  // public updatedAt: Date;
  // public deletedAt: Date;

}

export function initDepartment(sequelize: Sequelize.Sequelize) {
  Department.init({
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    weight: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    departmentName: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    imageUrl: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    departmentLeader: {
      type: Sequelize.BIGINT,
      allowNull: true,
      references: {
        model: "users",
        key: "id"
      }
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: "active"
    },
    ...defaultColumns
  }, {
    tableName: "departments",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
  Department.belongsTo(User, { foreignKey: "departmentLeader", as: "leader" });
}
