import * as Sequelize from "sequelize";
import { defaultColumns, StatusCode } from "./common";

export class Role extends Sequelize.Model {

  public id: string;
  public roleName: string;

  public permission: string;
  public menuItems: string[];
  public actions: string[];

  public status: StatusCode;

  public isSuperRole: boolean;

  // public createdAt: Date;
  // public updatedAt: Date;
  // public deletedAt: Date;

}

export function initRole(sequelize: Sequelize.Sequelize) {
  Role.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    roleName: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    isSuperRole: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    permission: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    actions: {
      type: Sequelize.ARRAY(Sequelize.TEXT),
      allowNull: false,
      defaultValue: []
    },
    menuItems: {
      type: Sequelize.ARRAY(Sequelize.TEXT),
      allowNull: false,
      defaultValue: []
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: "active"
    },
    ...defaultColumns
  }, {
    tableName: "roles",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
