import * as Sequelize from "sequelize";
import { defaultColumns } from "./common";

export class Major extends Sequelize.Model {

  public id: number;
  public majorName: string;
  public description: string;

  // public createdAt: Date;
  // public updatedAt: Date;
  // public deletedAt: Date;

}

export function initMajor(sequelize: Sequelize.Sequelize) {
  Major.init({
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    majorName: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    ...defaultColumns
  }, {
      tableName: "majors",
      sequelize,
      paranoid: true,
      timestamps: true,
    });
}
