import * as Sequelize from "sequelize";
import { defaultColumns, StatusCode } from "./common";
import { SimpleProject } from "./project";

export class SimpleTaskGroup extends Sequelize.Model {

  public id: string;
  public name: string;
  public description: string;
  public status: StatusCode;
  public color: string;

  public projectId: string;
  public project?: SimpleProject;

  public createdAt: Date;
  public createdBy: string;
  public updatedAt: Date;
  public updatedBy: string;
  public deletedAt: Date;
}

export function initSimpleTaskGroup(sequelize: Sequelize.Sequelize) {
  SimpleTaskGroup.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    color: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    projectId: {
      type: Sequelize.BIGINT,
      allowNull: false,

      references: {
        model: SimpleProject,
        key: "id"
      }
    },

    description: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: ""
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: StatusCode.Active
    },
    ...defaultColumns
  }, {
    tableName: "task_groups",
    sequelize,
    paranoid: true,
    timestamps: true,
  });

}
