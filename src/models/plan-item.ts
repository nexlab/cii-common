import * as Sequelize from "sequelize";
import {
  defaultColumns, StatusCode
} from "./common";
import { SimplePlan } from "./plan";
import { User } from "./user";

export enum PlanItemType {
  Normal = "normal",
  Milestone = "milestone",
}

export class PlanItem extends Sequelize.Model {

  public id: string;
  public planId: string;
  public groupId?: string;
  public title: string;
  public content: string;

  public type: PlanItemType;
  public startDate: Date;
  public deadline: Date;

  public progress: number;
  public reminder: boolean;
  public assigneeId: string;

  public status: StatusCode;

  public createdAt: Date;
  public createdBy: string;
  public updatedAt: Date;
  public updatedBy: string;
  public deletedAt: Date;

  public assignee?: User;
  public owner?: User;
  public plan?: SimplePlan;
}

export function initPlanItem(sequelize: Sequelize.Sequelize) {
  PlanItem.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },

    planId: {
      type: Sequelize.BIGINT,
      allowNull: false,
    },
    groupId: {
      type: Sequelize.BIGINT,
      allowNull: true,
    },
    title: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: ""
    },
    content: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: ""
    },
    type: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: ""
    },
    startDate: {
      type: Sequelize.DATE,
      allowNull: true,
    },
    deadline: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    progress: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    reminder: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    assigneeId: {
      type: Sequelize.BIGINT,
      allowNull: true,
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    ...defaultColumns
  }, {
    tableName: "plan_items",
    sequelize,
    paranoid: true,
    timestamps: true,
  });

  PlanItem.belongsTo(SimplePlan, {
    as: "plan",
    foreignKey: "planId"
  });
}
