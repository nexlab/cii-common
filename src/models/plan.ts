import * as Sequelize from "sequelize";
import { defaultColumns, StatusCode } from "./common";

export class SimplePlan extends Sequelize.Model {

  public id: string;
  public name: string;
  public description: string;
  public startDate: Date;
  public endDate: Date;

  public memberIds: string[];
  public subscribers: string[];
  public setting?: any;
  public remindTime: number;

  public status: StatusCode;
  public createdAt: Date;
  public createdBy: string;
  public updatedAt: Date;
  public updatedBy: string;
  public deletedAt: Date;

}

export function initSimplePlan(sequelize: Sequelize.Sequelize) {
  SimplePlan.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: false
    },

    startDate: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    endDate: {
      type: Sequelize.DATE,
      allowNull: true,
    },
    memberIds: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },
    subscribers: {
      type: Sequelize.ARRAY(Sequelize.BIGINT),
      allowNull: false,
      defaultValue: []
    },
    remindTime: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 24
    },
    status: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    ...defaultColumns
  }, {
    tableName: "plans",
    sequelize,
    paranoid: true,
    timestamps: true,
  });

}
