import * as Sequelize from "sequelize";
import { defaultColumns } from "./common";
import { User } from "./user";

export enum LikeAction {
  Liked = "liked",
  Disliked = "disliked"
}

export class MessageLike extends Sequelize.Model {

  public id: number;
  public action: LikeAction;
  public messageId: number;

  public createdAt: Date;
  public createdBy: number | string;
  public updatedAt: Date;
  public updatedBy: number | string;
  public deletedAt: Date;

  public owner?: User;
}

export function initMessageLike(sequelize: Sequelize.Sequelize) {
  MessageLike.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    action: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    messageId: {
      type: Sequelize.BIGINT,
      allowNull: false,
    },
    ...defaultColumns
  }, {
    tableName: "message_likes",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
