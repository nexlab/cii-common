import * as Sequelize from "sequelize";
import { defaultColumns } from "./common";

export interface IFlowProjectTemplate {
  key: string;
  name: string;
}

export class SimpleProjectTemplate extends Sequelize.Model {

  public id: number;
  public name: string;
  public description: string;
  public flow: IFlowProjectTemplate[];

  public createdAt: Date;
  public createdBy: string;
  public updatedAt: Date;
  public updatedBy: string;
  public deletedAt: Date;
}

export function initSimpleProjectTemplate(sequelize: Sequelize.Sequelize) {
  SimpleProjectTemplate.init({
    id: {
      type: Sequelize.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: Sequelize.TEXT,
      allowNull: false,
      defaultValue: ""
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: true
    },
    flow: {
      type: Sequelize.JSON,
      allowNull: false,
      defaultValue: {}
    },
    ...defaultColumns
  }, {
    tableName: "project_templates",
    sequelize,
    paranoid: true,
    timestamps: true,
  });
}
