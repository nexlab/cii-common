"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Joi = require("joi");
const models_1 = require("../../models");
exports.UserNotificationSettingSchema = Joi.object({
    setting: Joi.object({
        email: Joi.boolean().required(),
        appPush: Joi.boolean().required()
    }).required(),
    type: Joi.string().valid([models_1.ReferenceType.Message, models_1.ReferenceType.Plan, models_1.ReferenceType.Task, models_1.ReferenceType.TaskComment, models_1.ReferenceType.Project, models_1.ReferenceType.MessageComment, models_1.ReferenceType.Topic, models_1.ReferenceType.TopicComment]).required(),
    referenceId: Joi.number().required()
});
