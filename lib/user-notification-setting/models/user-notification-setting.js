"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const models_1 = require("../../models");
class UserNotificationSetting extends Sequelize.Model {
}
exports.UserNotificationSetting = UserNotificationSetting;
function initUserNotificationSetting(sequelize) {
    UserNotificationSetting.init({
        id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        },
        userId: {
            type: Sequelize.BIGINT,
            allowNull: false,
            references: {
                model: "users",
                key: "id"
            }
        },
        type: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        referenceId: {
            type: Sequelize.BIGINT,
            allowNull: false
        },
        setting: {
            type: Sequelize.JSON,
            allowNull: false
        }
    }, {
        tableName: "user_notification_setting",
        sequelize,
        timestamps: false
    });
    UserNotificationSetting.belongsTo(models_1.User, {
        as: "user",
        foreignKey: "userId"
    });
}
exports.initUserNotificationSetting = initUserNotificationSetting;
