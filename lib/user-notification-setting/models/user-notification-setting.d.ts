import * as Sequelize from "sequelize";
import { ReferenceType, User } from "../../models";
export interface IInputUserNotificationSetting {
    referenceId: number;
    type: ReferenceType;
    setting: object;
}
export interface IUserNotificationSetting {
    email: boolean;
    appPush: boolean;
}
export declare class UserNotificationSetting extends Sequelize.Model {
    id: number;
    userId: string;
    referenceId: number;
    type: ReferenceType;
    setting: IUserNotificationSetting;
    user?: User;
}
export declare function initUserNotificationSetting(sequelize: Sequelize.Sequelize): void;
