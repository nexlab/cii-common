"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const user_notification_setting_1 = require("../models/user-notification-setting");
function upsertUserNotificationSetting(userId, data) {
    return __awaiter(this, void 0, void 0, function* () {
        const current = yield user_notification_setting_1.UserNotificationSetting.findOne({
            where: {
                userId,
                type: data.type,
                referenceId: data.referenceId
            }
        });
        if (!current) {
            yield user_notification_setting_1.UserNotificationSetting.create({
                userId,
                type: data.type,
                referenceId: data.referenceId,
                setting: data.setting
            });
        }
        else {
            yield user_notification_setting_1.UserNotificationSetting.update({
                setting: data.setting
            }, {
                where: { id: current.id }
            });
        }
    });
}
exports.upsertUserNotificationSetting = upsertUserNotificationSetting;
function defaultSettingForEntire() {
    return {
        email: true,
        appPush: true
    };
}
exports.defaultSettingForEntire = defaultSettingForEntire;
function mappingUserNotificationForReference(userId, type, model) {
    return __awaiter(this, void 0, void 0, function* () {
        const ids = model.map((e) => e.id);
        const userNotifications = yield user_notification_setting_1.UserNotificationSetting.findAll({
            where: {
                referenceId: {
                    [sequelize_1.Op.in]: ids
                },
                userId,
                type
            }
        });
        return model.map((e) => {
            const current = userNotifications.find((a) => a.referenceId.toString() === e.id.toString());
            e.setting = current ? current.setting : defaultSettingForEntire();
            return e;
        });
    });
}
exports.mappingUserNotificationForReference = mappingUserNotificationForReference;
