import { ReferenceType } from "../../models";
import { IInputUserNotificationSetting } from "../models/user-notification-setting";
export declare function upsertUserNotificationSetting(userId: string, data: IInputUserNotificationSetting): Promise<void>;
export declare function defaultSettingForEntire(): {
    email: boolean;
    appPush: boolean;
};
export declare function mappingUserNotificationForReference(userId: string, type: ReferenceType, model: any[]): Promise<any[]>;
