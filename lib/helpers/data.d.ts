export declare function safeParseInt(input: string | number, defaultValue?: number): number;
export declare function isEmptyObject(input: object): boolean;
export declare function skipValueObject(input: object, valueSkip?: any[]): object;
export declare function safeParseJSON<T = any>(input: string | T, defaultValue?: {}): T;
export declare function toISOString(input: string | Date): string;
export declare function toDateString(input: string | Date): string;
