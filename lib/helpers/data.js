"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function safeParseInt(input, defaultValue = null) {
    if (!input || typeof input === "number") {
        return Math.floor(input || defaultValue);
    }
    try {
        return parseInt(input, 10);
    }
    catch (_a) {
        return defaultValue;
    }
}
exports.safeParseInt = safeParseInt;
function isEmptyObject(input) {
    return !input || (typeof input === "object"
        && !Array.isArray(input)
        && Object.keys(input).length === 0);
}
exports.isEmptyObject = isEmptyObject;
function skipValueObject(input, valueSkip = [null, undefined]) {
    if (isEmptyObject(input)) {
        return {};
    }
    return Object.keys(input).reduce((o, k) => valueSkip.indexOf(input[k]) !== -1 ? o : Object.assign(Object.assign({}, o), { [k]: input[k] }), {});
}
exports.skipValueObject = skipValueObject;
function safeParseJSON(input, defaultValue = {}) {
    try {
        const result = typeof input === "string" ? JSON.parse(input) : input;
        return result || defaultValue;
    }
    catch (e) {
        return defaultValue;
    }
}
exports.safeParseJSON = safeParseJSON;
function toISOString(input) {
    if (!input) {
        return null;
    }
    return typeof input === "string" ? input : input.toISOString();
}
exports.toISOString = toISOString;
function toDateString(input) {
    if (!input) {
        return null;
    }
    input = new Date(input);
    return input.toLocaleString();
}
exports.toDateString = toDateString;
