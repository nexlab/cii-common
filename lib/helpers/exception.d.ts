import { Response } from "express";
export interface IError {
    code?: string;
    message?: string;
    debugMessage?: string;
}
export interface IOptionError {
    statusCode?: number;
}
export declare function responseError(res: Response, error: any, options?: IOptionError): any;
