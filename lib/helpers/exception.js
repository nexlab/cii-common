"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("../resources/errors");
function parseError(error) {
    const result = {};
    result.code = error.code || "internal_error";
    result.message = error.message || "INTERNAL ERROR";
    if (error.debugMessage) {
        result.debugMessage = error.debugMessage;
    }
    return result;
}
function responseError(res, error, options = { statusCode: 400 }) {
    console.error("Response Error:");
    console.error(error);
    const statusCode = options.statusCode || 400;
    if (typeof error === "string") {
        return res.status(statusCode).json(parseError({
            code: error,
            message: errors_1.getMessageError(error)
        }));
    }
    if (typeof error === "object") {
        const errorCode = error.code || "error_unknown";
        return res.status(statusCode).json(parseError({
            code: errorCode,
            message: errors_1.getMessageError(errorCode),
            debugMessage: error.message || errors_1.getMessageError(errorCode)
        }));
    }
    if (Array.isArray(error)) {
        const errors = error.map(parseError);
        return res.status(400).json({ errors });
    }
    return res.status(statusCode).json(parseError(error));
}
exports.responseError = responseError;
