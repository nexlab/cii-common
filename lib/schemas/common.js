"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = require("joi");
exports.statusSchema = joi_1.string().valid(["active", "deleted"]);
exports.genderSchema = joi_1.string().valid(["male", "female"]);
exports.paginationSchema = {
    page: joi_1.number(),
    size: joi_1.number()
};
