import * as Sequelize from "sequelize";
export * from "./topic-users";
export declare function initDBTopicToServeNotification(sequelize: Sequelize.Sequelize): void;
