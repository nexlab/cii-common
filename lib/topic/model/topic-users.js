"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
class TopicUsers extends Sequelize.Model {
}
exports.TopicUsers = TopicUsers;
function initTopicUsers(sequelize) {
    TopicUsers.init({
        id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        },
        userId: {
            type: Sequelize.BIGINT,
            allowNull: false,
            references: {
                model: "users",
                key: "id"
            }
        },
        topicIdsUnRead: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false
        },
        createdAt: {
            type: Sequelize.DATE,
            allowNull: true,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            allowNull: true,
            defaultValue: Sequelize.NOW,
        },
        deletedAt: {
            type: Sequelize.DATE,
            allowNull: true,
        }
    }, {
        tableName: "topic_users",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initTopicUsers = initTopicUsers;
