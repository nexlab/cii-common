import * as Sequelize from "sequelize";
export declare class TopicUsers extends Sequelize.Model {
    id: number;
    userId: number;
    topicIdsUnRead: string[];
}
export declare function initTopicUsers(sequelize: Sequelize.Sequelize): void;
