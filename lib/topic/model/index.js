"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
const topic_users_1 = require("./topic-users");
__export(require("./topic-users"));
function initDBTopicToServeNotification(sequelize) {
    topic_users_1.initTopicUsers(sequelize);
}
exports.initDBTopicToServeNotification = initDBTopicToServeNotification;
