export interface ICreateTopicUsers {
    memberIds: Array<string | number>;
    topicId: string;
}
export declare function createTopicUsers(data: ICreateTopicUsers): Promise<any>;
export declare function markTopicUserAsRead(userId: number | string): Promise<number[]>;
