"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash = require("lodash");
const topic_users_1 = require("../model/topic-users");
function createTopicUsers(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const topicUsers = yield topic_users_1.TopicUsers.findAll({
                where: {
                    userId: data.memberIds
                }
            });
            function parseIds(reverse = false) {
                return topicUsers.filter((e) => {
                    const current = data.memberIds.find((a) => a.toString() === e.userId.toString());
                    if (current) {
                        return reverse;
                    }
                    return !reverse;
                });
            }
            const topicUsersNeedCreate = data.memberIds.filter((e) => {
                const current = topicUsers.find((a) => e.toString() === a.userId.toString());
                if (current) {
                    return false;
                }
                return true;
            });
            const topicUsersNeedUpdate = parseIds(true);
            if (topicUsersNeedCreate && topicUsersNeedCreate.length > 0) {
                yield Promise.all(topicUsersNeedCreate.map((e) => {
                    return topic_users_1.TopicUsers.create({
                        userId: e,
                        topicIdsUnRead: [data.topicId]
                    });
                }));
            }
            if (topicUsersNeedUpdate && topicUsersNeedUpdate.length > 0) {
                yield Promise.all(topicUsersNeedUpdate.map((e) => {
                    e.topicIdsUnRead.push(data.topicId);
                    return topic_users_1.TopicUsers.update({
                        topicIdsUnRead: lodash.uniq(e.topicIdsUnRead)
                    }, {
                        where: { userId: e.userId, }
                    });
                }));
            }
            return;
        }
        catch (error) {
            return Promise.reject(error);
        }
    });
}
exports.createTopicUsers = createTopicUsers;
function markTopicUserAsRead(userId) {
    return __awaiter(this, void 0, void 0, function* () {
        const current = yield topic_users_1.TopicUsers.find({
            where: { userId }
        });
        let result = [];
        if (current) {
            result = current.topicIdsUnRead || [];
        }
        if (result.length > 0) {
            yield topic_users_1.TopicUsers.update({ topicIdsUnRead: [] }, {
                where: { userId }
            });
        }
        return result;
    });
}
exports.markTopicUserAsRead = markTopicUserAsRead;
