"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
class MessageCategory extends Sequelize.Model {
}
exports.MessageCategory = MessageCategory;
function initMessageCategory(sequelize) {
    MessageCategory.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, name: {
            type: Sequelize.TEXT,
            allowNull: false
        }, description: {
            type: Sequelize.TEXT,
            allowNull: true
        }, priority: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        }, status: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: "active"
        } }, common_1.defaultColumns), {
        tableName: "message_categories",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initMessageCategory = initMessageCategory;
