"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
function initDBConnection(connectionConfig, extraConfig = {}) {
    const defaultOptions = Object.assign(Object.assign({ dialect: "postgres" }, extraConfig), { pool: Object.assign({ max: 1, min: 0 }, extraConfig.pool) });
    if (typeof connectionConfig === "string") {
        return new sequelize_1.Sequelize(connectionConfig, defaultOptions);
    }
    const options = Object.assign({ connectionConfig }, defaultOptions);
    return new sequelize_1.Sequelize(options);
}
function db(connectionConfig, extraConfig = {}) {
    const connectionCfg = process.env.DATABASE_URL || connectionConfig;
    return initDBConnection(connectionCfg, extraConfig);
}
exports.db = db;
