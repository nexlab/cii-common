import { Request } from "express";
import { FindAndCountOptions, Model, Order } from "sequelize";
export interface IPagingParams {
    page?: string | number;
    size?: string | number;
    order?: Order;
}
export interface IPaginationRequest extends Request {
    query: any;
}
export interface IPaginationInfo {
    page: number;
    size: number;
    totalPages: number;
    total: number;
}
export interface IPagingResult<T> {
    data: T[];
    pagination: IPaginationInfo;
}
export declare const PAGE_SIZE = 20;
export declare function parsePaginationParams(input: any): IPagingParams;
export declare function parseOrderParams(input: any): Order;
export declare function filterAll<T extends Model>(modelClass: (new () => T) & typeof Model, queryParams: FindAndCountOptions, pagingParams: IPagingParams): Promise<IPagingResult<T>>;
export interface IOptionFilterPagination<T> {
    customQuery?: (model: (new () => T) & typeof Model, query: any) => Promise<any>;
}
export declare function filterPagination<T extends Model>(modelClass: (new () => T) & typeof Model, queryParams: FindAndCountOptions, pagingParams: IPagingParams, optionsFilters?: IOptionFilterPagination<T>): Promise<IPagingResult<T>>;
export declare function defaultPagination(size?: number, page?: number): {
    data: any[];
    pagination: {
        page: number;
        size: number;
        total: number;
        totalPages: number;
    };
};
export declare function rawQuerySequelize(query: any): any;
