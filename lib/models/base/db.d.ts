import { Sequelize } from "sequelize";
export declare function db(connectionConfig?: string | object, extraConfig?: any): Sequelize;
