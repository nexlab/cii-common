"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
class Major extends Sequelize.Model {
}
exports.Major = Major;
function initMajor(sequelize) {
    Major.init(Object.assign({ id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        }, majorName: {
            type: Sequelize.TEXT,
            allowNull: false
        }, description: {
            type: Sequelize.TEXT,
            allowNull: true
        } }, common_1.defaultColumns), {
        tableName: "majors",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initMajor = initMajor;
