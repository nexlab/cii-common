import * as Sequelize from "sequelize";
export declare class Education extends Sequelize.Model {
    id: number;
    educationName: string;
    description: string;
}
export declare function initEducation(sequelize: Sequelize.Sequelize): void;
