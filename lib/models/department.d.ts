import * as Sequelize from "sequelize";
import { StatusCode } from "./common";
import { User } from "./user";
export declare class Department extends Sequelize.Model {
    id: number;
    departmentName: string;
    weight: number;
    description: string;
    imageUrl: string;
    departmentLeader: string;
    status: StatusCode;
    User: User;
    leader: User;
}
export declare function initDepartment(sequelize: Sequelize.Sequelize): void;
