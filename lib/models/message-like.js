"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
var LikeAction;
(function (LikeAction) {
    LikeAction["Liked"] = "liked";
    LikeAction["Disliked"] = "disliked";
})(LikeAction = exports.LikeAction || (exports.LikeAction = {}));
class MessageLike extends Sequelize.Model {
}
exports.MessageLike = MessageLike;
function initMessageLike(sequelize) {
    MessageLike.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, action: {
            type: Sequelize.TEXT,
            allowNull: false
        }, messageId: {
            type: Sequelize.BIGINT,
            allowNull: false,
        } }, common_1.defaultColumns), {
        tableName: "message_likes",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initMessageLike = initMessageLike;
