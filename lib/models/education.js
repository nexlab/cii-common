"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
class Education extends Sequelize.Model {
}
exports.Education = Education;
function initEducation(sequelize) {
    Education.init(Object.assign({ id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        }, educationName: {
            type: Sequelize.TEXT,
            allowNull: false
        }, description: {
            type: Sequelize.TEXT,
            allowNull: true
        } }, common_1.defaultColumns), {
        tableName: "educations",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initEducation = initEducation;
