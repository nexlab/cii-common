import * as Sequelize from "sequelize";
export declare class Company extends Sequelize.Model {
    id: number;
    companyTitleName: string;
    description: string;
}
export declare function initCompany(sequelize: Sequelize.Sequelize): void;
