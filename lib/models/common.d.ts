import * as Sequelize from "sequelize";
export declare enum StatusCode {
    Active = "active",
    Deactivate = "deactivate",
    Deleted = "deleted",
    HardDeleted = "hardDeleted"
}
export declare enum Gender {
    Male = "male",
    Female = "female"
}
export interface ISimpleOption {
    code: string;
    name: string;
}
export interface IUpdateOptions {
    actorId: string;
    note?: string;
}
export declare enum ReferenceType {
    Topic = "topic",
    Message = "message",
    Plan = "plan",
    Task = "project-task",
    Project = "project",
    PlanItem = "plan-item",
    Feedback = "feedback",
    Help = "help",
    TaskComment = "project-task-comment",
    MessageComment = "message-comment",
    TopicComment = "topic-comment",
    Department = "department",
    CustomRootFolder = "custom-root-folder"
}
export declare const defaultColumns: {
    createdAt: {
        type: Sequelize.DateDataTypeConstructor;
        allowNull: boolean;
        defaultValue: Sequelize.AbstractDataTypeConstructor;
    };
    createdBy: {
        type: Sequelize.BigIntDataTypeConstructor;
        allowNull: boolean;
    };
    updatedAt: {
        type: Sequelize.DateDataTypeConstructor;
        allowNull: boolean;
        defaultValue: Sequelize.AbstractDataTypeConstructor;
    };
    updatedBy: {
        type: Sequelize.BigIntDataTypeConstructor;
        allowNull: boolean;
    };
    deletedAt: {
        type: Sequelize.DateDataTypeConstructor;
        allowNull: boolean;
    };
};
