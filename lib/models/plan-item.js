"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
const plan_1 = require("./plan");
var PlanItemType;
(function (PlanItemType) {
    PlanItemType["Normal"] = "normal";
    PlanItemType["Milestone"] = "milestone";
})(PlanItemType = exports.PlanItemType || (exports.PlanItemType = {}));
class PlanItem extends Sequelize.Model {
}
exports.PlanItem = PlanItem;
function initPlanItem(sequelize) {
    PlanItem.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, planId: {
            type: Sequelize.BIGINT,
            allowNull: false,
        }, groupId: {
            type: Sequelize.BIGINT,
            allowNull: true,
        }, title: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: ""
        }, content: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: ""
        }, type: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: ""
        }, startDate: {
            type: Sequelize.DATE,
            allowNull: true,
        }, deadline: {
            type: Sequelize.DATE,
            allowNull: false,
        }, progress: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        }, reminder: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }, assigneeId: {
            type: Sequelize.BIGINT,
            allowNull: true,
        }, status: {
            type: Sequelize.TEXT,
            allowNull: false,
        } }, common_1.defaultColumns), {
        tableName: "plan_items",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
    PlanItem.belongsTo(plan_1.SimplePlan, {
        as: "plan",
        foreignKey: "planId"
    });
}
exports.initPlanItem = initPlanItem;
