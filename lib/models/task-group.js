"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
const project_1 = require("./project");
class SimpleTaskGroup extends Sequelize.Model {
}
exports.SimpleTaskGroup = SimpleTaskGroup;
function initSimpleTaskGroup(sequelize) {
    SimpleTaskGroup.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, name: {
            type: Sequelize.TEXT,
            allowNull: false
        }, color: {
            type: Sequelize.TEXT,
            allowNull: false
        }, projectId: {
            type: Sequelize.BIGINT,
            allowNull: false,
            references: {
                model: project_1.SimpleProject,
                key: "id"
            }
        }, description: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: ""
        }, status: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: common_1.StatusCode.Active
        } }, common_1.defaultColumns), {
        tableName: "task_groups",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initSimpleTaskGroup = initSimpleTaskGroup;
