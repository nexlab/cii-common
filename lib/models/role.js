"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
class Role extends Sequelize.Model {
}
exports.Role = Role;
function initRole(sequelize) {
    Role.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, roleName: {
            type: Sequelize.TEXT,
            allowNull: false
        }, isSuperRole: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }, permission: {
            type: Sequelize.TEXT,
            allowNull: false
        }, actions: {
            type: Sequelize.ARRAY(Sequelize.TEXT),
            allowNull: false,
            defaultValue: []
        }, menuItems: {
            type: Sequelize.ARRAY(Sequelize.TEXT),
            allowNull: false,
            defaultValue: []
        }, status: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: "active"
        } }, common_1.defaultColumns), {
        tableName: "roles",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initRole = initRole;
