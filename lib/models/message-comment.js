"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
class MessageComment extends Sequelize.Model {
}
exports.MessageComment = MessageComment;
function initMessageComment(sequelize) {
    MessageComment.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, content: {
            type: Sequelize.TEXT,
            allowNull: false
        }, messageId: {
            type: Sequelize.BIGINT,
            allowNull: false,
        }, parentId: {
            type: Sequelize.BIGINT,
            allowNull: true,
        } }, common_1.defaultColumns), {
        tableName: "message_comments",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initMessageComment = initMessageComment;
