"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
class ProjectCategory extends Sequelize.Model {
}
exports.ProjectCategory = ProjectCategory;
function initProjectCategory(sequelize) {
    ProjectCategory.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, name: {
            type: Sequelize.TEXT,
            allowNull: false
        }, description: {
            type: Sequelize.TEXT,
            allowNull: true
        }, status: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: common_1.StatusCode.Active
        } }, common_1.defaultColumns), {
        tableName: "project_categories",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initProjectCategory = initProjectCategory;
