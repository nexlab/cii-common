import * as Sequelize from "sequelize";
import { ReferenceType, StatusCode } from "./common";
export declare class Favorite extends Sequelize.Model {
    id: string;
    avatarUrl: string;
    type: ReferenceType;
    relateId: string;
    userId: string;
    status: StatusCode;
}
export declare function initFavorite(sequelize: Sequelize.Sequelize): void;
