import * as Sequelize from "sequelize";
import { SimpleProject } from "./project";
import { SimpleTaskGroup } from "./task-group";
export interface ITaskActivity {
    description: string;
    userId: string;
    createdAt: Date;
    previousData?: string;
    newData?: string;
}
export declare class SimpleTask extends Sequelize.Model {
    id: string;
    taskCode: string;
    title: string;
    description: string;
    projectId: string;
    groupId: string;
    startDate: Date;
    endDate: Date;
    estimation: number;
    reporterId: string;
    assigneeId: string;
    deadline: Date;
    labels: string[];
    memberIds: string[];
    readers: string[];
    activities: ITaskActivity[];
    numberOfComment?: number;
    numberOfAttachment?: number;
    project?: SimpleProject;
    group?: SimpleTaskGroup;
    taskStatus: string;
    rating: string;
    isPrivate: boolean;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    deletedAt: Date;
}
export declare function initSimpleTask(sequelize: Sequelize.Sequelize): void;
