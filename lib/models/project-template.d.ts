import * as Sequelize from "sequelize";
export interface IFlowProjectTemplate {
    key: string;
    name: string;
}
export declare class SimpleProjectTemplate extends Sequelize.Model {
    id: number;
    name: string;
    description: string;
    flow: IFlowProjectTemplate[];
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    deletedAt: Date;
}
export declare function initSimpleProjectTemplate(sequelize: Sequelize.Sequelize): void;
