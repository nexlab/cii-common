"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
const project_template_1 = require("./project-template");
var MyProjectFilter;
(function (MyProjectFilter) {
    MyProjectFilter["All"] = "all";
    MyProjectFilter["Owner"] = "owner";
    MyProjectFilter["OnlyMember"] = "onlyMember";
    MyProjectFilter["Closed"] = "closed";
    MyProjectFilter["ExpiryDate"] = "expiryDate";
})(MyProjectFilter = exports.MyProjectFilter || (exports.MyProjectFilter = {}));
class SimpleProject extends Sequelize.Model {
}
exports.SimpleProject = SimpleProject;
function initSimpleProject(sequelize) {
    SimpleProject.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, projectCode: {
            type: Sequelize.TEXT,
            allowNull: false
        }, name: {
            type: Sequelize.TEXT,
            allowNull: false
        }, description: {
            type: Sequelize.TEXT,
            allowNull: true
        }, avatarUrl: {
            type: Sequelize.TEXT,
            allowNull: true
        }, startDate: {
            type: Sequelize.DATE,
            allowNull: false,
        }, endDate: {
            type: Sequelize.DATE,
            allowNull: true,
        }, leaderId: {
            type: Sequelize.BIGINT,
            allowNull: false,
        }, categoryId: {
            type: Sequelize.BIGINT,
            allowNull: false
        }, templateId: {
            type: Sequelize.BIGINT,
            allowNull: true,
            references: {
                model: project_template_1.SimpleProjectTemplate,
                key: "id"
            }
        }, memberIds: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        }, subscribers: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        }, isClosed: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }, reminder: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }, remindTime: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 24
        }, status: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: common_1.StatusCode.Active
        }, ratingId: {
            type: Sequelize.BIGINT,
            allowNull: true
        } }, common_1.defaultColumns), {
        tableName: "projects",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
    SimpleProject.belongsTo(project_template_1.SimpleProjectTemplate, {
        foreignKey: "templateId",
        as: "template"
    });
}
exports.initSimpleProject = initSimpleProject;
