import * as Sequelize from "sequelize";
import { StatusCode } from "./common";
export declare class Role extends Sequelize.Model {
    id: string;
    roleName: string;
    permission: string;
    menuItems: string[];
    actions: string[];
    status: StatusCode;
    isSuperRole: boolean;
}
export declare function initRole(sequelize: Sequelize.Sequelize): void;
