import * as Sequelize from "sequelize";
import { StatusCode } from "./common";
export declare class MessageCategory extends Sequelize.Model {
    id: number;
    name: string;
    priority: number;
    description: string;
    status: StatusCode;
    createdAt: Date;
    createdBy: number;
    updatedAt: Date;
    updatedBy: number;
    deletedAt: Date;
}
export declare function initMessageCategory(sequelize: Sequelize.Sequelize): void;
