"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const helpers_1 = require("../helpers");
const common_1 = require("./common");
const message_category_1 = require("./message-category");
const message_comment_1 = require("./message-comment");
const message_like_1 = require("./message-like");
const user_1 = require("./user");
var MessageActivityType;
(function (MessageActivityType) {
    MessageActivityType["CreateMessage"] = "create_message";
    MessageActivityType["UpdateMessage"] = "update_message";
    MessageActivityType["OwnerEditMessage"] = "owner_edit_message";
    MessageActivityType["CloseMessage"] = "close_message";
    MessageActivityType["DeleteMessage"] = "delete_message";
    MessageActivityType["AddComment"] = "add_comment";
    MessageActivityType["EditComment"] = "edit_comment";
    MessageActivityType["DeleteComment"] = "delete_comment";
    MessageActivityType["AddLike"] = "add_like";
    MessageActivityType["DeleteLike"] = "delete_like";
})(MessageActivityType = exports.MessageActivityType || (exports.MessageActivityType = {}));
var FilterMyMessage;
(function (FilterMyMessage) {
    FilterMyMessage["All"] = "all";
    FilterMyMessage["Owner"] = "owner";
    FilterMyMessage["OnlyMember"] = "onlyMember";
    FilterMyMessage["Closed"] = "closed";
})(FilterMyMessage = exports.FilterMyMessage || (exports.FilterMyMessage = {}));
function addMessageActivity(activities, activity) {
    return [activity, ...helpers_1.safeParseJSON(activities, [])];
}
exports.addMessageActivity = addMessageActivity;
class Message extends Sequelize.Model {
}
exports.Message = Message;
function initMessage(sequelize) {
    Message.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, title: {
            type: Sequelize.TEXT,
            allowNull: false
        }, content: {
            type: Sequelize.TEXT,
            allowNull: false
        }, categoryId: {
            type: Sequelize.BIGINT,
            allowNull: false
        }, memberIds: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        }, subscribers: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        }, labels: {
            type: Sequelize.ARRAY(Sequelize.TEXT),
            allowNull: false,
            defaultValue: []
        }, attachmentIds: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        }, isClosed: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        }, activities: {
            type: Sequelize.JSON,
            allowNull: false,
            defaultValue: "{}",
        }, status: {
            type: Sequelize.TEXT,
            allowNull: false,
        }, readers: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        }, priorityType: {
            type: Sequelize.TEXT,
            allowNull: true
        } }, common_1.defaultColumns), {
        tableName: "messages",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
    Message.belongsTo(user_1.User, {
        foreignKey: "createdBy"
    });
    Message.belongsTo(message_category_1.MessageCategory, {
        foreignKey: "categoryId"
    });
    Message.hasMany(message_like_1.MessageLike, {
        as: "likes",
        foreignKey: "messageId"
    });
    Message.hasMany(message_comment_1.MessageComment, {
        as: "comments",
        foreignKey: "messageId"
    });
}
exports.initMessage = initMessage;
