"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
const project_1 = require("./project");
const task_group_1 = require("./task-group");
class SimpleTask extends Sequelize.Model {
}
exports.SimpleTask = SimpleTask;
function initSimpleTask(sequelize) {
    SimpleTask.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, taskCode: {
            type: Sequelize.TEXT,
            allowNull: false
        }, title: {
            type: Sequelize.TEXT,
            allowNull: false
        }, description: {
            type: Sequelize.TEXT,
            allowNull: false
        }, taskStatus: {
            type: Sequelize.TEXT,
            allowNull: false
        }, projectId: {
            type: Sequelize.BIGINT,
            allowNull: true,
            references: {
                model: project_1.SimpleProject,
                key: "id"
            }
        }, groupId: {
            type: Sequelize.BIGINT,
            allowNull: true,
            references: {
                model: task_group_1.SimpleTaskGroup,
                key: "id"
            }
        }, startDate: {
            type: Sequelize.DATE,
            allowNull: true,
        }, endDate: {
            type: Sequelize.DATE,
            allowNull: true,
        }, deadline: {
            type: Sequelize.DATE,
            allowNull: true,
        }, estimation: {
            type: Sequelize.FLOAT,
            allowNull: false,
            defaultValue: 0
        }, reporterId: {
            type: Sequelize.BIGINT,
            allowNull: true,
        }, assigneeId: {
            type: Sequelize.BIGINT,
            allowNull: true,
        }, labels: {
            type: Sequelize.ARRAY(Sequelize.TEXT),
            allowNull: false,
            defaultValue: [],
        }, memberIds: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        }, activities: {
            type: Sequelize.JSON,
            allowNull: false,
            defaultValue: {},
        }, isPrivate: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }, rating: {
            type: Sequelize.TEXT,
            allowNull: true
        }, readers: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        } }, common_1.defaultColumns), {
        tableName: "tasks",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
    SimpleTask.belongsTo(project_1.SimpleProject, {
        as: "project",
        foreignKey: "projectId"
    });
    SimpleTask.belongsTo(task_group_1.SimpleTaskGroup, {
        as: "group",
        foreignKey: "groupId"
    });
}
exports.initSimpleTask = initSimpleTask;
