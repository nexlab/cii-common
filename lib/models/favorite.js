"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
class Favorite extends Sequelize.Model {
}
exports.Favorite = Favorite;
function initFavorite(sequelize) {
    Favorite.init({
        id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        },
        avatarUrl: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        type: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        relateId: {
            type: Sequelize.BIGINT,
            allowNull: false
        },
        userId: {
            type: Sequelize.BIGINT,
            allowNull: false
        },
        metadata: {
            type: Sequelize.JSON,
            allowNull: false,
            defaultValue: {}
        },
        title: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        status: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: common_1.StatusCode.Active
        }
    }, {
        tableName: "favorites",
        sequelize,
        timestamps: false,
    });
}
exports.initFavorite = initFavorite;
