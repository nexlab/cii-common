import * as Sequelize from "sequelize";
import { Attachment } from "../file/models/attachment";
import { StatusCode } from "./common";
import { MessageCategory } from "./message-category";
import { MessageComment } from "./message-comment";
import { MessageLike } from "./message-like";
import { User } from "./user";
export interface IMessageCount {
    messageId: string;
    count: number;
}
export declare enum MessageActivityType {
    CreateMessage = "create_message",
    UpdateMessage = "update_message",
    OwnerEditMessage = "owner_edit_message",
    CloseMessage = "close_message",
    DeleteMessage = "delete_message",
    AddComment = "add_comment",
    EditComment = "edit_comment",
    DeleteComment = "delete_comment",
    AddLike = "add_like",
    DeleteLike = "delete_like"
}
export declare enum FilterMyMessage {
    All = "all",
    Owner = "owner",
    OnlyMember = "onlyMember",
    Closed = "closed"
}
export interface IMessageActivity {
    description: string;
    type: MessageActivityType;
    userId: number;
    createdAt: string;
    relatedUser?: User;
}
export declare function addMessageActivity(activities: IMessageActivity[] | string, activity: IMessageActivity): IMessageActivity[];
export declare class Message extends Sequelize.Model {
    id: string;
    title: string;
    content: string;
    categoryId: number;
    memberIds: Array<number | string>;
    subscribers: Array<number | string>;
    labels: string[];
    attachmentIds: number[];
    readers: string[];
    isClosed: boolean;
    activities: IMessageActivity[];
    createdAt: Date;
    createdBy: number | string;
    updatedAt: Date;
    updatedBy: number | string;
    deletedAt: Date;
    status: StatusCode;
    comments?: MessageComment[];
    likes?: MessageLike[];
    category?: MessageCategory;
    MessageCategory?: MessageCategory;
    User?: User;
    owner?: User;
    attachments?: Attachment[];
    members?: User[];
    notificationSubscribers?: User[];
    isRead?: boolean;
    priorityType: string;
}
export interface IMessageItem extends Message {
    numberOfComment: number;
    numberOfLike: number;
    meLike: boolean;
}
export declare function initMessage(sequelize: Sequelize.Sequelize): void;
