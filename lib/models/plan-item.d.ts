import * as Sequelize from "sequelize";
import { StatusCode } from "./common";
import { SimplePlan } from "./plan";
import { User } from "./user";
export declare enum PlanItemType {
    Normal = "normal",
    Milestone = "milestone"
}
export declare class PlanItem extends Sequelize.Model {
    id: string;
    planId: string;
    groupId?: string;
    title: string;
    content: string;
    type: PlanItemType;
    startDate: Date;
    deadline: Date;
    progress: number;
    reminder: boolean;
    assigneeId: string;
    status: StatusCode;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    deletedAt: Date;
    assignee?: User;
    owner?: User;
    plan?: SimplePlan;
}
export declare function initPlanItem(sequelize: Sequelize.Sequelize): void;
