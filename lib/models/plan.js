"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
class SimplePlan extends Sequelize.Model {
}
exports.SimplePlan = SimplePlan;
function initSimplePlan(sequelize) {
    SimplePlan.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, name: {
            type: Sequelize.TEXT,
            allowNull: false
        }, description: {
            type: Sequelize.TEXT,
            allowNull: false
        }, startDate: {
            type: Sequelize.DATE,
            allowNull: false,
        }, endDate: {
            type: Sequelize.DATE,
            allowNull: true,
        }, memberIds: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        }, subscribers: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        }, remindTime: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 24
        }, status: {
            type: Sequelize.TEXT,
            allowNull: false,
        } }, common_1.defaultColumns), {
        tableName: "plans",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initSimplePlan = initSimplePlan;
