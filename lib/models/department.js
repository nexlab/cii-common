"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
const user_1 = require("./user");
class Department extends Sequelize.Model {
}
exports.Department = Department;
function initDepartment(sequelize) {
    Department.init(Object.assign({ id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        }, weight: {
            type: Sequelize.INTEGER,
            allowNull: true
        }, departmentName: {
            type: Sequelize.TEXT,
            allowNull: false
        }, description: {
            type: Sequelize.TEXT,
            allowNull: true
        }, imageUrl: {
            type: Sequelize.TEXT,
            allowNull: true
        }, departmentLeader: {
            type: Sequelize.BIGINT,
            allowNull: true,
            references: {
                model: "users",
                key: "id"
            }
        }, status: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: "active"
        } }, common_1.defaultColumns), {
        tableName: "departments",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
    Department.belongsTo(user_1.User, { foreignKey: "departmentLeader", as: "leader" });
}
exports.initDepartment = initDepartment;
