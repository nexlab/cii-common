"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Action;
(function (Action) {
    Action["UserCreate"] = "user.create";
    Action["UserUpdate"] = "user.update";
    Action["UserDelete"] = "user.delete";
    Action["UserGet"] = "user.get";
    Action["UserList"] = "user.list";
    Action["UserPicker"] = "user.picker";
    Action["UserChangePassword"] = "user.change-password";
    Action["RoleCreate"] = "role.create";
    Action["RoleUpdate"] = "role.update";
    Action["RoleDelete"] = "role.delete";
    Action["RoleGet"] = "role.get";
    Action["RoleList"] = "role.list";
    Action["RoleListAction"] = "role.listAction";
    Action["DepartmentCreate"] = "department.create";
    Action["DepartmentUpdate"] = "department.update";
    Action["DepartmentDelete"] = "department.delete";
    Action["DepartmentGet"] = "department.get";
    Action["DepartmentList"] = "department.list";
    Action["EducationCreate"] = "education.create";
    Action["EducationUpdate"] = "education.update";
    Action["EducationDelete"] = "education.delete";
    Action["EducationGet"] = "education.get";
    Action["EducationList"] = "education.list";
    Action["MajorCreate"] = "major.create";
    Action["MajorUpdate"] = "major.update";
    Action["MajorDelete"] = "major.delete";
    Action["MajorGet"] = "major.get";
    Action["MajorList"] = "major.list";
    Action["CompanyCreate"] = "company-title.create";
    Action["CompanyUpdate"] = "company-title.update";
    Action["CompanyDelete"] = "company-title.delete";
    Action["CompanyGet"] = "company-title.get";
    Action["CompanyList"] = "company-title.list";
    Action["MessageCreate"] = "message.create";
    Action["MessageUpdate"] = "message.update";
    Action["MessageDelete"] = "message.delete";
    Action["MessageGet"] = "message.get";
    Action["MessageList"] = "message.list";
    Action["MessageCategoryCreate"] = "message-category.create";
    Action["MessageCategoryUpdate"] = "message-category.update";
    Action["MessageCategoryDelete"] = "message-category.delete";
    Action["MessageCategoryGet"] = "message-category.get";
    Action["MessageCategoryList"] = "message-category.list";
    Action["TopicCreate"] = "topic.create";
    Action["TopicUpdate"] = "topic.update";
    Action["TopicDelete"] = "topic.delete";
    Action["TopicGet"] = "topic.get";
    Action["TopicList"] = "topic.list";
    Action["TopicCategoryCreate"] = "topic-category.create";
    Action["TopicCategoryUpdate"] = "topic-category.update";
    Action["TopicCategoryDelete"] = "topic-category.delete";
    Action["TopicCategoryGet"] = "topic-category.get";
    Action["TopicCategoryList"] = "topic-category.list";
    Action["PlanCreate"] = "plan.create";
    Action["PlanUpdate"] = "plan.update";
    Action["PlanDelete"] = "plan.delete";
    Action["PlanGet"] = "plan.get";
    Action["PlanList"] = "plan.list";
    Action["PlanOwn"] = "plan.own";
    Action["PlanItemCreate"] = "plan-item.create";
    Action["PlanItemUpdate"] = "plan-item.update";
    Action["PlanItemDelete"] = "plan-item.delete";
    Action["PlanItemGet"] = "plan-item.get";
    Action["PlanItemList"] = "plan-item.list";
    Action["PlanItemGroupCreate"] = "plan-item-group.create";
    Action["PlanItemGroupUpdate"] = "plan-item-group.update";
    Action["PlanItemGroupDelete"] = "plan-item-group.delete";
    Action["PlanItemGroupGet"] = "plan-item-group.get";
    Action["PlanItemGroupList"] = "plan-item-group.list";
    Action["ProjectCreate"] = "project.create";
    Action["ProjectUpdate"] = "project.update";
    Action["ProjectDelete"] = "project.delete";
    Action["ProjectGet"] = "project.get";
    Action["ProjectList"] = "project.list";
    Action["ProjectOwner"] = "project.owner";
    Action["ProjectTemplateCreate"] = "project-template.create";
    Action["ProjectTemplateUpdate"] = "project-template.update";
    Action["ProjectTemplateDelete"] = "project-template.delete";
    Action["ProjectTemplateGet"] = "project-template.get";
    Action["ProjectTemplateList"] = "project-template.list";
    Action["ProjectCategoryCreate"] = "project-category.create";
    Action["ProjectCategoryUpdate"] = "project-category.update";
    Action["ProjectCategoryDelete"] = "project-category.delete";
    Action["ProjectCategoryGet"] = "project-category.get";
    Action["ProjectCategoryList"] = "project-category.list";
    Action["TaskCreate"] = "task.create";
    Action["TaskUpdate"] = "task.update";
    Action["TaskDelete"] = "task.delete";
    Action["TaskGet"] = "task.get";
    Action["TaskList"] = "task.list";
    Action["TaskCommentCreate"] = "task-comment.create";
    Action["TaskCommentUpdate"] = "task-comment.update";
    Action["TaskCommentDelete"] = "task-comment.delete";
    Action["TaskByProject"] = "task.by-project";
    Action["TaskMove"] = "task.move";
    Action["TaskGroupCreate"] = "task-group.create";
    Action["TaskGroupUpdate"] = "task-group.update";
    Action["TaskGroupDelete"] = "task-group.delete";
    Action["TaskGroupGet"] = "task-group.get";
    Action["TaskGroupList"] = "task-group.list";
    Action["TaskGroupByProject"] = "task-group.by-project";
    Action["TaskRatingCreate"] = "task-rating.create";
    Action["TaskRatingUpdate"] = "task-rating.update";
    Action["TaskRatingDelete"] = "task-rating.delete";
    Action["TaskRatingGet"] = "task-rating.get";
    Action["TaskRatingList"] = "task-rating.list";
    Action["FeedbackUpdateResolved"] = "feedback.update-resolved";
    Action["ReportDepartmentList"] = "report.department-list";
    Action["ReportUserList"] = "report.user-list";
    Action["ReportDepartmentDetail"] = "report.department-detail";
    Action["ReportUserDetail"] = "report.user-detail";
    Action["ReportDepartmentProjectList"] = "report.department-project-list";
    Action["ReportDepartmentUserList"] = "report.department-user-list";
    Action["ReportUserTaskList"] = "report.user-task-list";
    Action["FileList"] = "file.list";
    Action["FileDirectoryList"] = "file.directory-list";
    Action["FileDirectoryDetail"] = "file.directory-detail";
    Action["FileDirectoryCreate"] = "file.directory-create";
    Action["FileDirectoryEdit"] = "file.directory-edit";
    Action["FileDirectoryDelete"] = "file.directory-delete";
    Action["FileFileList"] = "file.file-list";
    Action["FileFileDetail"] = "file.file-detail";
    Action["FileFileCreate"] = "file.file-create";
    Action["FileFileEdit"] = "file.file-edit";
    Action["FileFileDelete"] = "file.file-delete";
})(Action = exports.Action || (exports.Action = {}));
exports.ActionGroups = {
    user: [
        Action.UserCreate, Action.UserGet,
        Action.UserUpdate, Action.UserDelete,
        Action.UserList, Action.UserChangePassword
    ],
    role: [
        Action.RoleCreate, Action.RoleDelete,
        Action.RoleGet, Action.RoleList,
        Action.RoleListAction, Action.RoleUpdate
    ],
    department: [
        Action.DepartmentCreate, Action.DepartmentDelete,
        Action.DepartmentList, Action.DepartmentUpdate,
        Action.DepartmentGet
    ],
    education: [
        Action.EducationCreate, Action.EducationDelete,
        Action.EducationGet, Action.EducationList,
        Action.EducationUpdate
    ],
    major: [
        Action.MajorCreate, Action.MajorDelete,
        Action.MajorGet, Action.MajorList,
        Action.MajorUpdate
    ],
    company: [
        Action.CompanyCreate, Action.CompanyDelete,
        Action.CompanyGet, Action.CompanyList,
        Action.CompanyUpdate
    ],
    message: [
        Action.MessageCreate, Action.MessageDelete,
        Action.MessageGet, Action.MessageList,
        Action.MessageUpdate
    ],
    "message-category": [
        Action.MessageCategoryCreate, Action.MessageCategoryDelete,
        Action.MessageCategoryGet,
        Action.MessageCategoryList,
        Action.MessageCategoryUpdate
    ],
    plan: [
        Action.PlanCreate,
        Action.PlanDelete,
        Action.PlanGet,
        Action.PlanList,
        Action.PlanUpdate,
        Action.PlanOwn
    ],
    "plan-item-group": [
        Action.PlanItemGroupCreate,
        Action.PlanItemGroupDelete,
        Action.PlanItemGroupGet,
        Action.PlanItemGroupList,
        Action.PlanItemGroupUpdate
    ],
    "plan-item": [
        Action.PlanItemCreate,
        Action.PlanItemDelete,
        Action.PlanItemGet,
        Action.PlanItemList,
        Action.PlanItemUpdate
    ],
    project: [
        Action.ProjectCreate,
        Action.ProjectDelete,
        Action.ProjectGet,
        Action.ProjectList,
        Action.ProjectOwner,
        Action.ProjectUpdate
    ],
    "project-category": [
        Action.ProjectCategoryCreate,
        Action.ProjectCategoryDelete,
        Action.ProjectCategoryGet,
        Action.ProjectCategoryList,
        Action.ProjectCategoryUpdate
    ],
    "project-template": [
        Action.ProjectTemplateCreate,
        Action.ProjectTemplateDelete,
        Action.ProjectTemplateGet,
        Action.ProjectTemplateList,
        Action.ProjectTemplateUpdate
    ],
    "task-rating": [
        Action.TaskRatingCreate,
        Action.TaskRatingDelete,
        Action.TaskRatingGet,
        Action.TaskRatingList,
        Action.TaskRatingUpdate
    ],
    "task-group": [
        Action.TaskGroupCreate,
        Action.TaskGroupDelete,
        Action.TaskGroupGet,
        Action.TaskGroupList,
        Action.TaskGroupUpdate,
        Action.TaskGroupByProject
    ],
    task: [
        Action.TaskCreate,
        Action.TaskDelete,
        Action.TaskCommentCreate,
        Action.TaskCommentUpdate,
        Action.TaskCommentDelete,
        Action.TaskGet,
        Action.TaskList,
        Action.TaskUpdate,
        Action.TaskByProject,
        Action.TaskMove
    ],
    feedback: [
        Action.FeedbackUpdateResolved
    ],
    report: [
        Action.ReportDepartmentList,
        Action.ReportUserList,
        Action.ReportDepartmentDetail,
        Action.ReportUserDetail,
        Action.ReportDepartmentProjectList,
        Action.ReportDepartmentUserList,
        Action.ReportUserTaskList,
    ],
    file: [
        Action.FileDirectoryList,
        Action.FileDirectoryDetail,
        Action.FileDirectoryCreate,
        Action.FileDirectoryEdit,
        Action.FileDirectoryDelete,
        Action.FileFileList,
        Action.FileFileDetail,
        Action.FileFileCreate,
        Action.FileFileEdit,
        Action.FileFileDelete,
    ],
};
var MenuItem;
(function (MenuItem) {
    MenuItem["StaffManagement"] = "staff_management";
    MenuItem["DataManagement"] = "data_management";
    MenuItem["PlanSchedule"] = "plan_schedule";
    MenuItem["ProjectManagement"] = "project_management";
    MenuItem["NotificationSystem"] = "notification_system";
    MenuItem["ReportExtract"] = "report_extract";
    MenuItem["InternalNews"] = "internal_news";
    MenuItem["InterDepartmental"] = "inter-departmental";
    MenuItem["MessageSystem"] = "message_system";
    MenuItem["Setting"] = "setting";
})(MenuItem = exports.MenuItem || (exports.MenuItem = {}));
exports.MenuItems = [
    MenuItem.StaffManagement, MenuItem.DataManagement,
    MenuItem.PlanSchedule, MenuItem.ProjectManagement,
    MenuItem.NotificationSystem, MenuItem.ReportExtract,
    MenuItem.InternalNews, MenuItem.InterDepartmental,
    MenuItem.MessageSystem, MenuItem.Setting
];
exports.Permissions = {
    Admin: "admin",
    User: "user"
};
