export declare enum Action {
    UserCreate = "user.create",
    UserUpdate = "user.update",
    UserDelete = "user.delete",
    UserGet = "user.get",
    UserList = "user.list",
    UserPicker = "user.picker",
    UserChangePassword = "user.change-password",
    RoleCreate = "role.create",
    RoleUpdate = "role.update",
    RoleDelete = "role.delete",
    RoleGet = "role.get",
    RoleList = "role.list",
    RoleListAction = "role.listAction",
    DepartmentCreate = "department.create",
    DepartmentUpdate = "department.update",
    DepartmentDelete = "department.delete",
    DepartmentGet = "department.get",
    DepartmentList = "department.list",
    EducationCreate = "education.create",
    EducationUpdate = "education.update",
    EducationDelete = "education.delete",
    EducationGet = "education.get",
    EducationList = "education.list",
    MajorCreate = "major.create",
    MajorUpdate = "major.update",
    MajorDelete = "major.delete",
    MajorGet = "major.get",
    MajorList = "major.list",
    CompanyCreate = "company-title.create",
    CompanyUpdate = "company-title.update",
    CompanyDelete = "company-title.delete",
    CompanyGet = "company-title.get",
    CompanyList = "company-title.list",
    MessageCreate = "message.create",
    MessageUpdate = "message.update",
    MessageDelete = "message.delete",
    MessageGet = "message.get",
    MessageList = "message.list",
    MessageCategoryCreate = "message-category.create",
    MessageCategoryUpdate = "message-category.update",
    MessageCategoryDelete = "message-category.delete",
    MessageCategoryGet = "message-category.get",
    MessageCategoryList = "message-category.list",
    TopicCreate = "topic.create",
    TopicUpdate = "topic.update",
    TopicDelete = "topic.delete",
    TopicGet = "topic.get",
    TopicList = "topic.list",
    TopicCategoryCreate = "topic-category.create",
    TopicCategoryUpdate = "topic-category.update",
    TopicCategoryDelete = "topic-category.delete",
    TopicCategoryGet = "topic-category.get",
    TopicCategoryList = "topic-category.list",
    PlanCreate = "plan.create",
    PlanUpdate = "plan.update",
    PlanDelete = "plan.delete",
    PlanGet = "plan.get",
    PlanList = "plan.list",
    PlanOwn = "plan.own",
    PlanItemCreate = "plan-item.create",
    PlanItemUpdate = "plan-item.update",
    PlanItemDelete = "plan-item.delete",
    PlanItemGet = "plan-item.get",
    PlanItemList = "plan-item.list",
    PlanItemGroupCreate = "plan-item-group.create",
    PlanItemGroupUpdate = "plan-item-group.update",
    PlanItemGroupDelete = "plan-item-group.delete",
    PlanItemGroupGet = "plan-item-group.get",
    PlanItemGroupList = "plan-item-group.list",
    ProjectCreate = "project.create",
    ProjectUpdate = "project.update",
    ProjectDelete = "project.delete",
    ProjectGet = "project.get",
    ProjectList = "project.list",
    ProjectOwner = "project.owner",
    ProjectTemplateCreate = "project-template.create",
    ProjectTemplateUpdate = "project-template.update",
    ProjectTemplateDelete = "project-template.delete",
    ProjectTemplateGet = "project-template.get",
    ProjectTemplateList = "project-template.list",
    ProjectCategoryCreate = "project-category.create",
    ProjectCategoryUpdate = "project-category.update",
    ProjectCategoryDelete = "project-category.delete",
    ProjectCategoryGet = "project-category.get",
    ProjectCategoryList = "project-category.list",
    TaskCreate = "task.create",
    TaskUpdate = "task.update",
    TaskDelete = "task.delete",
    TaskGet = "task.get",
    TaskList = "task.list",
    TaskCommentCreate = "task-comment.create",
    TaskCommentUpdate = "task-comment.update",
    TaskCommentDelete = "task-comment.delete",
    TaskByProject = "task.by-project",
    TaskMove = "task.move",
    TaskGroupCreate = "task-group.create",
    TaskGroupUpdate = "task-group.update",
    TaskGroupDelete = "task-group.delete",
    TaskGroupGet = "task-group.get",
    TaskGroupList = "task-group.list",
    TaskGroupByProject = "task-group.by-project",
    TaskRatingCreate = "task-rating.create",
    TaskRatingUpdate = "task-rating.update",
    TaskRatingDelete = "task-rating.delete",
    TaskRatingGet = "task-rating.get",
    TaskRatingList = "task-rating.list",
    FeedbackUpdateResolved = "feedback.update-resolved",
    ReportDepartmentList = "report.department-list",
    ReportUserList = "report.user-list",
    ReportDepartmentDetail = "report.department-detail",
    ReportUserDetail = "report.user-detail",
    ReportDepartmentProjectList = "report.department-project-list",
    ReportDepartmentUserList = "report.department-user-list",
    ReportUserTaskList = "report.user-task-list",
    FileList = "file.list",
    FileDirectoryList = "file.directory-list",
    FileDirectoryDetail = "file.directory-detail",
    FileDirectoryCreate = "file.directory-create",
    FileDirectoryEdit = "file.directory-edit",
    FileDirectoryDelete = "file.directory-delete",
    FileFileList = "file.file-list",
    FileFileDetail = "file.file-detail",
    FileFileCreate = "file.file-create",
    FileFileEdit = "file.file-edit",
    FileFileDelete = "file.file-delete"
}
export declare const ActionGroups: {
    user: Action[];
    role: Action[];
    department: Action[];
    education: Action[];
    major: Action[];
    company: Action[];
    message: Action[];
    "message-category": Action[];
    plan: Action[];
    "plan-item-group": Action[];
    "plan-item": Action[];
    project: Action[];
    "project-category": Action[];
    "project-template": Action[];
    "task-rating": Action[];
    "task-group": Action[];
    task: Action[];
    feedback: Action[];
    report: Action[];
    file: Action[];
};
export declare enum MenuItem {
    StaffManagement = "staff_management",
    DataManagement = "data_management",
    PlanSchedule = "plan_schedule",
    ProjectManagement = "project_management",
    NotificationSystem = "notification_system",
    ReportExtract = "report_extract",
    InternalNews = "internal_news",
    InterDepartmental = "inter-departmental",
    MessageSystem = "message_system",
    Setting = "setting"
}
export declare const MenuItems: MenuItem[];
export declare const Permissions: {
    Admin: string;
    User: string;
};
