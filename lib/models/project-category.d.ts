import * as Sequelize from "sequelize";
import { StatusCode } from "./common";
export declare class ProjectCategory extends Sequelize.Model {
    id: string;
    name: string;
    description: string;
    status: StatusCode;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    deletedAt: Date;
}
export declare function initProjectCategory(sequelize: Sequelize.Sequelize): void;
