import * as Sequelize from "sequelize";
export declare class Major extends Sequelize.Model {
    id: number;
    majorName: string;
    description: string;
}
export declare function initMajor(sequelize: Sequelize.Sequelize): void;
