import * as Sequelize from "sequelize";
import { Attachment } from "../file/models/attachment";
import { User } from "./user";
export declare class MessageComment extends Sequelize.Model {
    id: number;
    content: string;
    messageId: number | string;
    parentId: number | string;
    createdAt: Date;
    createdBy: number | string;
    updatedAt: Date;
    updatedBy: number | string;
    deletedAt: Date;
    attachments: Attachment[];
    owner?: User;
}
export declare function initMessageComment(sequelize: Sequelize.Sequelize): void;
