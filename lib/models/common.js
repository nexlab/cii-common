"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
var StatusCode;
(function (StatusCode) {
    StatusCode["Active"] = "active";
    StatusCode["Deactivate"] = "deactivate";
    StatusCode["Deleted"] = "deleted";
    StatusCode["HardDeleted"] = "hardDeleted";
})(StatusCode = exports.StatusCode || (exports.StatusCode = {}));
var Gender;
(function (Gender) {
    Gender["Male"] = "male";
    Gender["Female"] = "female";
})(Gender = exports.Gender || (exports.Gender = {}));
var ReferenceType;
(function (ReferenceType) {
    ReferenceType["Topic"] = "topic";
    ReferenceType["Message"] = "message";
    ReferenceType["Plan"] = "plan";
    ReferenceType["Task"] = "project-task";
    ReferenceType["Project"] = "project";
    ReferenceType["PlanItem"] = "plan-item";
    ReferenceType["Feedback"] = "feedback";
    ReferenceType["Help"] = "help";
    ReferenceType["TaskComment"] = "project-task-comment";
    ReferenceType["MessageComment"] = "message-comment";
    ReferenceType["TopicComment"] = "topic-comment";
    ReferenceType["Department"] = "department";
    ReferenceType["CustomRootFolder"] = "custom-root-folder";
})(ReferenceType = exports.ReferenceType || (exports.ReferenceType = {}));
// default column for init model
exports.defaultColumns = {
    createdAt: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: Sequelize.NOW,
    },
    createdBy: {
        type: Sequelize.BIGINT,
        allowNull: true,
    },
    updatedAt: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: Sequelize.NOW,
    },
    updatedBy: {
        type: Sequelize.BIGINT,
        allowNull: true,
    },
    deletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
    },
};
