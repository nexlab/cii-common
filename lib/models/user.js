"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
const company_1 = require("./company");
const role_1 = require("./role");
class User extends Sequelize.Model {
}
exports.User = User;
function initUser(sequelize) {
    User.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, email: {
            type: Sequelize.TEXT,
            allowNull: false
        }, username: {
            type: Sequelize.TEXT,
            allowNull: true
        }, phoneCode: {
            type: Sequelize.TEXT,
            allowNull: true
        }, phoneNumber: {
            type: Sequelize.TEXT,
            allowNull: true
        }, firstName: {
            type: Sequelize.TEXT,
            allowNull: true
        }, lastName: {
            type: Sequelize.TEXT,
            allowNull: true
        }, gender: {
            type: Sequelize.TEXT,
            allowNull: true
        }, description: {
            type: Sequelize.TEXT,
            allowNull: true
        }, notificationEmail: {
            type: Sequelize.TEXT,
            allowNull: true
        }, homeTown: {
            type: Sequelize.TEXT,
            allowNull: true
        }, address: {
            type: Sequelize.TEXT,
            allowNull: true
        }, birthday: {
            type: Sequelize.DATEONLY,
            allowNull: true
        }, onboardDate: {
            type: Sequelize.DATEONLY,
            allowNull: true
        }, avatarUrl: {
            type: Sequelize.TEXT,
            allowNull: true
        }, identifierNumber: {
            type: Sequelize.TEXT,
            allowNull: true
        }, authId: {
            type: Sequelize.TEXT,
            allowNull: false,
            unique: true
        }, roleId: {
            type: Sequelize.INTEGER,
            allowNull: true,
            references: {
                model: "roles",
                key: "id"
            }
        }, companyId: {
            type: Sequelize.INTEGER,
            allowNull: true
        }, majorId: {
            type: Sequelize.INTEGER,
            allowNull: true
        }, educationId: {
            type: Sequelize.INTEGER,
            allowNull: true
        }, departmentIds: {
            type: Sequelize.ARRAY(Sequelize.INTEGER),
            allowNull: true
        }, superuser: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }, metadata: {
            type: Sequelize.JSON,
            allowNull: true,
            defaultValue: {}
        }, status: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: "active"
        } }, common_1.defaultColumns), {
        tableName: "users",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
    User.belongsTo(role_1.Role, { foreignKey: "roleId" });
    User.belongsTo(company_1.Company, { foreignKey: "companyId", as: "company" });
}
exports.initUser = initUser;
function getFullName(user) {
    return `${user.firstName} ${user.lastName}`;
}
exports.getFullName = getFullName;
