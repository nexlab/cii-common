import * as Sequelize from "sequelize";
import { StatusCode } from "./common";
export declare class SimplePlan extends Sequelize.Model {
    id: string;
    name: string;
    description: string;
    startDate: Date;
    endDate: Date;
    memberIds: string[];
    subscribers: string[];
    setting?: any;
    remindTime: number;
    status: StatusCode;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    deletedAt: Date;
}
export declare function initSimplePlan(sequelize: Sequelize.Sequelize): void;
