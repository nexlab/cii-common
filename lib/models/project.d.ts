import * as Sequelize from "sequelize";
import { StatusCode } from "./common";
import { SimpleProjectTemplate } from "./project-template";
export declare enum MyProjectFilter {
    All = "all",
    Owner = "owner",
    OnlyMember = "onlyMember",
    Closed = "closed",
    ExpiryDate = "expiryDate"
}
export declare class SimpleProject extends Sequelize.Model {
    id: string;
    projectCode: string;
    name: string;
    description: string;
    avatarUrl: string;
    startDate: Date;
    endDate: Date;
    leaderId: string;
    categoryId: string;
    templateId: string;
    memberIds: string[];
    subscribers: string[];
    reminder: boolean;
    isClosed: boolean;
    remindTime: number;
    numberOfTask?: number;
    status: StatusCode;
    ratingId: string;
    template: SimpleProjectTemplate;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    deletedAt: Date;
}
export declare function initSimpleProject(sequelize: Sequelize.Sequelize): void;
