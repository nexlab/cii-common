"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
class SimpleProjectTemplate extends Sequelize.Model {
}
exports.SimpleProjectTemplate = SimpleProjectTemplate;
function initSimpleProjectTemplate(sequelize) {
    SimpleProjectTemplate.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, name: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: ""
        }, description: {
            type: Sequelize.TEXT,
            allowNull: true
        }, flow: {
            type: Sequelize.JSON,
            allowNull: false,
            defaultValue: {}
        } }, common_1.defaultColumns), {
        tableName: "project_templates",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initSimpleProjectTemplate = initSimpleProjectTemplate;
