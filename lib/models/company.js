"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const common_1 = require("./common");
// import { User } from "./user";
class Company extends Sequelize.Model {
}
exports.Company = Company;
function initCompany(sequelize) {
    Company.init(Object.assign({ id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        }, companyTitleName: {
            type: Sequelize.TEXT,
            allowNull: false
        }, description: {
            type: Sequelize.TEXT,
            allowNull: true
        } }, common_1.defaultColumns), {
        tableName: "company_title",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
    // Company.hasMany(User, {
    //   as: "users",
    //   foreignKey: "companyId"
    // });
}
exports.initCompany = initCompany;
