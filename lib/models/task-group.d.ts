import * as Sequelize from "sequelize";
import { StatusCode } from "./common";
import { SimpleProject } from "./project";
export declare class SimpleTaskGroup extends Sequelize.Model {
    id: string;
    name: string;
    description: string;
    status: StatusCode;
    color: string;
    projectId: string;
    project?: SimpleProject;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    deletedAt: Date;
}
export declare function initSimpleTaskGroup(sequelize: Sequelize.Sequelize): void;
