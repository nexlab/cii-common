import * as Sequelize from "sequelize";
import { User } from "./user";
export declare enum LikeAction {
    Liked = "liked",
    Disliked = "disliked"
}
export declare class MessageLike extends Sequelize.Model {
    id: number;
    action: LikeAction;
    messageId: number;
    createdAt: Date;
    createdBy: number | string;
    updatedAt: Date;
    updatedBy: number | string;
    deletedAt: Date;
    owner?: User;
}
export declare function initMessageLike(sequelize: Sequelize.Sequelize): void;
