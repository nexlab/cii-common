import { Company, Education, Major } from "../models";
export declare function serializeMajor(model: Major): any;
export declare function serializeCompany(model: Company): any;
export declare function serializeEducation(model: Education): any;
