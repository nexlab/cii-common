"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function serializeSimpleRole(model) {
    return {
        id: model.id,
        roleName: model.roleName,
        menuItems: model.menuItems
    };
}
exports.serializeSimpleRole = serializeSimpleRole;
