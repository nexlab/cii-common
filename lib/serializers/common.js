"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("../helpers");
const user_1 = require("./user");
function metadataSerializer(input) {
    return {
        createdAt: input.createdAt,
        createdBy: parseSimpleUser(input.createdBy),
        updatedAt: input.updatedAt,
        updatedBy: parseSimpleUser(input.updatedBy)
    };
}
exports.metadataSerializer = metadataSerializer;
function parseSimpleUser(data) {
    if (data !== null && typeof data === "object" && Object.keys(data).length > 0) {
        return user_1.serializeSimpleUser(data);
    }
    return helpers_1.safeParseInt(data, null);
}
