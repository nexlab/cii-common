"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("../helpers");
function serializeSimpleUser(user) {
    return user ? {
        id: helpers_1.safeParseInt(user.id, null),
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        avatarUrl: user.avatarUrl,
        onboardDate: user.onboardDate,
    } : null;
}
exports.serializeSimpleUser = serializeSimpleUser;
