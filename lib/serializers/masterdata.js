"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("../helpers");
function serializeMajor(model) {
    return {
        id: helpers_1.safeParseInt(model.id, null),
        majorName: model.majorName,
        description: model.description
    };
}
exports.serializeMajor = serializeMajor;
function serializeCompany(model) {
    return {
        id: helpers_1.safeParseInt(model.id, null),
        companyTitleName: model.companyTitleName,
        description: model.description
    };
}
exports.serializeCompany = serializeCompany;
function serializeEducation(model) {
    return {
        id: helpers_1.safeParseInt(model.id, null),
        educationName: model.educationName,
        description: model.description
    };
}
exports.serializeEducation = serializeEducation;
