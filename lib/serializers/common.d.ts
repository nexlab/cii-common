export declare function metadataSerializer(input: any): {
    createdAt: any;
    createdBy: any;
    updatedAt: any;
    updatedBy: any;
};
