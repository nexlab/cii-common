import { User } from "../models";
export interface ISimpleUser {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    avatarUrl: string;
    onboardDate: Date;
}
export declare function serializeSimpleUser(user: User): ISimpleUser;
