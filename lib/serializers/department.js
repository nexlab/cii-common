"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function serializeDepartment(model) {
    return {
        id: model.id,
        weight: model.weight,
        departmentName: model.departmentName,
        imageUrl: model.imageUrl,
        description: model.description,
        departmentLeader: model.departmentLeader
    };
}
exports.serializeDepartment = serializeDepartment;
