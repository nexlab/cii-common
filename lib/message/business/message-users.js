"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash = require("lodash");
const message_users_1 = require("../model/message-users");
function createMessageUsers(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const messageUsers = yield message_users_1.MessageUsers.findAll({
                where: {
                    userId: data.memberIds
                }
            });
            function parseIds(reverse = false) {
                return messageUsers.filter((e) => {
                    const current = data.memberIds.find((a) => a.toString() === e.userId.toString());
                    if (current) {
                        return reverse;
                    }
                    return !reverse;
                });
            }
            const messageUsersNeedCreate = data.memberIds.filter((e) => {
                const current = messageUsers.find((a) => e.toString() === a.userId.toString());
                if (current) {
                    return false;
                }
                return true;
            });
            const messageUsersNeedUpdate = parseIds(true);
            if (messageUsersNeedCreate && messageUsersNeedCreate.length > 0) {
                yield Promise.all(messageUsersNeedCreate.map((e) => {
                    return message_users_1.MessageUsers.create({
                        userId: e,
                        messageIdsUnRead: [data.messageId]
                    });
                }));
            }
            if (messageUsersNeedUpdate && messageUsersNeedUpdate.length > 0) {
                yield Promise.all(messageUsersNeedUpdate.map((e) => {
                    e.messageIdsUnRead.push(data.messageId);
                    return message_users_1.MessageUsers.update({
                        messageIdsUnRead: lodash.uniq(e.messageIdsUnRead)
                    }, {
                        where: { userId: e.userId, }
                    });
                }));
            }
            return;
        }
        catch (error) {
            return Promise.reject(error);
        }
    });
}
exports.createMessageUsers = createMessageUsers;
function markMessageUserAsRead(userId) {
    return __awaiter(this, void 0, void 0, function* () {
        const current = yield message_users_1.MessageUsers.find({
            where: { userId }
        });
        let result = [];
        if (current) {
            result = current.messageIdsUnRead || [];
        }
        if (result.length > 0) {
            yield message_users_1.MessageUsers.update({ messageIdsUnRead: [] }, {
                where: { userId }
            });
        }
        return result;
    });
}
exports.markMessageUserAsRead = markMessageUserAsRead;
