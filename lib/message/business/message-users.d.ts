export interface ICreateMessageUsers {
    memberIds: Array<string | number>;
    messageId: string;
}
export declare function createMessageUsers(data: ICreateMessageUsers): Promise<any>;
export declare function markMessageUserAsRead(userId: number | string): Promise<number[]>;
