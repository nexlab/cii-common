import * as Sequelize from "sequelize";
export declare class MessageUsers extends Sequelize.Model {
    id: number;
    userId: number;
    messageIdsUnRead: string[];
}
export declare function initMessageUsers(sequelize: Sequelize.Sequelize): void;
