import * as Sequelize from "sequelize";
import { StatusCode } from "../../models";
export declare class SimpleMessage extends Sequelize.Model {
    id: number;
    title: string;
    content: string;
    categoryId: number;
    memberIds: Array<number | string>;
    subscribers: Array<number | string>;
    labels: string[];
    attachmentIds: number[];
    isClosed: boolean;
    createdAt: Date;
    createdBy: number | string;
    updatedAt: Date;
    updatedBy: number | string;
    deletedAt: Date;
    status: StatusCode;
}
export declare function initSimpleMessage(sequelize: Sequelize.Sequelize): void;
