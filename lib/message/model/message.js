"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const models_1 = require("../../models");
class SimpleMessage extends Sequelize.Model {
}
exports.SimpleMessage = SimpleMessage;
function initSimpleMessage(sequelize) {
    SimpleMessage.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, title: {
            type: Sequelize.TEXT,
            allowNull: false
        }, content: {
            type: Sequelize.TEXT,
            allowNull: false
        }, categoryId: {
            type: Sequelize.BIGINT,
            allowNull: false
        }, memberIds: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        }, subscribers: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        }, labels: {
            type: Sequelize.ARRAY(Sequelize.TEXT),
            allowNull: false,
            defaultValue: []
        }, attachmentIds: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        }, isClosed: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        }, activities: {
            type: Sequelize.JSON,
            allowNull: false,
            defaultValue: "{}",
        }, status: {
            type: Sequelize.TEXT,
            allowNull: false,
        } }, models_1.defaultColumns), {
        tableName: "messages",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initSimpleMessage = initSimpleMessage;
