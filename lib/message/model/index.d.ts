import * as Sequelize from "sequelize";
export * from "./message";
export * from "./message-users";
export declare function initDBMessageToServeNotification(sequelize: Sequelize.Sequelize): void;
