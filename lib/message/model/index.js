"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
const message_1 = require("./message");
const message_users_1 = require("./message-users");
__export(require("./message"));
__export(require("./message-users"));
function initDBMessageToServeNotification(sequelize) {
    message_1.initSimpleMessage(sequelize);
    message_users_1.initMessageUsers(sequelize);
}
exports.initDBMessageToServeNotification = initDBMessageToServeNotification;
