import { ReferenceType } from "../../models";
import { IParamTemplateEmail, TypeContentTemplateEmail, TypeEmailTemplate } from "./types";
export * from "./types";
export declare const emailTemplates: {
    [TypeEmailTemplate.MessageNotification]: string;
    [TypeEmailTemplate.TopicNotification]: string;
    [TypeEmailTemplate.PlanItemNotification]: string;
    [TypeEmailTemplate.PlanNotification]: string;
    [TypeEmailTemplate.ProjectNotification]: string;
    [TypeEmailTemplate.ReminderNotification]: string;
    [TypeEmailTemplate.TaskNotification]: string;
};
export interface IParamEmail {
    template: {
        type: TypeEmailTemplate;
        params: IParamTemplateEmail;
    };
    titleAndContent: {
        type: TypeContentTemplateEmail;
        params: {
            title?: {
                [key: string]: string;
            };
            content?: {
                [key: string]: string;
            };
        };
        content?: string;
        title?: string;
    };
}
export declare function parseDataEmail(params: IParamEmail): {
    emailBody: string;
    emailTitle: string;
};
export interface IParamParseLink {
    id: string;
    planId?: string;
    projectId?: string;
}
export declare function parseLink(params: IParamParseLink, type: ReferenceType): string;
