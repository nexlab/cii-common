"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
const lodash = require("lodash");
const models_1 = require("../../models");
const templates_1 = require("./templates");
const types_1 = require("./types");
const vi_1 = require("./vi");
__export(require("./types"));
exports.emailTemplates = {
    [types_1.TypeEmailTemplate.MessageNotification]: templates_1.messageEmailTemplate,
    [types_1.TypeEmailTemplate.TopicNotification]: templates_1.topicEmailTemplate,
    [types_1.TypeEmailTemplate.PlanItemNotification]: templates_1.planItemEmailTemplate,
    [types_1.TypeEmailTemplate.PlanNotification]: templates_1.planTemplateEmail,
    [types_1.TypeEmailTemplate.ProjectNotification]: templates_1.projectEmailTemplate,
    [types_1.TypeEmailTemplate.ReminderNotification]: templates_1.reminderEmailTemplate,
    [types_1.TypeEmailTemplate.TaskNotification]: templates_1.taskEmailTemplate,
};
function parseDataEmail(params) {
    let emailBody = lodash.cloneDeep(exports.emailTemplates[params.template.type]);
    const titleAndContent = lodash.cloneDeep(vi_1.contentAndTitleForEmail[params.titleAndContent.type]);
    const paramTitle = params.titleAndContent.params ? params.titleAndContent.params.title : null;
    let emailTitle = titleAndContent.title;
    if (params.titleAndContent.title) {
        emailTitle = params.titleAndContent.title;
    }
    if (emailTitle && paramTitle && Object.keys(paramTitle).length > 0) {
        Object.keys(paramTitle).forEach((e) => {
            emailTitle = emailTitle.replace(`{{${e}}}`, paramTitle[e]);
        });
    }
    const paramContent = params.titleAndContent.params ? params.titleAndContent.params.content : null;
    let emailContent = titleAndContent.content;
    if (params.titleAndContent.content) {
        emailContent = params.titleAndContent.content;
    }
    if (emailContent && paramContent && Object.keys(paramContent).length > 0) {
        Object.keys(paramContent).forEach((e) => {
            emailContent = emailContent.replace(`{{${e}}}`, paramContent[e]);
        });
    }
    params.template.params.content = emailContent;
    if (titleAndContent.params) {
        params.template.params = Object.assign(Object.assign({}, params.template.params), titleAndContent.params);
    }
    const paramTemplate = params.template.params;
    if (paramTemplate && Object.keys(paramTemplate).length > 0) {
        Object.keys(paramTemplate).forEach((e) => {
            emailBody = emailBody.replace(`{{${e}}}`, paramTemplate[e]);
        });
    }
    return {
        emailBody,
        emailTitle
    };
}
exports.parseDataEmail = parseDataEmail;
const baseUrl = "https://system.ciiec-cc.vn";
const [topic, message, plan, planItem, project, task, taskComment] = [
    "/my-topic?type=openModal&id={{id}}",
    "/my-inbox?type=openModal&id={{id}}",
    "/plans/{{id}}/detail",
    "/plans/{{planId}}/detail/{{id}}",
    "/projects/{{id}}",
    "/projects/{{projectId}}/task/{{id}}",
    "/projects/{{projectId}}/task/{{id}}/task-comment/{{commentId}}"
];
function parseLink(params, type) {
    let path;
    switch (type) {
        case models_1.ReferenceType.Message:
            path = message;
            break;
        case models_1.ReferenceType.Topic:
            path = topic;
            break;
        case models_1.ReferenceType.Plan:
            path = plan;
            break;
        case models_1.ReferenceType.PlanItem:
            path = planItem;
            break;
        case models_1.ReferenceType.Project:
            path = project;
            break;
        case models_1.ReferenceType.Task:
            path = task;
            break;
        case models_1.ReferenceType.TaskComment:
            path = taskComment;
            break;
    }
    let url = baseUrl + path;
    for (const key in params) {
        url = url.replace(`{{${key}}}`, params[key]);
    }
    return url;
}
exports.parseLink = parseLink;
