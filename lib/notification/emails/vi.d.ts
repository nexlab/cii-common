import { IContentAndTitleForEmail } from "./types";
export declare const contentAndTitleForEmail: {
    [key: string]: IContentAndTitleForEmail;
};
