"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
function getLocaleString(input, lang = "en") {
    return typeof input === "string" ? input : input[lang];
}
exports.getLocaleString = getLocaleString;
/**
 * Replaces list dictionary key => value with pattern
 *
 * @param  {string} input       Input text
 * @param  {Object} params      Key value params
 *
 * @return {string}             Result output string
 */
function replaceParams(input, params) {
    return Object.keys(params).reduce((result, k) => {
        const val = params[k];
        return lodash_1.replace(result, `{{${k}}}`, val);
    }, input);
}
function replaceNotificationParams(form, params) {
    return Object.assign(Object.assign({}, form), { headings: typeof form.headings === "object" ? Object.keys(form.headings).reduce((o, k) => (Object.assign(Object.assign({}, o), { [k]: replaceParams(form.headings[k], params) })), {})
            : replaceParams(form.headings, params), contents: typeof form.contents === "object" ? Object.keys(form.contents).reduce((o, k) => (Object.assign(Object.assign({}, o), { [k]: replaceParams(form.contents[k], params) })), {})
            : replaceParams(form.contents, params) });
}
exports.replaceNotificationParams = replaceNotificationParams;
function validateNotificationForm(params, options, parentId) {
    const opts = Object.assign(Object.assign({}, options), { language: "en" });
    const parseText = (e) => e.replace(/<(.|\n)*?>/g, '');
    const headings = typeof params.title === "object" ? params.title
        : {
            en: parseText(params.title),
            vi: parseText(params.title),
        };
    const contents = typeof params.content === "object" ? params.content : {
        en: parseText(params.content),
        vi: parseText(params.content),
    };
    return {
        headings,
        contents,
        data: Object.assign({ title: getLocaleString(headings, opts.language), content: getLocaleString(contents, opts.language), parentId }, params)
    };
}
exports.validateNotificationForm = validateNotificationForm;
function validateEmailForm(params, _options) {
    // const opts = { ...options, language: "en" };
    const emailSubject = params.title;
    const emailBody = params.content;
    return {
        email_body: emailBody,
        email_subject: emailSubject,
        isEmail: true
    };
}
exports.validateEmailForm = validateEmailForm;
