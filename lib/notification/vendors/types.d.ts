import { ReferenceType } from "../../models";
export interface IPushNotificationParams {
    contents: {
        [key: string]: string;
    };
    headings?: {
        [key: string]: string;
    };
    subtitle?: {
        [key: string]: string;
    };
    data?: object;
    url?: string;
    templateId?: string;
}
export interface INotificationInput {
    title: string;
    content: string;
    senderId: string;
    groupId?: string;
    metadata?: object;
    parentId?: string;
    type?: ReferenceType;
    referenceId?: string;
    referenceUrl?: string;
    receiverIds: Array<number | string>;
}
export interface IBroadcastNotificationPayload extends IPushNotificationParams {
    groups?: string[];
}
export interface INotificationPusher {
    sendToUser(userId: string, payload: IPushNotificationParams): Promise<any>;
    sendToUsers(userIds: string[], payload: IPushNotificationParams): Promise<any>;
    sendToEmailUsers(emailUsers: string[], payload: IPushNotificationParams): Promise<any>;
    broadcast(payload: IBroadcastNotificationPayload): Promise<any>;
}
