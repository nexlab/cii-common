import { IValidateEmailFormInput } from "../business";
import { INotificationInput, IPushNotificationParams } from "./types";
export interface ISerializationOptions {
    language: string;
}
export interface IReplaceParams {
    [key: string]: string;
}
export declare type ILocaleData = string | {
    [key: string]: string;
};
export declare function getLocaleString(input: ILocaleData, lang?: string): string;
export declare function replaceNotificationParams<T = IPushNotificationParams>(form: IPushNotificationParams, params: IReplaceParams): T;
export declare function validateNotificationForm(params: INotificationInput, options?: ISerializationOptions, parentId?: any): IPushNotificationParams;
export declare function validateEmailForm(params: IValidateEmailFormInput, _options?: ISerializationOptions): IPushNotificationParams;
