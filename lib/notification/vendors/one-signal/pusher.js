"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function filterByUserId(userId) {
    return { field: "tag", key: "userId", relation: "=", value: userId };
}
// function filterByEmail(email: string): object {
//   return { field: "email", value: email };
// }
function OneSignalPusher(client) {
    function sendToUser(userId, payload) {
        return client.createNotification(Object.assign(Object.assign({}, payload), { filters: [
                filterByUserId(userId),
            ] }));
    }
    function broadcast(payload) {
        const groups = payload.groups ? payload.groups : ["All"];
        delete payload.groups;
        return client.createNotification(Object.assign(Object.assign({}, payload), { included_segments: groups }));
    }
    function sendToUsers(userIds, payload) {
        const filters = [];
        const limit = 100;
        function filterFunc(start) {
            const result = [filterByUserId(userIds[start])];
            const end = Math.min(userIds.length, start + limit);
            for (let i = start + 1; i < end; i++) {
                result.push({ operator: "OR" });
                result.push(filterByUserId(userIds[i]));
            }
            return result;
        }
        const max = userIds.length % limit > 0
            ? Math.floor(userIds.length / limit) + 1 : userIds.length / limit;
        for (let j = 0; j < max; j++) {
            filters.push(filterFunc(j * limit));
        }
        return Promise.all(filters.map((f) => client.createNotification(Object.assign(Object.assign({}, payload), { filters: f }))));
    }
    function sendToEmailUsers(emailUsers, payload) {
        // const filters = [];
        // const limit = 100;
        // function filterFunc(start: number) {
        //   const result = [filterByEmail(emailUsers[start])];
        //   const end = Math.min(emailUsers.length, start + limit);
        //   for (let i = start + 1; i < end; i++) {
        //     result.push({ operator: "OR" });
        //     if (parseInt(emailUsers[i], 0)) {
        //       result.push(filterByUserId(emailUsers[i]));
        //     } else {
        //       include_email_tokens.push(emailUsers[i]);
        //     }
        //   }
        //   return result;
        // }
        // const max = emailUsers.length % limit > 0
        //   ? Math.floor(emailUsers.length / limit) + 1 : emailUsers.length / limit;
        // for (let j = 0; j < max; j++) {
        //   filters.push(filterFunc(j * limit));
        // }
        // return Promise.all(
        //   filters.map((f) => client.createNotification({
        //     include_email_tokens,
        //     ...payload,
        //     filters: f,
        //   }))
        // );
        const userIds = emailUsers.filter((e) => parseInt(e, 0));
        let filters = [];
        if (userIds && userIds.length > 0) {
            filters = [filterByUserId(userIds[0])];
            userIds.forEach((e) => {
                filters.push({ operator: "OR" });
                filters.push(filterByUserId(e));
            });
        }
        return client.createNotification(Object.assign(Object.assign({ include_email_tokens: emailUsers.filter((e) => !(parseInt(e, 0))) }, payload), { filters }));
    }
    return {
        sendToUser,
        sendToUsers,
        sendToEmailUsers,
        broadcast,
    };
}
exports.OneSignalPusher = OneSignalPusher;
