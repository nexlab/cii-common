import { AxiosRequestConfig } from "axios";
import { ICreateNotificationRequest, ICreateNotificationResults } from "../types";
export declare function createNotification(data: ICreateNotificationRequest, options: AxiosRequestConfig): Promise<ICreateNotificationResults>;
