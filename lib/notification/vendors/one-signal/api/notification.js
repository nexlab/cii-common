"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
function createNotification(data, options) {
    return axios_1.default.post("/api/v1/notifications", data, options)
        .then((res) => res.data);
}
exports.createNotification = createNotification;
