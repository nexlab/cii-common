"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const api_1 = require("./api");
function OneSignalClient(options) {
    const headers = {
        "Content-Type": "application/json; charset=utf-8",
        Authorization: `Basic ${options.api_key}`
    };
    const requestOptions = {
        headers,
        baseURL: "https://onesignal.com",
        port: 443,
    };
    return {
        createNotification: (data) => api_1.createNotification(Object.assign(Object.assign({}, data), { app_id: options.app_id }), requestOptions),
    };
}
exports.OneSignalClient = OneSignalClient;
