import { ICreateNotificationRequest, ICreateNotificationResults } from "./types";
export interface IOneSignalClient {
    createNotification: (data: ICreateNotificationRequest) => Promise<ICreateNotificationResults>;
}
export interface IOneSignalClientOptions {
    app_id: string;
    api_key: string;
}
export declare function OneSignalClient(options: IOneSignalClientOptions): IOneSignalClient;
