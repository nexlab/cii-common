import { INotificationPusher } from "../types";
import { IOneSignalClient } from "./client";
export declare function OneSignalPusher(client: IOneSignalClient): INotificationPusher;
