"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const sequelize_1 = require("sequelize");
const models_1 = require("../../models");
const user_notification_setting_1 = require("../../user-notification-setting");
const models_2 = require("../models");
const vendors_1 = require("../vendors");
const one_signal_1 = require("../vendors/one-signal");
const notification_user_1 = require("./notification-user");
const emails_1 = require("../emails");
let _pusher;
function notificationPusher(appId, apiKey) {
    if (_pusher) {
        return _pusher;
    }
    appId = appId || process.env.ONE_SIGNAL_APP_ID;
    apiKey = apiKey || process.env.ONE_SIGNAL_API_KEY;
    _pusher = one_signal_1.OneSignalPusher(one_signal_1.OneSignalClient({
        app_id: appId,
        api_key: apiKey,
    }));
    return _pusher;
}
exports.notificationPusher = notificationPusher;
function createNotification(input, updateOptions) {
    return __awaiter(this, void 0, void 0, function* () {
        const opts = Object.assign({}, updateOptions);
        const model = yield models_2.Notification.create(Object.assign({ senderId: opts.actorId, createdBy: opts.actorId || input.senderId, updatedBy: opts.actorId || input.senderId }, lodash_1.omit(input, ["receiverIds"])));
        const receivers = yield notification_user_1.createManyNotificationUsers(model.id, input.receiverIds);
        return Object.assign(Object.assign({}, model), { receivers });
    });
}
exports.createNotification = createNotification;
function createAndPushNotification(pusher, input, updateOptions) {
    return __awaiter(this, void 0, void 0, function* () {
        pusher = pusher || notificationPusher();
        // only push app
        if (updateOptions.filterUserSettingNotification === false) {
            const modelOnlyPushApp = yield createNotification(input, updateOptions);
            const payloadMore = vendors_1.validateNotificationForm(input, null, input.parentId);
            yield pusher.sendToUsers(input.receiverIds, payloadMore);
            return modelOnlyPushApp;
        }
        if (![models_1.ReferenceType.Message, models_1.ReferenceType.Topic].includes(input.type)) {
            return pushFollowUserSettingNotification(pusher, input, updateOptions);
        }
        const model = yield createNotification(input, updateOptions);
        const payload = vendors_1.validateNotificationForm(input, null, input.parentId);
        yield pusher.sendToUsers(input.receiverIds, payload);
        return model;
    });
}
exports.createAndPushNotification = createAndPushNotification;
function filterUserForPush(input) {
    return __awaiter(this, void 0, void 0, function* () {
        const receiverIdsForEmail = [];
        const receiverIdsForAppPush = [];
        if (input.receiverIds.length === 0) {
            return { receiverIdsForAppPush, receiverIdsForEmail };
        }
        const userNotificationSetting = yield user_notification_setting_1.UserNotificationSetting.findAll({
            where: {
                type: input.type,
                userId: {
                    [sequelize_1.Op.in]: input.receiverIds
                },
                referenceId: input.referenceId
            }
        });
        return parseListUser(input, userNotificationSetting);
    });
}
exports.filterUserForPush = filterUserForPush;
function parseListUser(input, userNotificationSetting) {
    return __awaiter(this, void 0, void 0, function* () {
        const arr = input.receiverIds;
        const users = yield models_1.User.findAll({
            where: { id: { [sequelize_1.Op.in]: arr }, status: models_1.StatusCode.Active }
        });
        const receiverIdsForEmail = [];
        const receiverIdsForAppPush = [];
        const receiverIds = arr.map((e) => {
            let current = null;
            if (userNotificationSetting) {
                const currentUserNotificationSetting = userNotificationSetting.find((a) => a.userId.toString() === e.toString());
                current = currentUserNotificationSetting ? currentUserNotificationSetting.toJSON() : null;
            }
            const currentUser = users.find((b) => b.id.toString() === e.toString());
            return {
                setting: current ? current.setting : user_notification_setting_1.defaultSettingForEntire(),
                user: currentUser && currentUser.toJSON ? currentUser.toJSON() : currentUser
            };
        }).filter((e) => e.user);
        receiverIds.forEach((e) => {
            if (e.setting.email) {
                if (e.user.notificationEmail) {
                    receiverIdsForEmail.push(e.user.notificationEmail);
                }
                else {
                    receiverIdsForEmail.push(e.user.id);
                }
            }
            if (e.setting.appPush) {
                receiverIdsForAppPush.push(e.user.id);
            }
        });
        return {
            receiverIdsForEmail,
            receiverIdsForAppPush
        };
    });
}
exports.parseListUser = parseListUser;
function pushFollowUserSettingNotification(pusher, input, updateOptions) {
    return __awaiter(this, void 0, void 0, function* () {
        const userIds = yield filterUserForPush(input);
        if (userIds.receiverIdsForAppPush.length > 0) {
            input.receiverIds = userIds.receiverIdsForAppPush;
            yield createNotification(input, updateOptions);
            const payloadForNotification = vendors_1.validateNotificationForm(input);
            yield pusher.sendToUsers(input.receiverIds, payloadForNotification);
        }
        return;
    });
}
exports.pushFollowUserSettingNotification = pushFollowUserSettingNotification;
function pushEmailNotification(pusher, input, _updateOptions) {
    return __awaiter(this, void 0, void 0, function* () {
        pusher = pusher || notificationPusher();
        const userIds = input.userNotificationSetting === false
            ? yield parseListUser(input)
            : yield filterUserForPush(input);
        if (userIds.receiverIdsForEmail.length > 0) {
            const dataEmail = emails_1.parseDataEmail(input.params);
            const dataEmailForm = {
                receiverIds: userIds.receiverIdsForEmail,
                title: dataEmail.emailTitle,
                content: dataEmail.emailBody
            };
            const payload = vendors_1.validateEmailForm(dataEmailForm);
            yield pusher.sendToEmailUsers(dataEmailForm.receiverIds, payload);
        }
        return;
    });
}
exports.pushEmailNotification = pushEmailNotification;
