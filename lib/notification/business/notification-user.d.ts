import { NotificationUser } from "../models";
export declare function createManyNotificationUsers(notificationId: number | number, receiverIds: Array<string | number>): Promise<NotificationUser[]>;
export declare function markNotificationUserAsRead(userId: number | string): Promise<any>;
export declare function countNewNotificationUser(userId: number | string): Promise<number>;
