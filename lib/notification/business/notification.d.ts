import { IUpdateOptions, ReferenceType } from "../../models";
import { UserNotificationSetting } from "../../user-notification-setting";
import { Notification } from "../models";
import { INotificationInput, INotificationPusher } from "../vendors";
import { IParamEmail } from "../emails";
export declare function notificationPusher(appId?: string, apiKey?: string): INotificationPusher;
export declare function createNotification(input: INotificationInput, updateOptions?: IUpdateOptions): Promise<Notification>;
export interface IOptionCreateAndPushNotification extends IUpdateOptions {
    filterUserSettingNotification?: boolean;
}
export declare function createAndPushNotification(pusher: INotificationPusher, input: INotificationInput, updateOptions?: IOptionCreateAndPushNotification): Promise<Notification>;
export interface IOptionFilterForPush {
    userNotificationSetting?: boolean;
}
export declare function filterUserForPush(input: INotificationInput | INotificationEmailInput): Promise<any>;
export declare function parseListUser(input: INotificationInput | INotificationEmailInput, userNotificationSetting?: UserNotificationSetting[]): Promise<{
    receiverIdsForEmail: any[];
    receiverIdsForAppPush: any[];
}>;
export declare function pushFollowUserSettingNotification(pusher: INotificationPusher, input: INotificationInput, updateOptions?: IUpdateOptions): Promise<any>;
export interface INotificationEmailInput {
    receiverIds: Array<number | string>;
    params: IParamEmail;
    type: ReferenceType;
    referenceId: string;
    userNotificationSetting?: boolean;
}
export interface IValidateEmailFormInput {
    receiverIds: Array<number | string>;
    title: string;
    content: string;
}
export declare function pushEmailNotification(pusher: INotificationPusher, input: INotificationEmailInput, _updateOptions?: IUpdateOptions): Promise<any>;
