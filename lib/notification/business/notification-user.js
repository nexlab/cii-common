"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const models_1 = require("../models");
function createManyNotificationUsers(notificationId, receiverIds) {
    return receiverIds.map((userId) => models_1.NotificationUser.create({
        notificationId,
        userId,
        isRead: false,
    }));
}
exports.createManyNotificationUsers = createManyNotificationUsers;
function markNotificationUserAsRead(userId) {
    return models_1.NotificationUser.update({ isRead: true }, {
        where: { userId }
    });
}
exports.markNotificationUserAsRead = markNotificationUserAsRead;
function countNewNotificationUser(userId) {
    return models_1.NotificationUser.count({
        where: {
            userId,
            isRead: false
        }
    });
}
exports.countNewNotificationUser = countNewNotificationUser;
