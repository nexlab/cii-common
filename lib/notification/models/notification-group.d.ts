import * as Sequelize from "sequelize";
export declare class NotificationGroup extends Sequelize.Model {
    id: number;
    title: string;
    subscriptions: string[];
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    deletedAt: Date;
}
export declare function initNotificationGroup(sequelize: Sequelize.Sequelize): void;
