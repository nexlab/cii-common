import * as Sequelize from "sequelize";
import { Notification } from "./notification";
export declare class NotificationUser extends Sequelize.Model {
    id: number;
    notificationId: string;
    userId: string;
    isRead: string;
    notification?: Notification;
    metadata?: any;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
}
export declare function initNotificationUser(sequelize: Sequelize.Sequelize): void;
