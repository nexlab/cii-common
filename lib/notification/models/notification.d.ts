import * as Sequelize from "sequelize";
import { ReferenceType } from "../../models";
import { NotificationUser } from "./notification-user";
export declare class Notification extends Sequelize.Model {
    id: number;
    title: string;
    content: string;
    senderId: string;
    groupId: string;
    metadata: object;
    type: ReferenceType;
    referenceId: string;
    referenceUrl: string;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    deletedAt: Date;
    receivers?: NotificationUser[];
}
export declare function initNotification(sequelize: Sequelize.Sequelize): void;
