import { Sequelize } from "sequelize";
import { Notification } from "./notification";
import { NotificationGroup } from "./notification-group";
import { NotificationUser } from "./notification-user";
export { Notification, NotificationGroup, NotificationUser, };
export declare function initDBNotification(sequelize: Sequelize): void;
