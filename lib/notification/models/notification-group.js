"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const models_1 = require("../../models");
class NotificationGroup extends Sequelize.Model {
}
exports.NotificationGroup = NotificationGroup;
function initNotificationGroup(sequelize) {
    NotificationGroup.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, title: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: ""
        }, subscriptions: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false,
            defaultValue: []
        } }, models_1.defaultColumns), {
        tableName: "notification_groups",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initNotificationGroup = initNotificationGroup;
