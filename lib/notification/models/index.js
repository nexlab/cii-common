"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const notification_1 = require("./notification");
exports.Notification = notification_1.Notification;
const notification_group_1 = require("./notification-group");
exports.NotificationGroup = notification_group_1.NotificationGroup;
const notification_user_1 = require("./notification-user");
exports.NotificationUser = notification_user_1.NotificationUser;
function initDBNotification(sequelize) {
    notification_1.initNotification(sequelize);
    notification_group_1.initNotificationGroup(sequelize);
    notification_user_1.initNotificationUser(sequelize);
    notification_1.Notification.hasMany(notification_user_1.NotificationUser, {
        as: "receivers",
        foreignKey: "notificationId",
    });
}
exports.initDBNotification = initDBNotification;
