"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const models_1 = require("../../models");
class Notification extends Sequelize.Model {
}
exports.Notification = Notification;
function initNotification(sequelize) {
    Notification.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, title: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: ""
        }, content: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: ""
        }, senderId: {
            type: Sequelize.BIGINT,
            allowNull: true,
        }, groupId: {
            type: Sequelize.BIGINT,
            allowNull: true,
        }, type: {
            type: Sequelize.TEXT,
            allowNull: false,
        }, referenceId: {
            type: Sequelize.BIGINT,
            allowNull: true,
        }, metadata: {
            type: Sequelize.JSON,
            allowNull: false,
            defaultValue: {}
        }, referenceUrl: {
            type: Sequelize.TEXT,
            allowNull: true,
        } }, models_1.defaultColumns), {
        tableName: "notifications",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initNotification = initNotification;
