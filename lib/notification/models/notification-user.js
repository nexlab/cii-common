"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const notification_1 = require("./notification");
class NotificationUser extends Sequelize.Model {
}
exports.NotificationUser = NotificationUser;
function initNotificationUser(sequelize) {
    NotificationUser.init({
        id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        },
        notificationId: {
            type: Sequelize.BIGINT,
            allowNull: false,
        },
        userId: {
            type: Sequelize.BIGINT,
            allowNull: false,
        },
        isRead: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        createdAt: {
            type: Sequelize.DATE,
            allowNull: true,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            allowNull: true,
            defaultValue: Sequelize.NOW,
        },
        deletedAt: {
            type: Sequelize.DATE,
            allowNull: true,
        },
    }, {
        tableName: "notification_users",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
    NotificationUser.belongsTo(notification_1.Notification, {
        as: "notification",
        foreignKey: "notificationId"
    });
}
exports.initNotificationUser = initNotificationUser;
