import * as Sequelize from "sequelize";
import { ReferenceType } from "../../models";
export declare enum FileType {
    Photo = "photo",
    Video = "video",
    File = "file"
}
export declare class Attachment extends Sequelize.Model {
    id: number;
    fileName: string;
    fileType: FileType;
    fileSize: string;
    fileUrl: string;
    extension: string;
    departmentId: number;
    referenceId: string;
    referenceType: ReferenceType;
    thumbnailUrl: string;
    directoryId: string;
    description: string;
    type: string;
    password: string;
    status: string;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    deletedAt: Date;
    hardDeletedAt: Date;
}
export declare function initAttachment(sequelize: Sequelize.Sequelize): void;
