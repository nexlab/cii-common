import { Sequelize } from "sequelize";
import { Attachment, FileType } from "./attachment";
import { FileDirectory } from "./file-directory";
export { Attachment, FileDirectory, FileType };
export declare function initDBAttachment(sequelize: Sequelize): void;
