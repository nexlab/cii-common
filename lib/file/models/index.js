"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const attachment_1 = require("./attachment");
exports.Attachment = attachment_1.Attachment;
exports.FileType = attachment_1.FileType;
const file_directory_1 = require("./file-directory");
exports.FileDirectory = file_directory_1.FileDirectory;
function initDBAttachment(sequelize) {
    attachment_1.initAttachment(sequelize);
    file_directory_1.initFileDirectory(sequelize);
}
exports.initDBAttachment = initDBAttachment;
