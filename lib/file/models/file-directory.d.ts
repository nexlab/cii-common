import * as Sequelize from "sequelize";
export declare class FileDirectory extends Sequelize.Model {
    id: number;
    name: string;
    url: string;
    refId: number;
    refType: string;
    directoryId: number;
    departmentId: number;
    type: string;
    status: string;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
    deletedAt: Date;
    hardDeletedAt: Date;
}
export declare function initFileDirectory(sequelize: Sequelize.Sequelize): void;
