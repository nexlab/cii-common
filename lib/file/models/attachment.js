"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const models_1 = require("../../models");
var FileType;
(function (FileType) {
    FileType["Photo"] = "photo";
    FileType["Video"] = "video";
    FileType["File"] = "file";
})(FileType = exports.FileType || (exports.FileType = {}));
class Attachment extends Sequelize.Model {
}
exports.Attachment = Attachment;
function initAttachment(sequelize) {
    Attachment.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, fileName: {
            type: Sequelize.TEXT,
            allowNull: false,
        }, fileType: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: "",
        }, fileSize: {
            type: Sequelize.BIGINT,
            allowNull: false,
            defaultValue: 0,
        }, fileUrl: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: "",
        }, extension: {
            type: Sequelize.TEXT,
            allowNull: true,
            defaultValue: "",
        }, thumbnailUrl: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: "",
        }, referenceId: {
            type: Sequelize.TEXT,
            allowNull: true,
        }, referenceType: {
            type: Sequelize.TEXT,
            allowNull: true,
        }, directoryId: {
            type: Sequelize.BIGINT,
            allowNull: false,
            defaultValue: 0,
        }, departmentId: {
            type: Sequelize.BIGINT,
            allowNull: true,
        }, description: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: "",
        }, type: {
            type: Sequelize.TEXT,
            allowNull: true,
        }, status: {
            type: Sequelize.TEXT,
            allowNull: true,
            defaultValue: "active",
        }, password: {
            type: Sequelize.STRING,
            allowNull: true,
        }, hardDeletedAt: {
            type: Sequelize.DATE,
            allowNull: true,
        } }, models_1.defaultColumns), {
        tableName: "files",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initAttachment = initAttachment;
