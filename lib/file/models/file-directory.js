"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const models_1 = require("../../models");
class FileDirectory extends Sequelize.Model {
}
exports.FileDirectory = FileDirectory;
function initFileDirectory(sequelize) {
    FileDirectory.init(Object.assign({ id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        }, name: {
            type: Sequelize.TEXT,
            allowNull: false
        }, url: {
            type: Sequelize.TEXT,
            allowNull: false,
            defaultValue: ""
        }, refType: {
            type: Sequelize.TEXT,
            allowNull: true
        }, refId: {
            type: Sequelize.BIGINT,
            allowNull: true
        }, directoryId: {
            type: Sequelize.BIGINT,
            allowNull: true
        }, departmentId: {
            type: Sequelize.BIGINT,
            allowNull: true,
        }, type: {
            type: Sequelize.TEXT,
            allowNull: true,
        }, status: {
            type: Sequelize.TEXT,
            allowNull: true,
            defaultValue: "active"
        }, hardDeletedAt: {
            type: Sequelize.DATE,
            allowNull: true,
        } }, models_1.defaultColumns), {
        tableName: "file_directories",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initFileDirectory = initFileDirectory;
