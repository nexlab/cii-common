import { IUpdateOptions, ReferenceType } from "../../models";
import { Attachment } from "../models";
export interface IAttachmentInput {
    fileName: string;
    fileType: string;
    fileSize: string;
    fileUrl: string;
    extension: string;
    thumbnailUrl: string;
    directoryId: number;
    departmentId: number;
    description: string;
    type: string;
    referenceId: string;
    referenceType: ReferenceType;
}
export interface IAttachmentInputUpdate extends IAttachmentInput {
    id?: number;
}
export declare function createAttachment(input: IAttachmentInput, updateOptions: IUpdateOptions): Promise<Attachment>;
export declare function deleteAttachment(input: IAttachmentInputUpdate): Promise<any>;
export declare function findAllReferenceAttachments(referenceIds: Array<number | string>, referenceType: string): Promise<Attachment[]>;
