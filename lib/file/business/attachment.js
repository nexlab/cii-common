"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const models_1 = require("../../models");
const models_2 = require("../models");
function createAttachment(input, updateOptions) {
    input.directoryId = input.directoryId || 0;
    return models_2.Attachment.create(Object.assign(Object.assign({}, input), { createdBy: updateOptions.actorId, updatedBy: updateOptions.actorId, status: models_1.StatusCode.Active }));
}
exports.createAttachment = createAttachment;
function deleteAttachment(input) {
    return models_2.Attachment.update({
        status: models_1.StatusCode.Deleted
    }, {
        where: { id: input.id }
    });
}
exports.deleteAttachment = deleteAttachment;
function findAllReferenceAttachments(referenceIds, referenceType) {
    referenceIds = referenceIds.map((e) => e.toString());
    return models_2.Attachment.findAll({
        where: {
            referenceType,
            referenceId: { [sequelize_1.Op.in]: referenceIds },
            status: models_1.StatusCode.Active
        },
        order: [["id", "ASC"]]
    });
}
exports.findAllReferenceAttachments = findAllReferenceAttachments;
