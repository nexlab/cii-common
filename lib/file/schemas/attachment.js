"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Joi = require("joi");
const models_1 = require("../models");
const common = {
    id: Joi.string().optional(),
    fileName: Joi.string().required(),
    fileType: Joi.string().valid([models_1.FileType.File, models_1.FileType.Photo, models_1.FileType.Video]).required(),
    fileSize: Joi.number().integer().default(0),
    fileUrl: Joi.string().required().trim(),
    thumbnailUrl: Joi.string().required().trim().allow(""),
    directoryId: Joi.number().allow(null),
    description: Joi.string().allow(""),
    extension: Joi.string(),
    createdAt: Joi.date().optional()
};
exports.AttachmentInputSchema = Joi.object(common);
