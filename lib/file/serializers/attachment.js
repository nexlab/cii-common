"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function serializeAttachment(input) {
    return {
        fileType: input.fileType,
        fileName: input.fileName,
        fileUrl: input.fileUrl,
        fileSize: input.fileSize,
        extension: input.extension,
        thumbnailUrl: input.thumbnailUrl,
        description: input.description,
        directoryId: input.directoryId
    };
}
exports.serializeAttachment = serializeAttachment;
