export declare enum ErrorKey {
    RecordNotFound = "record_not_found",
    UpdateProjectTemplate = "update_project_template",
    UpdateTaskRating = "update_task_rating",
    ProjectTaskPrivate = "project_task_private"
}
