"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("./types");
exports.default = {
    [types_1.ErrorKey.RecordNotFound]: "Không tìm thấy dữ liệu",
    error_unknown: "Lỗi không xác định",
    permission: "Không có quyền truy cập",
    [types_1.ErrorKey.UpdateProjectTemplate]: "Không thể bỏ trạng thái hoặc xoá khi mẫu dự án đã có dự án",
    [types_1.ErrorKey.UpdateTaskRating]: "Không thể bỏ trạng thái hoặc xoá khi đánh giá đã được gắn nhiệm vụ",
    [types_1.ErrorKey.ProjectTaskPrivate]: "Công việc đã được bảo mật"
};
