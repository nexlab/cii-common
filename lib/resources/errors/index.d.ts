export * from "./types";
export interface IOptionMessageError {
    params?: object;
}
export declare function getMessageError(key: string, opts?: IOptionMessageError): string;
export declare function parseMessageError(message: string, params: object): string;
