"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
const vi_1 = require("./vi");
__export(require("./types"));
function getMessageError(key, opts = {}) {
    const message = vi_1.default[key];
    if (!message) {
        return getMessageError("error_unknown");
    }
    return opts.params ? parseMessageError(message, opts.params) : message;
}
exports.getMessageError = getMessageError;
function parseMessageError(message, params) {
    const delimiterLeft = "{{";
    const delimiterRight = "}}";
    const keyParams = Object.keys(params);
    if (keyParams.length === 0) {
        return message;
    }
    keyParams.forEach((e) => {
        message = message.replace(delimiterLeft + e + delimiterRight, params[e]);
    });
    return message;
}
exports.parseMessageError = parseMessageError;
