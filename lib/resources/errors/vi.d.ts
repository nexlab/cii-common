import { ErrorKey } from "./types";
declare const _default: {
    [ErrorKey.RecordNotFound]: string;
    error_unknown: string;
    permission: string;
    [ErrorKey.UpdateProjectTemplate]: string;
    [ErrorKey.UpdateTaskRating]: string;
    [ErrorKey.ProjectTaskPrivate]: string;
};
export default _default;
