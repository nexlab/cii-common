"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ErrorKey;
(function (ErrorKey) {
    ErrorKey["RecordNotFound"] = "record_not_found";
    ErrorKey["UpdateProjectTemplate"] = "update_project_template";
    ErrorKey["UpdateTaskRating"] = "update_task_rating";
    ErrorKey["ProjectTaskPrivate"] = "project_task_private";
})(ErrorKey = exports.ErrorKey || (exports.ErrorKey = {}));
