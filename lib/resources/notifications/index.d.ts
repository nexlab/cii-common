export * from "./types";
export interface IParamsNotification {
    content?: object;
    title?: object;
}
export interface IOptionNotification {
    params?: IParamsNotification;
}
export interface INotificationContent {
    title: string;
    content: string;
}
export declare function getContentNotification(key: string, opts?: IOptionNotification): INotificationContent;
export declare function parseContentNotification(notification: INotificationContent, params: IParamsNotification): INotificationContent;
