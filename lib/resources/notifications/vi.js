"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("./types");
exports.default = {
    [types_1.NotificationKeyType.MessageCreate]: {
        title: "{{userName}} vừa tạo tin nhắn mới",
        content: "{{messageTitle}}"
    },
    [types_1.NotificationKeyType.MessageUpdate]: {
        title: "{{messageTitle}}",
        content: "{{userName}} vừa cập nhật nội dung tin nhắn"
    },
    [types_1.NotificationKeyType.MessageUpdateMember]: {
        title: "{{messageTitle}}",
        content: "{{userName}} vừa cập nhật danh sách thành viên trong tin nhắn"
    },
    [types_1.NotificationKeyType.MessageAddLike]: {
        title: "Thích tin nhắn",
        content: "{{userName}} đã thích tin nhắn {{messageTitle}}"
    },
    [types_1.NotificationKeyType.MessageAddComment]: {
        title: "{{messageTitle}}",
        content: "{{userName}} vừa thêm một bình luận"
    },
    [types_1.NotificationKeyType.MessageCommentAddMention]: {
        title: "{{messageTitle}}",
        content: "{{userName}} vừa đề cập đến bạn trong một bình luận"
    },
    [types_1.NotificationKeyType.MessageAddAttachment]: {
        title: "{{messageTitle}}",
        content: "{{userName}} vừa thay đổi file đính kèm"
    },
    [types_1.NotificationKeyType.MessageClose]: {
        title: "{{messageTitle}}",
        content: "{{userName}} đã đóng tin nhắn"
    },
    [types_1.NotificationKeyType.TopicCreate]: {
        title: "{{userName}} vừa tạo tin nhắn mới",
        content: "{{topicTitle}}"
    },
    [types_1.NotificationKeyType.TopicUpdate]: {
        title: "{{topicTitle}}",
        content: "{{userName}} vừa cập nhật nội dung tin nhắn"
    },
    [types_1.NotificationKeyType.TopicUpdateMember]: {
        title: "{{topicTitle}}",
        content: "{{userName}} vừa cập nhật danh sách thành viên trong tin nhắn"
    },
    [types_1.NotificationKeyType.TopicAddLike]: {
        title: "Thích tin nhắn",
        content: "{{userName}} đã thích tin nhắn {{topicTitle}}"
    },
    [types_1.NotificationKeyType.TopicAddComment]: {
        title: "{{topicTitle}}",
        content: "{{userName}} vừa thêm một bình luận"
    },
    [types_1.NotificationKeyType.TopicCommentAddMention]: {
        title: "{{topicTitle}}",
        content: "{{userName}} vừa đề cập đến bạn trong một bình luận"
    },
    [types_1.NotificationKeyType.TopicAddAttachment]: {
        title: "{{topicTitle}}",
        content: "{{userName}} vừa thay đổi file đính kèm"
    },
    [types_1.NotificationKeyType.TopicClose]: {
        title: "{{topicTitle}}",
        content: "{{userName}} đã đóng tin nhắn"
    },
    [types_1.NotificationKeyType.PlanUpdate]: {
        title: "Cập nhật kế hoạch",
        content: "{{userName}} đã cập nhật kế hoạch '{{planName}}'"
    },
    [types_1.NotificationKeyType.PlanDelete]: {
        title: "Xoá kế hoạch",
        content: "{{userName}} đã xoá kế hoạch '{{planName}}'"
    },
    [types_1.NotificationKeyType.PlanItemUpdate]: {
        title: "Cập nhật hạng mục kế hoạch",
        content: "{{userName}} đã cập nhật hạng mục kế hoạch '{{planItemName}}'"
    },
    [types_1.NotificationKeyType.PlanItemDelete]: {
        title: "Xoá hạng mục kế hoạch",
        content: "{{userName}} đã xoá hạng mục kế hoạch '{{planItemName}}'"
    },
    [types_1.NotificationKeyType.RemindPlan]: {
        title: "Nhắc nhở",
        content: "Hạng mục kế hoạch '{{planItemTitle}}' sắp hết hạn"
    },
    [types_1.NotificationKeyType.RemindTask]: {
        title: "Nhắc nhở",
        content: "Nhiệm vụ'{{taskTitle}}' sắp hết hạn"
    },
    [types_1.NotificationKeyType.ProjectCreate]: {
        title: "{{userName}} vừa tạo bảng công việc",
        content: "{{projectName}}"
    },
    [types_1.NotificationKeyType.ProjectUpdate]: {
        title: "{{userName}} vừa cập nhật bảng công việc",
        content: "{{projectName}}"
    },
    [types_1.NotificationKeyType.ProjectClose]: {
        title: "{{userName}} vừa đóng bảng công việc",
        content: "{{projectName}}"
    },
    [types_1.NotificationKeyType.ActivityTaskCreate]: {
        title: "{{userName}} vừa tạo một công việc mới'",
        content: "{{taskTitle}}"
    },
    [types_1.NotificationKeyType.ActivityTaskEditTitle]: {
        title: "{{userName}} vừa thay đổi tiêu đề công việc'",
        content: "{{taskTitle}}"
    },
    [types_1.NotificationKeyType.ActivityTaskEditContent]: {
        title: "{{userName}} vừa thay đổi nội dung công việc'",
        content: "{{taskTitle}}"
    },
    [types_1.NotificationKeyType.ActivityTaskEditStatus]: {
        title: "{{userName}} vừa thay đổi trạng thái công việc",
        content: "{{taskTitle}}"
    },
    [types_1.NotificationKeyType.ActivityTaskEditDeadline]: {
        title: "{{userName}} vừa thay đổi thời hạn của công việc",
        content: "{{taskTitle}}"
    },
    [types_1.NotificationKeyType.ActivityTaskEditTaskGroup]: {
        title: "{{userName}} vừa thay đổi nhóm công việc",
        content: "{{taskTitle}}"
    },
    [types_1.NotificationKeyType.ActivityTaskEditEstimation]: {
        title: "{{userName}} vừa thay đổi ước lượng công việc",
        content: "{{taskTitle}}"
    },
    [types_1.NotificationKeyType.ActivityTaskUpdateAttachment]: {
        title: "{{userName}} vừa cập nhật file đính kèm",
        content: "{{taskTitle}}"
    },
    [types_1.NotificationKeyType.ActivityTaskChangeAssignee]: {
        title: "{{userName}} vừa thay đổi người được phân công",
        content: "{{taskTitle}}"
    },
    [types_1.NotificationKeyType.ActivityTaskAddComment]: {
        title: "{{userName}} vừa thêm một bình luận",
        content: "{{taskTitle}}"
    },
    [types_1.NotificationKeyType.ActivityTaskChangeReporter]: {
        title: "{{userName}} vừa thay đổi người phụ trách công việc",
        content: "{{taskTitle}}"
    },
    [types_1.NotificationKeyType.ActivityTaskChangeRating]: {
        title: "{{userName}} vừa đánh giá cho công việc",
        content: "{{taskTitle}}"
    },
    [types_1.NotificationKeyType.ActivityTaskEditPrivate]: {
        title: "{{userName}} vừa bật/tắt tính năng bảo mật cho công việc",
        content: "{{taskTitle}}"
    },
};
