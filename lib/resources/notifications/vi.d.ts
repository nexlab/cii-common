declare const _default: {
    message_create: {
        title: string;
        content: string;
    };
    message_update: {
        title: string;
        content: string;
    };
    message_update_member: {
        title: string;
        content: string;
    };
    message_add_like: {
        title: string;
        content: string;
    };
    message_add_comment: {
        title: string;
        content: string;
    };
    message_comment_add_mention: {
        title: string;
        content: string;
    };
    message_add_attachment: {
        title: string;
        content: string;
    };
    message_close: {
        title: string;
        content: string;
    };
    topic_create: {
        title: string;
        content: string;
    };
    topic_update: {
        title: string;
        content: string;
    };
    topic_update_member: {
        title: string;
        content: string;
    };
    topic_add_like: {
        title: string;
        content: string;
    };
    topic_add_comment: {
        title: string;
        content: string;
    };
    topic_comment_add_mention: {
        title: string;
        content: string;
    };
    topic_add_attachment: {
        title: string;
        content: string;
    };
    topic_close: {
        title: string;
        content: string;
    };
    plan_update: {
        title: string;
        content: string;
    };
    plan_delete: {
        title: string;
        content: string;
    };
    plan_item_update: {
        title: string;
        content: string;
    };
    plan_item_delete: {
        title: string;
        content: string;
    };
    remind_plan: {
        title: string;
        content: string;
    };
    remind_task: {
        title: string;
        content: string;
    };
    project_create: {
        title: string;
        content: string;
    };
    project_update: {
        title: string;
        content: string;
    };
    project_close: {
        title: string;
        content: string;
    };
    activity_task_create_task: {
        title: string;
        content: string;
    };
    activity_task_edit_title: {
        title: string;
        content: string;
    };
    activity_task_edit_description: {
        title: string;
        content: string;
    };
    activity_task_edit_task_status: {
        title: string;
        content: string;
    };
    activity_task_edit_deadline: {
        title: string;
        content: string;
    };
    activity_task_edit_task_group: {
        title: string;
        content: string;
    };
    activity_task_edit_estimation: {
        title: string;
        content: string;
    };
    activity_task_update_attachment: {
        title: string;
        content: string;
    };
    activity_task_change_assignee: {
        title: string;
        content: string;
    };
    activity_task_add_comment: {
        title: string;
        content: string;
    };
    activity_task_change_reporter: {
        title: string;
        content: string;
    };
    activity_task_change_rating: {
        title: string;
        content: string;
    };
    activity_task_edit_private: {
        title: string;
        content: string;
    };
};
export default _default;
