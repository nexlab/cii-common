"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
const lodash = require("lodash");
const vi_1 = require("./vi");
__export(require("./types"));
function getContentNotification(key, opts = {}) {
    const notification = lodash.cloneDeep(vi_1.default[key]);
    if (!notification) {
        console.log("Cannot found content notification with key " + key);
        return null;
    }
    return opts.params ? parseContentNotification(notification, opts.params) : notification;
}
exports.getContentNotification = getContentNotification;
function parseContentNotification(notification, params) {
    const delimiterLeft = "{{";
    const delimiterRight = "}}";
    const keyParamsContent = Object.keys(params.content || {});
    const keyParamsTitle = Object.keys(params.title || {});
    if (keyParamsContent.length > 0) {
        keyParamsContent.forEach((e) => {
            notification.content = notification.content.replace(delimiterLeft + e + delimiterRight, params.content[e]);
        });
    }
    if (keyParamsTitle.length > 0) {
        keyParamsTitle.forEach((e) => {
            notification.title = notification.title.replace(delimiterLeft + e + delimiterRight, params.title[e]);
        });
    }
    return notification;
}
exports.parseContentNotification = parseContentNotification;
