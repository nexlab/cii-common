import { Response } from "express";
import { Model } from "sequelize";
import { IRequest } from "../../authentication";
export interface ICRUDForModule {
    create(): Promise<any>;
    update(): Promise<any>;
    filter(): Promise<any>;
    destroy(): Promise<any>;
    get(): Promise<any>;
}
export declare enum FunctionNameCrud {
    create = "create",
    update = "update",
    filter = "filter",
    destroy = "destroy",
    get = "get"
}
export interface IOptionCRUDModule {
    status?: boolean;
    createdBy?: boolean;
    updatedBy?: boolean;
}
export declare function crudForModule<M extends Model>(req: IRequest, res: Response, model: {
    new (): M;
} & typeof Model, serializer: any, options?: IOptionCRUDModule): ICRUDForModule;
