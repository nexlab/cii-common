"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("../../helpers");
const models_1 = require("../../models");
var FunctionNameCrud;
(function (FunctionNameCrud) {
    FunctionNameCrud["create"] = "create";
    FunctionNameCrud["update"] = "update";
    FunctionNameCrud["filter"] = "filter";
    FunctionNameCrud["destroy"] = "destroy";
    FunctionNameCrud["get"] = "get";
})(FunctionNameCrud = exports.FunctionNameCrud || (exports.FunctionNameCrud = {}));
function crudForModule(req, res, model, serializer, options) {
    function create() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const form = req.body;
                if (options && options.createdBy) {
                    const user = req.currentUser;
                    form.createdBy = user.id;
                }
                const result = yield model.create(form);
                return res.json(serializer(result));
            }
            catch (error) {
                return helpers_1.responseError(res, error);
            }
        });
    }
    function update() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const form = req.body;
                const id = req.params.id;
                if (options && options.updatedBy) {
                    const user = req.currentUser;
                    form.updatedBy = user.id;
                }
                const result = yield model.update(form, {
                    where: { id },
                    returning: true
                });
                return res.json(serializer(result[1][0]));
            }
            catch (error) {
                return helpers_1.responseError(res, error);
            }
        });
    }
    function filter() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = req.query;
                const filterWhere = {};
                if (options && options.status) {
                    filterWhere.where = {
                        status: models_1.StatusCode.Active
                    };
                }
                const result = query.size && query.page
                    ? yield models_1.filterPagination(model, filterWhere, models_1.parsePaginationParams(query))
                    : yield models_1.filterAll(model, filterWhere, models_1.parsePaginationParams(query));
                result.data = result.data.map(serializer);
                return res.json(result);
            }
            catch (error) {
                return helpers_1.responseError(res, error);
            }
        });
    }
    function destroy() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.params.id;
                if (options && options.status) {
                    yield model.update({
                        status: models_1.StatusCode.Deleted
                    }, {
                        where: { id }
                    });
                }
                else {
                    yield model.destroy({
                        where: { id },
                        force: true
                    });
                }
                return res.json({
                    message: "Delete entire successfully"
                });
            }
            catch (error) {
                return helpers_1.responseError(res, error);
            }
        });
    }
    function get() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.params.id;
                const result = yield model.findById(id);
                return res.json(serializer(result));
            }
            catch (error) {
                return helpers_1.responseError(res, error);
            }
        });
    }
    return {
        create,
        update,
        filter,
        destroy,
        get
    };
}
exports.crudForModule = crudForModule;
