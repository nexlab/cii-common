"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = require("joi");
const helpers_1 = require("../helpers");
const models_1 = require("../models");
const resources_1 = require("../resources");
// authentication
function privateMiddleware(verify, options) {
    const { authIdKey = "authId" } = Object.assign({}, options);
    return (req, res, next) => __awaiter(this, void 0, void 0, function* () {
        // if (req.method.toLowerCase() === "options") {
        //   return next();
        // }
        const token = req.header("Authorization");
        if (!token) {
            console.log("Authorization header is required");
            return res.status(401).send({
                message: "Authorization header is required"
            });
        }
        try {
            const uid = typeof verify === "function" ? yield verify(token) : verify;
            const user = yield models_1.User.findOne({
                where: { [authIdKey]: uid },
                include: [{ model: models_1.Role }]
            });
            if (!user) {
                const message = `Can not find user with firebase ID: ${uid}`;
                console.log(message);
                return res.status(401).send({
                    message
                });
            }
            if (user.status !== models_1.StatusCode.Active) {
                const message = `User is ${user.status}`;
                console.log(message);
                return res.status(401).send({
                    message: `User is ${user.status}`
                });
            }
            req.currentUser = user;
            console.log("Log request:");
            console.log("Url: " + req.originalUrl);
            console.log("Method: " + req.method);
            console.log("Headers: " + JSON.stringify(req.headers, null, 2));
            console.log("Payload: " + JSON.stringify({
                body: req.body,
                params: req.params,
                query: req.query
            }, null, 2));
            return next();
        }
        catch (e) {
            if (!e.statusCode || e.statusCode !== 401) {
                console.error("Authorize error:", e);
            }
            console.log(e.message);
            return res.status(e.statusCode || 401).send(e || {
                message: "Unauthorized"
            });
        }
    });
}
exports.privateMiddleware = privateMiddleware;
// authorization
function authorization(actionName) {
    return (req, res, next) => {
        const { currentUser } = req;
        if (currentUser && currentUser.superuser) {
            return next();
        }
        if (currentUser && currentUser.Role.isSuperRole) {
            return next();
        }
        if (!currentUser || !currentUser.Role) {
            return helpers_1.responseError(res, {
                message: "Unauthorized",
                code: "unauthorized"
            }, { statusCode: 401 });
        }
        const listActions = currentUser.Role.actions;
        if (typeof actionName === "string") {
            if (listActions.includes(actionName)) {
                return next();
            }
        }
        else if (Array.isArray(actionName)) {
            if (listActions.some((r) => actionName.includes(r))) {
                return next();
            }
        }
        return helpers_1.responseError(res, {
            message: resources_1.getMessageError("permission"),
            code: "permission"
        }, {
            statusCode: 403
        });
    };
}
exports.authorization = authorization;
// validate
function commonValidator(schema, key) {
    return (req, res, next) => {
        const value = req[key];
        return joi_1.validate(value, schema).then(() => {
            return next();
        }).catch((errors) => {
            const firstError = errors.details[0];
            const error = {
                code: firstError.type,
                message: firstError.message
            };
            return helpers_1.responseError(res, error);
        });
    };
}
function validateParams(schema) {
    return commonValidator(schema, "params");
}
exports.validateParams = validateParams;
function validateBody(schema) {
    return commonValidator(schema, "body");
}
exports.validateBody = validateBody;
function validateQuery(schema) {
    return commonValidator(schema, "query");
}
exports.validateQuery = validateQuery;
function validateHeader(schema) {
    return commonValidator(schema, "headers");
}
exports.validateHeader = validateHeader;
