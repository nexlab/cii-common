import { User } from "../models";
export declare function userFindManyByIds(ids: Array<string | number>): Promise<User[]>;
export declare function mapMetadata(data: any[]): Promise<any[]>;
