import { Request, Response } from "express";
import { SchemaLike } from "joi";
import { User } from "../models";
import { Action } from "../models/permissions";
export declare type VerifyIdToken = (token: string) => Promise<string>;
export interface IRequest extends Request {
    currentUser: User;
}
export interface IPrivateMiddlewareOptions {
    authIdKey?: string;
}
export declare function privateMiddleware(verify: string | VerifyIdToken, options?: IPrivateMiddlewareOptions): (req: IRequest, res: Response, next: any) => Promise<any>;
export declare function authorization(actionName: Action | Action[]): (req: IRequest, res: Response, next: any) => any;
export declare function validateParams(schema: SchemaLike): (req: Request, res: Response, next: any) => Promise<any>;
export declare function validateBody(schema: SchemaLike): (req: Request, res: Response, next: any) => Promise<any>;
export declare function validateQuery(schema: SchemaLike): (req: Request, res: Response, next: any) => Promise<any>;
export declare function validateHeader(schema: SchemaLike): (req: Request, res: Response, next: any) => Promise<any>;
