"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash = require("lodash");
const sequelize_1 = require("sequelize");
const models_1 = require("../models");
function userFindManyByIds(ids) {
    return models_1.User.findAll({
        where: {
            id: {
                [sequelize_1.Op.in]: ids,
            }
        }
    });
}
exports.userFindManyByIds = userFindManyByIds;
function mapMetadata(data) {
    return __awaiter(this, void 0, void 0, function* () {
        const userIds = lodash.uniq(data.map((e) => [e.createdBy || null, e.updatedBy || null]).reduce((pre, current) => {
            return pre.concat(current);
        }, [])).filter((e) => e);
        const users = yield userFindManyByIds(userIds);
        return data.map((e) => (Object.assign(Object.assign({}, e.toJSON ? e.toJSON() : e), { createdBy: users.find((u) => u.id.toString() === (e.createdBy ? e.createdBy.toString() : null)), updatedBy: users.find((u) => u.id.toString() === (e.updatedBy ? e.updatedBy.toString() : null)) })));
    });
}
exports.mapMetadata = mapMetadata;
