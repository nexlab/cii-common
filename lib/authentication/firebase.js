"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const firebaseAdmin = require("firebase-admin");
const middleware_1 = require("./middleware");
function createFirebaseUser(form) {
    // create firebase user
    return firebaseAdmin.auth()
        .createUser({
        email: form.email,
        password: form.password
    });
}
exports.createFirebaseUser = createFirebaseUser;
function deleteFirebaseUser(uid) {
    // delete user firebase
    return firebaseAdmin.auth().deleteUser(uid);
}
exports.deleteFirebaseUser = deleteFirebaseUser;
function getUserFirebaseByEmail(email) {
    // get user firebase by email
    return firebaseAdmin.auth().getUserByEmail(email);
}
exports.getUserFirebaseByEmail = getUserFirebaseByEmail;
// update information of user, but create this function to update password for user
function updateUserFirebaseByAuthId(authId, form) {
    return firebaseAdmin.auth().updateUser(authId, form);
}
exports.updateUserFirebaseByAuthId = updateUserFirebaseByAuthId;
function verifyFirebaseToken(token) {
    return firebaseAdmin.auth()
        .verifyIdToken(token)
        .then((user) => user.uid);
}
exports.verifyFirebaseToken = verifyFirebaseToken;
function firebaseAuthMiddleware(authId) {
    return middleware_1.privateMiddleware(authId || verifyFirebaseToken);
}
exports.firebaseAuthMiddleware = firebaseAuthMiddleware;
