import * as firebaseAdmin from "firebase-admin";
export interface ICreateUserInput {
    email: string;
    password: string;
}
export declare function createFirebaseUser(form: ICreateUserInput): Promise<firebaseAdmin.auth.UserRecord>;
export declare function deleteFirebaseUser(uid: string): Promise<void>;
export declare function getUserFirebaseByEmail(email: string): Promise<firebaseAdmin.auth.UserRecord>;
export interface IUserFirebaseForm {
    password?: string;
    email?: string;
}
export declare function updateUserFirebaseByAuthId(authId: string, form: IUserFirebaseForm): Promise<firebaseAdmin.auth.UserRecord>;
export declare function verifyFirebaseToken(token: string): Promise<string>;
export declare function firebaseAuthMiddleware(authId?: string): any;
