import * as Sequelize from "sequelize";
export declare class PlanUsers extends Sequelize.Model {
    id: number;
    userId: number;
    planIdsUnRead: number[];
}
export declare function initPlanUsers(sequelize: Sequelize.Sequelize): void;
