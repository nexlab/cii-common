"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
class PlanUsers extends Sequelize.Model {
}
exports.PlanUsers = PlanUsers;
function initPlanUsers(sequelize) {
    PlanUsers.init({
        id: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true,
        },
        userId: {
            type: Sequelize.BIGINT,
            allowNull: false,
            references: {
                model: "users",
                key: "id"
            }
        },
        planIdsUnRead: {
            type: Sequelize.ARRAY(Sequelize.BIGINT),
            allowNull: false
        },
        createdAt: {
            type: Sequelize.DATE,
            allowNull: true,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type: Sequelize.DATE,
            allowNull: true,
            defaultValue: Sequelize.NOW,
        },
        deletedAt: {
            type: Sequelize.DATE,
            allowNull: true,
        }
    }, {
        tableName: "plan_users",
        sequelize,
        paranoid: true,
        timestamps: true,
    });
}
exports.initPlanUsers = initPlanUsers;
