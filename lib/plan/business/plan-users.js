"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash = require("lodash");
const plan_users_1 = require("../model/plan-users");
function createPlanUsers(data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const planUsers = yield plan_users_1.PlanUsers.findAll({
                where: {
                    userId: data.memberIds
                }
            });
            function parseIds(reverse = false) {
                return planUsers.filter((e) => {
                    const current = data.memberIds.find((a) => a.toString() === e.userId.toString());
                    if (current) {
                        return reverse;
                    }
                    return !reverse;
                });
            }
            const planUsersNeedCreate = data.memberIds.filter((e) => {
                const current = planUsers.find((a) => e.toString() === a.userId.toString());
                if (current) {
                    return false;
                }
                return true;
            });
            const planUsersNeedUpdate = parseIds(true);
            if (planUsersNeedCreate && planUsersNeedCreate.length > 0) {
                yield Promise.all(planUsersNeedCreate.map((e) => {
                    return plan_users_1.PlanUsers.create({
                        userId: e,
                        planIdsUnRead: [data.planId]
                    });
                }));
            }
            if (planUsersNeedUpdate && planUsersNeedUpdate.length > 0) {
                yield Promise.all(planUsersNeedUpdate.map((e) => {
                    e.planIdsUnRead.push(data.planId);
                    return plan_users_1.PlanUsers.update({
                        planIdsUnRead: lodash.uniq(e.planIdsUnRead)
                    }, {
                        where: { userId: e.userId, }
                    });
                }));
            }
            return;
        }
        catch (error) {
            return Promise.reject(error);
        }
    });
}
exports.createPlanUsers = createPlanUsers;
function markPlanUserAsRead(userId) {
    return __awaiter(this, void 0, void 0, function* () {
        const current = yield plan_users_1.PlanUsers.find({
            where: { userId }
        });
        let result = [];
        if (current) {
            result = current.planIdsUnRead || [];
        }
        if (result.length > 0) {
            yield plan_users_1.PlanUsers.update({ planIdsUnRead: [] }, {
                where: { userId }
            });
        }
        return result;
    });
}
exports.markPlanUserAsRead = markPlanUserAsRead;
