export interface ICreatePlanUsers {
    memberIds: Array<string | number>;
    planId: string;
}
export declare function createPlanUsers(data: ICreatePlanUsers): Promise<any>;
export declare function markPlanUserAsRead(userId: number | string): Promise<number[]>;
